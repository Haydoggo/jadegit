cmake_minimum_required(VERSION 3.22)

# Derive version information from git
execute_process(
  COMMAND git describe --dirty --long --tags --match "v[0-9]*"
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
  OUTPUT_VARIABLE GIT_VERSION
  OUTPUT_STRIP_TRAILING_WHITESPACE
)
# Append branch
execute_process(
  COMMAND git branch --show-current
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
  OUTPUT_VARIABLE GIT_BRANCH
  OUTPUT_STRIP_TRAILING_WHITESPACE
)
if(GIT_BRANCH)
	string(CONCAT GIT_VERSION ${GIT_VERSION} "-" ${GIT_BRANCH})
endif()

# Parse version components
string(REGEX REPLACE "^v([0-9]+)\\.([0-9]+)\\.([0-9]+)(\\-.*)?\\-([0-9]+)\\-(.*)$" "\\1" GIT_VERSION_MAJOR "${GIT_VERSION}")
set(GIT_VERSION_MINOR ${CMAKE_MATCH_2})
set(GIT_VERSION_PATCH ${CMAKE_MATCH_3})
set(GIT_VERSION_BETA ${CMAKE_MATCH_4})
set(GIT_VERSION_TWEAK ${CMAKE_MATCH_5})
set(GIT_VERSION_HASH ${CMAKE_MATCH_6})

# Set overall version string in preferred format
set(GIT_VERSION ${GIT_VERSION_MAJOR}.${GIT_VERSION_MINOR}.${GIT_VERSION_PATCH}${GIT_VERSION_BETA}.${GIT_VERSION_TWEAK}-${GIT_VERSION_HASH})

# Define project using version info derived above
project(jadegit VERSION ${GIT_VERSION_MAJOR}.${GIT_VERSION_MINOR}.${GIT_VERSION_PATCH}.${GIT_VERSION_TWEAK} LANGUAGES CXX)

option(BUILD_SHARED_LIBS "Build shared libraries" OFF)
option(BUILD_TESTING "Build tests" ON)

# JADE and/or libgit2 dependent functionality may be disabled
# Provides ability to narrow the scope of a build to functionality that can be used in isolation
option(USE_JADE "Include JADE dependent functionality" ON)
option(USE_GIT2 "Include libgit2 dependent functionality" ON)

# jadegit may be built as command line utility without schema/dev functionality
include(CMakeDependentOption)
cmake_dependent_option(WITH_SCHEMA "With JadeGitSchema dependent functionality" ON "USE_JADE;USE_GIT2" OFF)

# jadegit may be built for unicode JADE environments
cmake_dependent_option(UNICODE "Unicode JADE environment" OFF "USE_JADE" OFF)

include(GNUInstallDirs)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/lib/$<0:>)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/lib/$<0:>)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin/$<0:>)
set(CMAKE_PDB_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin/$<0:>)

# Enable parallel builds
add_compile_options($<$<CXX_COMPILER_ID:MSVC>:/MP>)

add_subdirectory(deps)
add_subdirectory(src)

# Enable testing
if(PROJECT_IS_TOP_LEVEL)
    include(CTest)
	if(BUILD_TESTING)
		add_subdirectory(test)
	endif()
endif()

#include "Builder.h"
#include <deploy/build/PowerShellDeploymentBuilder.h>

using namespace JadeGit;
using namespace JadeGit::Deploy;

TEST_CASE("PowerShellDeploymentBuilder", "[deploy]")
{
	// Setup builder
	MemoryFileSystem fs;
	PowerShellDeploymentBuilder builder(fs);

	// Common tests
	common_test_scenarios(fs, builder);
}
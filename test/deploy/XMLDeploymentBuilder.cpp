#include "Builder.h"
#include <deploy/build/XMLDeploymentBuilder.h>

using namespace JadeGit;
using namespace JadeGit::Build;
using namespace JadeGit::Deploy;

TEST_CASE("XMLDeploymentBuilder", "[deploy]")
{
	// Setup builder
	MemoryFileSystem fs;
	XMLDeploymentBuilder builder(fs);

	// Common tests
	common_test_scenarios(fs, builder);
}
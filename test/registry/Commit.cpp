#include <catch2/catch_test_macros.hpp>
#include <registry/Commit.h>

using namespace JadeGit;
using namespace JadeGit::Registry;

TEST_CASE("Commit.String", "[registry]")
{
	const char* sha = "4a202b346bb0fb0db7eff3cffeb3c70babbd2045";

#if USE_GIT2
	git_oid oid = { {0} };
	REQUIRE(git_oid_fromstr(&oid, sha) == 0);
	CommitT commit = make_commit(oid);
#else
	CommitT commit = make_commit(sha);
#endif

	std::ostringstream oss;
	oss << commit;

	CHECK(oss.str() == sha);
}

TEST_CASE("Commit.Equals", "[registry]")
{
	const char* sha = "4a202b346bb0fb0db7eff3cffeb3c70babbd2045";

#if USE_GIT2
	git_oid oid = { {0} };
	REQUIRE(git_oid_fromstr(&oid, sha) == 0);
	CommitT a = make_commit(oid);
#else
	CommitT a = make_commit(sha);
#endif
	CommitT b = make_commit(sha);

	CHECK(a == b);
}

TEST_CASE("Commit.NotEquals", "[registry]")
{
	CommitT a = make_commit("4a202b346bb0fb0db7eff3cffeb3c70babbd2045");
	CommitT b = make_commit("4a202b346bb0fb0db7eff3cffeb3c70babbd2046");

	CHECK(a != b);
}
#include <catch2/catch_test_macros.hpp>
#include <registry/Commit.h>
#include <registry/Root.h>
#include <registry/Schema.h>
#include <registry/Validator.h>

using namespace JadeGit;
using namespace JadeGit::Registry;

TEST_CASE("Validator", "[registry]")
{
	Validator validator;
	Root registry;

	SECTION("Empty")
	{
		CHECK(registry.accept(validator));
	}
	SECTION("Basic")
	{
		RepositoryT repo;
		repo.name = "foo";
		repo.origin = "http://gitmock.com/foo.git";
		repo.latest.push_back(make_commit("aaaa"));
		repo.schemas.push_back(make_schema("FooSchema"));

		registry.add(repo);

		CHECK(registry.accept(validator));
	}
	SECTION("DuplicateNames")
	{
		RepositoryT repo;
		repo.name = "foo";

		registry.add(repo);
		registry.add(repo);

		CHECK_THROWS(registry.accept(validator));
	}
	SECTION("DuplicateOrigins")
	{
		RepositoryT repo;
		repo.origin = "http://gitmock.com/foo.git";

		registry.add(repo);
		registry.add(repo);

		CHECK_THROWS(registry.accept(validator));
	}
	SECTION("DuplicateSchemas")
	{
		RepositoryT foo;
		foo.name = "foo";
		foo.origin = "http://gitmock.com/foo.git";
		foo.schemas.push_back(make_schema("CommonSchema"));
		foo.schemas.push_back(make_schema("FooSchema"));

		registry.add(foo);

		RepositoryT bar;
		bar.name = "bar";
		bar.origin = "http://gitmock.com/bar.git";
		bar.schemas.push_back(make_schema("CommonSchema"));
		bar.schemas.push_back(make_schema("BarSchema"));

		registry.add(bar);

		CHECK_THROWS(registry.accept(validator));
	}
}
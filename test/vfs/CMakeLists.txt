target_sources(jadegit_test PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/FileSystem.cpp
	${CMAKE_CURRENT_LIST_DIR}/MemoryFileSystem.cpp
)

if(USE_GIT2)
	target_sources(jadegit_test PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/GitFileSystem.cpp
	)
endif()

catch_discover_tests(jadegit_test
					 TEST_SPEC "[vfs]"
					 TEST_PREFIX "jadegit.vfs."
					 WORKING_DIRECTORY ${PROJECT_SOURCE_DIR})
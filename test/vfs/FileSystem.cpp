#include "FileSystem.h"
#include <jadegit/vfs/File.h>
#include <jadegit/vfs/RecursiveFileIterator.h>

using namespace std;
using namespace JadeGit;

namespace Catch {
	template<>
	struct is_range<FileIterator> {
		static const bool value = false;
	};

	template<>
	struct is_range<RecursiveFileIterator> {
		static const bool value = false;
	};
}

void iterator_test_scenarios(FileSystemFactory& factory)
{
	SECTION("Iterate.SingleFile")
	{
		FileSystem& fs = factory.clone(SourceFileSystem("resources/SingleFile"));

		// Open root folder
		File folder = fs.open("");

		// Check it exists
		REQUIRE(folder.exists());

		// Create iterator
		FileIterator iter = folder.begin();

		// Iterator should not be at the end
		REQUIRE(iter != folder.end());

		// Iterator should point at first entry (the file)
		REQUIRE(iter->path() == "HelloWorld.txt");

		// Move iterator (which should make it go to end)
		REQUIRE(++iter == folder.end());
	}
	SECTION("Iterate.SingleFileWithinFolder")
	{
		// Setup file system, cloning from embedded file system
		FileSystem& fs = factory.clone(SourceFileSystem("resources/SingleFileWithinFolder"));

		// Create iterator
		RecursiveFileIterator iter(fs.open(""));

		// Iterator should not be at the end
		REQUIRE(iter != end(iter));

		// Iterator should point at first entry (the folder)
		REQUIRE(iter->path() == "Folder");

		// Move iterator (which should move it to file within)
		REQUIRE("Folder/HelloWorld.txt" == (++iter)->path());

		// Move iterator (which should make it go to end)
		REQUIRE(++iter == end(iter));
	}
}

void readonly_test_scenarios(FileSystemFactory& factory)
{
	SECTION("ReadOnly.Empty")
	{
		CHECK(factory.empty().isReadOnly());
	}
	SECTION("ReadOnly.Clone")
	{
		CHECK(factory.clone(SourceFileSystem("resources/SingleFileWithinFolder")).isReadOnly());
	}
}

void writable_test_scenarios(FileSystemFactory& factory)
{
	FileSystem& fs = factory.empty();

	SECTION("FileCreation")
	{
		// Open single file
		File file = fs.open("HelloWorld.txt");

		// Check it doesn't already exist
		CHECK(!file.exists());

		// Write to file
		file.write("Hello\nWorld"sv);

		// Check it now exists and can be read as expected
		CHECK(file.exists());
		CHECK("Hello\nWorld" == file.read());

		// Check it can be re-opened and still read as expected
		file = fs.open("HelloWorld.txt");
		CHECK(file.exists());
		CHECK("Hello\nWorld" == file.read());
	}
	SECTION("FileUpdate")
	{
		// Open single file
		File file = fs.open("HelloWorld.txt");

		// Check it doesn't already exist
		CHECK(!file.exists());

		// Write to file
		file.write("This is a longer hello\nworld"sv);

		// Check it now exists and can be read as expected
		CHECK(file.exists());
		CHECK("This is a longer hello\nworld" == file.read());

		// Check it can be re-opened and still read as expected
		file = fs.open("HelloWorld.txt");
		CHECK(file.exists());
		CHECK("This is a longer hello\nworld" == file.read());

		// Check it can be re-written with smaller content
		file = fs.open("HelloWorld.txt");
		file.write("Hello\nWorld"sv);

		// Check it can be re-opened and still read as expected
		file = fs.open("HelloWorld.txt");
		CHECK(file.exists());
		CHECK("Hello\nWorld" == file.read());
	}
	SECTION("FolderCreation")
	{
		// Open single file
		File file = fs.open("Folder/HelloWorld.txt");

		// Check it doesn't already exist
		CHECK(!file.exists());

		// Write to file
		file.write("Hello World (again)"sv);

		// Check it now exists and can be read as expected
		CHECK(file.exists());
		CHECK("Hello World (again)" == file.read());

		// Check file can be re-opened via root and still read as expected
		file = fs.open("Folder/HelloWorld.txt");
		CHECK(file.exists());
		CHECK("Hello World (again)" == file.read());

		// Check folder can now be opened
		File folder = fs.open("Folder");
		CHECK(folder.exists());
		CHECK(folder.isDirectory());

		// Check file can be opened via folder
		file = folder.open("HelloWorld.txt");

		// Check it exists and can be read as expected
		CHECK(file.exists());
		CHECK("Hello World (again)" == file.read());

		// Check new folder can be iterated over
		folder = fs.open("");
		CHECK(folder.exists());

		FileIterator iter = folder.begin();
		CHECK(iter != folder.end());		// Iterator should not be at the end
		CHECK(iter->path() == "Folder");	// Iterator should point at first entry (the folder created above)
		CHECK(++iter == folder.end());		// Move iterator (which should make it go to end)
	}
}
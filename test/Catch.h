#pragma once
#include <catch2/catch_test_macros.hpp>

namespace Catch
{
	class TestInvokerAsStdFunction : public Catch::ITestInvoker
	{
	public:
		TestInvokerAsStdFunction(std::function<void()> f) : f(f) {}

		void invoke() const final { f(); }

	public:
		std::function<void()> f;
	};

	inline Detail::unique_ptr<ITestInvoker> makeTestInvoker(std::function<void()> f) noexcept
	{
		return Detail::make_unique<TestInvokerAsStdFunction>(f);
	}
}
#include "TestFileSystem.h"

namespace JadeGit
{
	TestFileSystem::TestFileSystem(fs::path subfolder) : NativeFileSystem(fs::temp_directory_path() / "test" / subfolder, true)
	{
		fs::remove_all(base);
		fs::create_directory(base);
	}

	TestFileSystem::~TestFileSystem()
	{
		fs::remove_all(base);
	}

	fs::path TestFileSystem::path() const
	{
		return base;
	}
}
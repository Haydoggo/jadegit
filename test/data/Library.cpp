#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/vfs/MemoryFileSystem.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("Library.Creation", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");

	{
		Library library(&schema, nullptr, "testLibrary");

		// Check name has been set as expected
		CHECK("testLibrary" == library.GetName());
		CHECK("testLibrary" == library.GetValue("name").Get<string>());

		// Check library has been added to collection
		CHECK(&library == schema.libraries.Get("testLibrary"));
	}

	// Check implicit deletion has removed library from collection
	CHECK(nullptr == schema.libraries.Get("testLibrary"));
}

TEST_CASE("Library.EntrypointsInverseMaintenance", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");

	Function function(&schema, nullptr, "testFunction");

	{
		Library library(&schema, nullptr, "testLibrary");

		// Adding function to library should set inverse
		library.entrypoints.Add(&function);
		CHECK(&library == function.library);
	}

	// Check function library has been reset implicitly when library is deleted
	CHECK(nullptr == static_cast<Library*>(function.library));
}

TEST_CASE("Library.Resolution", "[data]")
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "BaseSchema");
	Library library(&schema, nullptr, "testlib");

	// Check local library can be resolved irrespective of inherit option
	CHECK(Entity::resolve<Library>(schema, "testlib", false, false) == &library);
	CHECK(Entity::resolve<Library>(schema, "testlib", false, true) == &library);

	Schema subschema(assembly, nullptr, "SubSchema");
	subschema.superschema = &schema;

	// Check inherited library can be resolved when inherit option is used
	CHECK(Entity::resolve<Library>(subschema, "testlib", false, false) == nullptr);
	CHECK(Entity::resolve<Library>(subschema, "testlib", false, true) == &library);
}
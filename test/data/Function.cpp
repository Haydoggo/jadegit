#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/vfs/MemoryFileSystem.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("Function.Creation", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");

	{
		Function function(&schema, nullptr, "testFunction");

		// Check name has been set as expected
		CHECK("testFunction" == function.GetName());
		CHECK("testFunction" == function.GetValue("name").Get<string>());

		// Check function has been added to collection
		CHECK(&function == schema.functions.Get("testFunction"));
	}

	// Check function has been removed from collection
	CHECK(nullptr == schema.functions.Get("testFunction"));
}

TEST_CASE("Function.LibraryInverseMaintenance", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");

	Library libraryA(&schema, nullptr, "testLibraryA");
	Library libraryB(&schema, nullptr, "testLibraryB");

	{
		Function function(&schema, nullptr, "testFunction");

		// Setting library should add to inverse collection
		function.library = &libraryA;
		CHECK(libraryA.entrypoints.Includes(&function));

		// Switching to new library should remove from previous and add to new
		function.library = &libraryB;
		CHECK(!libraryA.entrypoints.Includes(&function));
		CHECK(libraryB.entrypoints.Includes(&function));
	}

	// Check function has been removed from last library
	CHECK(libraryB.entrypoints.Empty());
}
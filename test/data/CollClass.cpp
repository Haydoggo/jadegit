#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/Attribute.h>
#include <jadegit/data/Key.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/vfs/MemoryFileSystem.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Data;
using namespace tinyxml2;

TEST_CASE("ClassClass.Creation", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");
	schema.superschema = assembly.GetRootSchema();

	Class klass(&schema, nullptr, "TestClass");
	PrimAttribute name(&klass, nullptr, "name");

	CollClass dict(&schema, nullptr, "TestDict");
	dict.memberType = &klass;

	MemberKey key(&dict, nullptr);
	key.property = &name;
}

TEST_CASE("ClassClass.MemberKeyWrite", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");
	schema.superschema = assembly.GetRootSchema();
	
	Class klass(&schema, nullptr, "TestClass");
	PrimAttribute name(&klass, nullptr, "name");

	CollClass dict(&schema, nullptr, "TestDict");
	dict.Created("47183823-2574-4bfd-b411-99ed177d3e43");
	dict.memberType = &klass;

	MemberKey key(&dict, nullptr);
	key.property = &name;

	XMLDocument document;
	dict.Write(document);

	XMLPrinter printer;
	document.Print(&printer);

	CHECK(string(printer.CStr()) ==
		"<CollClass name=\"TestDict\" id=\"47183823-2574-4bfd-b411-99ed177d3e43\">\n"
		"    <memberType name=\"TestClass\"/>\n"
		"    <MemberKey>\n"
		"        <property name=\"TestClass::name\"/>\n"
		"    </MemberKey>\n"
		"</CollClass>\n");
}

TEST_CASE("ClassClass.MemberKeyPathWrite", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");
	schema.superschema = assembly.GetRootSchema();

	Class klass(&schema, nullptr, "TestClass");
	PrimAttribute name(&klass, nullptr, "name");

	CollClass dict(&schema, nullptr, "TestDict");
	dict.Created("47183823-2574-4bfd-b411-99ed177d3e43");
	dict.memberType = &klass;

	MemberKey key(&dict, nullptr);
	key.keyPath.Add(&name);

	XMLDocument document;
	dict.Write(document);

	XMLPrinter printer;
	document.Print(&printer);

	CHECK(string(printer.CStr()) ==
		"<CollClass name=\"TestDict\" id=\"47183823-2574-4bfd-b411-99ed177d3e43\">\n"
		"    <memberType name=\"TestClass\"/>\n"
		"    <MemberKey>\n"
		"        <keyPath>\n"
		"            <PrimAttribute name=\"TestClass::name\"/>\n"
		"        </keyPath>\n"
		"    </MemberKey>\n"
		"</CollClass>\n");
}
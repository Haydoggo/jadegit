#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/KeyValue.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("Value.BooleanChangeSignal", "[data]") 
{
	KeyValue<bool> value(false);

	bool latest = false;

	value.OnChange().Connect(nullptr, [&](bool value) { latest = value; });

	value = true;

	CHECK(true == latest);
}

TEST_CASE("Value.BooleanChangeSignalViaSet", "[data]") 
{
	KeyValue<bool> value(false);

	bool latest = false;

	value.OnChange().Connect(nullptr, [&](bool value) { latest = value; });

	value.Set("true");

	CHECK(true == latest);
}

TEST_CASE("Value.BooleanToBasicString", "[data]") 
{
	Any value(Value<bool>(false));

	CHECK("false" == value.ToBasicString());

	value = Value<bool>(true);

	CHECK("true" == value.ToBasicString());
}

TEST_CASE("Value.ByteToString", "[data]") 
{
	Any value(Value<Byte>(99));
	CHECK("99" == value.ToString());
}

TEST_CASE("Value.CharacterSet", "[data]")
{
	Value<char> character('A');
	character.Set(string("B"));

	CHECK('B' == character);
}

TEST_CASE("Value.IntegerSet", "[data]")
{
	Value<int> integer(123);
	integer.Set(string("456"));

	CHECK(456 == integer);
}

TEST_CASE("Value.IntegerChangeSignal", "[data]")
{
	KeyValue<int> value(123);

	int latest = 0;

	value.OnChange().Connect(nullptr, [&](int value) { latest = value; });

	value = 456;

	CHECK(456 == latest);
}

TEST_CASE("Value.IntegerToString", "[data]") 
{
	Any value(Value<int>(123));
	CHECK("123" == value.ToString());
}

TEST_CASE("Value.IntegerToBasicString", "[data]") 
{
	Any value(Value<int>(456));
	CHECK("456" == value.ToBasicString());
}

TEST_CASE("Value.IntegerComparisons", "[data]")
{
	Value<int> a(1);
	Value<int> b(1);
	Value<int> c(2);

	CHECK(a == b);
	CHECK(b == b);
	CHECK(b != c);
}

TEST_CASE("Value.RealSet", "[data]")
{
	Value<double> real(123.123);
	real.Set(string("456.456"));

	CHECK(456.456 == real);
}

TEST_CASE("Value.StringChangeSignal", "[data]") 
{
	KeyValue<string> value = "Hello";

	string latest = "";

	value.OnChange().Connect(nullptr, [&](string value) { latest = value; });

	value = "World";

	CHECK("World" == latest);
}

TEST_CASE("Value.CastStringToInteger", "[data]") 
{
	Any value(Value<string>("123"));
	CHECK(123 == value.Get<int>());
}
target_precompile_headers(jadegit_test PRIVATE
	<jadegit/data/Assembly.h>
)

target_sources(jadegit_test PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/Any.cpp
	${CMAKE_CURRENT_LIST_DIR}/Application.cpp
	${CMAKE_CURRENT_LIST_DIR}/Array.cpp
	${CMAKE_CURRENT_LIST_DIR}/Assembly.cpp
	${CMAKE_CURRENT_LIST_DIR}/Chrono.cpp
	${CMAKE_CURRENT_LIST_DIR}/Class.cpp
	${CMAKE_CURRENT_LIST_DIR}/CollClass.cpp
	${CMAKE_CURRENT_LIST_DIR}/Collection.cpp
	${CMAKE_CURRENT_LIST_DIR}/Constant.cpp
	${CMAKE_CURRENT_LIST_DIR}/ConstantCategory.cpp
	${CMAKE_CURRENT_LIST_DIR}/Database.cpp
	${CMAKE_CURRENT_LIST_DIR}/DbClassMap.cpp
	${CMAKE_CURRENT_LIST_DIR}/DbFile.cpp
	${CMAKE_CURRENT_LIST_DIR}/EntityRegistration.cpp
	${CMAKE_CURRENT_LIST_DIR}/Enum.cpp
	${CMAKE_CURRENT_LIST_DIR}/ExplicitInverseRef.cpp
	${CMAKE_CURRENT_LIST_DIR}/Form.cpp
	${CMAKE_CURRENT_LIST_DIR}/Function.cpp
	${CMAKE_CURRENT_LIST_DIR}/GlobalConstant.cpp
	${CMAKE_CURRENT_LIST_DIR}/GUIClass.cpp
	${CMAKE_CURRENT_LIST_DIR}/ImplicitInverseRef.cpp
	${CMAKE_CURRENT_LIST_DIR}/JadeImportedPackage.cpp
	${CMAKE_CURRENT_LIST_DIR}/JadeMethod.cpp
	${CMAKE_CURRENT_LIST_DIR}/Library.cpp
	${CMAKE_CURRENT_LIST_DIR}/Locale.cpp
	${CMAKE_CURRENT_LIST_DIR}/MockEntityIDAllocator.cpp
	${CMAKE_CURRENT_LIST_DIR}/Schema.cpp
	${CMAKE_CURRENT_LIST_DIR}/Signal.cpp
	${CMAKE_CURRENT_LIST_DIR}/Value.cpp
	${CMAKE_CURRENT_LIST_DIR}/Version.cpp
)

include(${CMAKE_CURRENT_LIST_DIR}/config/CMakeLists.txt)

catch_discover_tests(jadegit_test
					 TEST_SPEC "[data]"
					 TEST_PREFIX "jadegit.data."
					 WORKING_DIRECTORY ${PROJECT_SOURCE_DIR})
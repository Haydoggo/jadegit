#pragma once
#include "ExternalSchemaEntity.h"
#include "RootSchema/ExternalColumnMeta.h"

namespace JadeGit::Data
{
	class ExternalTable;

	class ExternalColumn : public ExternalSchemaEntity
	{
	public:
		ExternalColumn(ExternalTable& parent, const Class* dataClass, const char* name);

		ObjectValue<ExternalTable* const, &ExternalColumnMeta::table> table;
	};
}
#pragma once
#include "ActiveXFeature.h"

namespace JadeGit::Data
{
	class ActiveXConstant : public ActiveXFeature
	{
	public:
		ActiveXConstant(ActiveXClass& parent, const Class* dataClass, const char* name);

		void Accept(EntityVisitor& v) final;

	protected:
		void loaded(bool strict, std::queue<std::future<void>>& tasks) final;
	};
}
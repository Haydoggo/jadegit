#pragma once
#include "Type.h"
#include "RootSchema/MemberKeyMeta.h"

namespace JadeGit::Data
{
	class CollClass;

	class Key : public Object
	{
	public:
		Key(CollClass* parent, const Class* dataClass);

		CollClass* const collClass;

		Value<bool> caseInsensitive = false;
		Value<bool> descending = false;
		Value<int> sortOrder = 0;
	};

	class ExternalKey : public NamedObject<Key>
	{
	public:
		ExternalKey(CollClass* parent, const Class* dataClass, const char* name = nullptr);

		Value<int> length = 0;
		Value<int> precision = 0;
		Value<int> scaleFactor = 0;
		Value<Type*> type;
	};

	class MemberKey : public Key
	{
	public:
		MemberKey(CollClass* parent, const Class* dataClass);

		ObjectValue<Array<Property*>, &MemberKeyMeta::keyPath> keyPath;
		Value<Property*> property;
	};
}
#pragma once
#include <jadegit/MemoryAllocated.h>
#include <vector>
#include <memory>

namespace JadeGit::Data
{
	class RootSchema;
	class Assembly;

	// Base meta object
	class MetaObject : virtual public MemoryAllocated
	{
	public:
		MetaObject() {}
		virtual ~MetaObject() = default;

	protected:
		MetaObject(MetaObject& parent);

		std::pmr::vector<std::unique_ptr<MetaObject>> children;

		const RootSchema& GetRootSchema(const Assembly& assembly) const;
	};

	// Base template for metadata which can be implicitly casted to the subject
	template<class TSubject>
	class Meta : public MetaObject
	{
	public:
		Meta(MetaObject& parent, TSubject* subject) : MetaObject(parent), subject(subject) {}

		operator TSubject* () const
		{
			return this ? subject : nullptr;
		}

		TSubject* operator->() const noexcept
		{
			return subject;
		}

	protected:
		Meta(TSubject* subject) : subject(subject) {}

		TSubject * const subject;

		const RootSchema& GetRootSchema()
		{
			return MetaObject::GetRootSchema(subject->getAssembly());
		}
	};
}
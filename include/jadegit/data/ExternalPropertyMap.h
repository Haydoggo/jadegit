#pragma once
#include "ExternalSchemaMap.h"

namespace JadeGit::Data
{
	class ExternalPropertyMap : public ExternalSchemaMap
	{
	public:
		using ExternalSchemaMap::ExternalSchemaMap;

	protected:
		void loaded(bool strict, std::queue<std::future<void>>& tasks) final;
	};
}
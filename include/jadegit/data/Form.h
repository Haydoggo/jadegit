#pragma once
#include "Control.h"
#include "MenuItem.h"
#include "Array.h"
#include "EntityDict.h"
#include "RootSchema/FormMeta.h"

namespace JadeGit::Data
{
	class Control;
	class GUIClass;
	class Locale;

	class Form : public MajorEntity<Form, Window>
	{
	public:
		static const std::filesystem::path subFolder;

		Form(Locale* parent, const Class* dataClass, const char* name);

		ObjectValue<Locale* const, &FormMeta::locale> locale;
		MemberKeyDictionary<Control, &Entity::name> controls;
		ObjectValue<Array<Control*>, &FormMeta::controlList> controlList;
		ObjectValue<Array<MenuItem*>, &FormMeta::menuList> menuList;

		// Resolve form class
		GUIClass* GetClass() const;

		// Resolve super-form
		Form* GetSuperForm() const;

		void Accept(EntityVisitor &v) override;

	protected:
		bool isOrganized() const override { return true; }
	};

	extern template ObjectValue<Locale* const, &FormMeta::locale>;
	extern template MemberKeyDictionary<Control, &Entity::name>;
	extern template ObjectValue<Array<Control*>, &FormMeta::controlList>;
	extern template ObjectValue<Array<MenuItem*>, &FormMeta::menuList>;
}
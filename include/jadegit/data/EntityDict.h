#pragma once
#include "Entity.h"
#include "MemberKeyDictionary.h"

namespace JadeGit::Data
{
	template<class TChild, auto prop>
	using EntityDict = ObjectValue<MemberKeyDictionary<TChild, &Entity::name>, prop>;
}
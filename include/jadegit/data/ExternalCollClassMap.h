#pragma once
#include "ExternalAttributeMap.h"
#include "RootSchema/ExternalCollClassMapMeta.h"

namespace JadeGit::Data
{
	class ExternalDatabase;

	class ExternalCollClassMap : public ExternalSchemaMap
	{
	public:
		ExternalCollClassMap(ExternalDatabase& parent, const Class* dataClass, const char* name);

		ObjectValue<ExternalDatabase* const, &ExternalCollClassMapMeta::database> database;

	protected:
		void loaded(bool strict, std::queue<std::future<void>>& tasks) final;
	};
}
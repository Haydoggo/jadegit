#pragma once
#include <jadegit/data/Object.h>
#include <assert.h>
#include <functional>
#include <typeindex>

namespace JadeGit::Data
{
	class Class;
	class Object;
	class AnyValue;

	class ObjectFactory
	{
	public:
		static ObjectFactory& Get();

		class Registration
		{
		public:
			virtual Object* Create(Component* parent, const Class* dataClass) const = 0;
			virtual Object* Resolve(const Component* origin, bool expected) const = 0;
			virtual std::function<Object* ()> resolver(const Component* origin, const tinyxml2::XMLElement* source, bool expected, bool shallow, bool inherit) const = 0;
			virtual Object* load(Component* parent, const Class* dataClass, const FileElement& source) const = 0;
			virtual AnyValue* CreateValue() const = 0;
			virtual AnyValue* CreateValue(Object& parent, const Property& property, const Class* dataClass, bool exclusive) const = 0;
		};

		template<class TDerived>
		void Register(const char* key, const Registration* registrar)
		{
			registryByName[key] = registrar;
			registryByType[std::type_index(typeid(TDerived))] = registrar;
		}

		template<class TDerived = Object>
		TDerived* Create(const std::string& key, Component* origin) const
		{
			Class* dataClass = nullptr;
			Object* result = Lookup(key, origin, dataClass)->Create(origin, dataClass);
			assert(!result || dynamic_cast<TDerived*>(result));
			return static_cast<TDerived*>(result);
		}

		Object* load(const std::string& key, Component* parent, const FileElement& source) const;

		template<class TDerived>
		TDerived* Resolve(const std::string& key, const Component* origin) const
		{
			Object* result = Lookup(key)->Resolve(origin, true);
			assert(!result || dynamic_cast<TDerived*>(result));
			return static_cast<TDerived*>(result);
		}

		template<class TDerived>
		TDerived* Resolve(const Component* origin) const
		{
			return static_cast<TDerived*>(Lookup(typeid(TDerived))->Resolve(origin, true));
		}

		template<>
		Assembly* Resolve(const Component* origin) const
		{
			return &origin->getAssembly();
		}

		std::function<Object* ()> resolver(const std::string& key, const Component* origin, const tinyxml2::XMLElement* source, bool expected = true, bool shallow = true, bool inherit = true) const;

		template<class TDerived>
		std::function<Object* ()> resolver(const Component* origin, const tinyxml2::XMLElement* source, bool expected = true, bool shallow = true, bool inherit = true) const
		{
			return resolver(typeid(TDerived), origin, source, expected, shallow, inherit);
		}

		const Registration* Lookup(const Class* dataClass, bool required = true) const;
		
	protected:
		ObjectFactory() {}

		std::map<std::string, const Registration*> registryByName;
		std::map<std::type_index, const Registration*> registryByType;

		const Registration* Lookup(const std::string& key) const;
		const Registration* Lookup(const std::string& key, const Component* origin, Class* &dataClass, bool required = true) const;
		const Registration* Lookup(const type_info &type) const;

		std::function<Object* ()> resolver(const type_info& type, const Component* origin, const tinyxml2::XMLElement* source, bool expected = true, bool shallow = true, bool inherit = true) const;
	};
}
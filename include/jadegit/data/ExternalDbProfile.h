#pragma once
#include "Object.h"
#include "ObjectValue.h"
#include "RootSchema/ExternalDbProfileMeta.h"

namespace JadeGit::Data
{
	class ExternalDatabase;

	class ExternalDbProfile : public Object
	{
	public:
		ExternalDbProfile(ExternalDatabase& parent, const Class* dataClass);

		ObjectValue<ExternalDatabase* const, &ExternalDbProfileMeta::database> database;
	};
}
#pragma once
#include "Feature.h"
#include "RootSchema/JadeExposedFeatureMeta.h"

namespace JadeGit::Data
{
	class JadeExposedClass;

	class JadeExposedFeature : public Entity
	{
	public:
		JadeExposedFeature(JadeExposedClass* parent, const Class* dataClass, const char* name = nullptr);

		ObjectValue<JadeExposedClass* const, &JadeExposedFeatureMeta::exposedClass> exposedClass;
		Value<std::string> exposedName;
		Value<std::string> exposedType;
		ObjectValue<Feature*, &JadeExposedFeatureMeta::relatedFeature> relatedFeature;

		void Accept(EntityVisitor& v) final;

		const Feature& getOriginal() const final;

	protected:
		void loaded(bool strict, std::queue<std::future<void>>& tasks) final;
	};

	extern template ObjectValue<JadeExposedClass* const, &JadeExposedFeatureMeta::exposedClass>;	
	extern template ObjectValue<Feature*, &JadeExposedFeatureMeta::relatedFeature>;
}
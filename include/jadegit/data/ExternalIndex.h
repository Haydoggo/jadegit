#pragma once
#include "ExternalIndexKey.h"
#include "Array.h"
#include "RootSchema/ExternalIndexMeta.h"

namespace JadeGit::Data
{
	class ExternalTable;

	class ExternalIndex : public ExternalSchemaEntity
	{
	public:
		ExternalIndex(ExternalTable& parent, const Class* dataClass, const char* name);

		ObjectValue<Array<ExternalIndexKey*>, &ExternalIndexMeta::keys> keys;
		ObjectValue<ExternalTable* const, &ExternalIndexMeta::table> table;
	};
}
#pragma once
#include "MethodMeta.h"

namespace JadeGit::Data
{
	class JadeInterfaceMethodMeta : public RootClass<>
	{
	public:
		static const JadeInterfaceMethodMeta& get(const Object& object);
		
		JadeInterfaceMethodMeta(RootSchema& parent, const MethodMeta& superclass);
	
		ExplicitInverseRef* const interfaceImplementors;
	};
};

#pragma once
#include "RoutineMeta.h"

namespace JadeGit::Data
{
	class MethodMeta : public RootClass<>
	{
	public:
		static const MethodMeta& get(const Object& object);
		
		MethodMeta(RootSchema& parent, const RoutineMeta& superclass);
	
		PrimAttribute* const condition;
		PrimAttribute* const conditionSafe;
		ExplicitInverseRef* const controlMethod;
		ExplicitInverseRef* const controlMethodRefs;
		ExplicitInverseRef* const exportedMethodRefs;
		PrimAttribute* const final;
		ExplicitInverseRef* const interfaceImplements;
		PrimAttribute* const lockReceiver;
		PrimAttribute* const methodInvocation;
		PrimAttribute* const partitionMethod;
		PrimAttribute* const subschemaCopyFinal;
		PrimAttribute* const subschemaFinal;
		PrimAttribute* const unitTestFlags;
		PrimAttribute* const updating;
	};
};

#pragma once
#include "SchemaEntityMeta.h"

namespace JadeGit::Data
{
	class DbFileMeta : public RootClass<>
	{
	public:
		static const DbFileMeta& get(const Object& object);
		
		DbFileMeta(RootSchema& parent, const SchemaEntityMeta& superclass);
	
		ExplicitInverseRef* const classMapRefs;
		ExplicitInverseRef* const database;
		PrimAttribute* const excludeFromBackup;
		PrimAttribute* const kind;
		PrimAttribute* const partitionable;
		PrimAttribute* const physicalFileName;
	};
};

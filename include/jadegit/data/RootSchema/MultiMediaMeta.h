#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class MultiMediaMeta : public RootClass<GUIClass>
	{
	public:
		static const MultiMediaMeta& get(const Object& object);
		
		MultiMediaMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const autoSize;
		PrimAttribute* const mediaData;
		PrimAttribute* const mediaName;
		PrimAttribute* const repeat;
		PrimAttribute* const showMenu;
		PrimAttribute* const showMode;
		PrimAttribute* const showName;
		PrimAttribute* const showOpenMenu;
		PrimAttribute* const showPlayBar;
		PrimAttribute* const showPosition;
		PrimAttribute* const showRecord;
		PrimAttribute* const timerPeriod;
		PrimAttribute* const useDotNetVersion;
		PrimAttribute* const zoom;
	};
};

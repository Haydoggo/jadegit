#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class JadeDockBaseMeta : public RootClass<GUIClass>
	{
	public:
		static const JadeDockBaseMeta& get(const Object& object);
		
		JadeDockBaseMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const alignContainer;
		PrimAttribute* const borderHeightBottom;
		PrimAttribute* const borderHeightTop;
		PrimAttribute* const borderWidthLeft;
		PrimAttribute* const borderWidthRight;
		PrimAttribute* const caption;
		PrimAttribute* const drawGrip;
		PrimAttribute* const floatingStyle;
		PrimAttribute* const maximumHeight;
		PrimAttribute* const maximumWidth;
		PrimAttribute* const minimumHeight;
		PrimAttribute* const minimumWidth;
		PrimAttribute* const showResizeBar;
	};
};

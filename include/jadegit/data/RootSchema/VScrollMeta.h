#pragma once
#include "ScrollBarMeta.h"

namespace JadeGit::Data
{
	class VScrollMeta : public RootClass<GUIClass>
	{
	public:
		static const VScrollMeta& get(const Object& object);
		
		VScrollMeta(RootSchema& parent, const ScrollBarMeta& superclass);
	};
};

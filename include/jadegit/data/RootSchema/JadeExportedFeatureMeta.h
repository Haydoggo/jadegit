#pragma once
#include "JadeExportedEntityMeta.h"

namespace JadeGit::Data
{
	class JadeExportedFeatureMeta : public RootClass<>
	{
	public:
		static const JadeExportedFeatureMeta& get(const Object& object);
		
		JadeExportedFeatureMeta(RootSchema& parent, const JadeExportedEntityMeta& superclass);
	
		ExplicitInverseRef* const exportedType;
	};
};

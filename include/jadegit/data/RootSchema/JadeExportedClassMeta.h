#pragma once
#include "JadeExportedTypeMeta.h"

namespace JadeGit::Data
{
	class JadeExportedClassMeta : public RootClass<>
	{
	public:
		static const JadeExportedClassMeta& get(const Object& object);
		
		JadeExportedClassMeta(RootSchema& parent, const JadeExportedTypeMeta& superclass);
	
		ExplicitInverseRef* const importedClassRefs;
		ExplicitInverseRef* const originalClass;
		PrimAttribute* const persistentAllowed;
		PrimAttribute* const sharedTransientAllowed;
		PrimAttribute* const subclassPersistentAllowed;
		PrimAttribute* const subclassSharedTransientAllowed;
		PrimAttribute* const subclassTransientAllowed;
		PrimAttribute* const transient;
		PrimAttribute* const transientAllowed;
	};
};

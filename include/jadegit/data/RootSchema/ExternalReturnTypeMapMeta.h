#pragma once
#include "ExternalScriptElementMapMeta.h"

namespace JadeGit::Data
{
	class ExternalReturnTypeMapMeta : public RootClass<>
	{
	public:
		static const ExternalReturnTypeMapMeta& get(const Object& object);
		
		ExternalReturnTypeMapMeta(RootSchema& parent, const ExternalScriptElementMapMeta& superclass);
	};
};

#pragma once
#include "RelationalAttributeMeta.h"

namespace JadeGit::Data
{
	class RAMethodMeta : public RootClass<>
	{
	public:
		static const RAMethodMeta& get(const Object& object);
		
		RAMethodMeta(RootSchema& parent, const RelationalAttributeMeta& superclass);
	};
};

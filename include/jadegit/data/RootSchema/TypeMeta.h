#pragma once
#include "SchemaEntityMeta.h"

namespace JadeGit::Data
{
	class TypeMeta : public RootClass<>
	{
	public:
		static const TypeMeta& get(const Object& object);
		
		TypeMeta(RootSchema& parent, const SchemaEntityMeta& superclass);
	
		ExplicitInverseRef* const collClassRefs;
		ExplicitInverseRef* const constantRefs;
		ExplicitInverseRef* const consts;
		PrimAttribute* const final;
		ExplicitInverseRef* const methods;
		PrimAttribute* const persistentAllowed;
		ExplicitInverseRef* const propertyRefs;
		ExplicitInverseRef* const schema;
		PrimAttribute* const sharedTransientAllowed;
		PrimAttribute* const subclassPersistentAllowed;
		PrimAttribute* const subclassSharedTransientAllowed;
		PrimAttribute* const subclassTransientAllowed;
		PrimAttribute* const subschemaFinal;
		ExplicitInverseRef* const subschemaTypes;
		ExplicitInverseRef* const superschemaType;
		PrimAttribute* const transientAllowed;
	};
};

#pragma once
#include "JadeExportedEntityMeta.h"

namespace JadeGit::Data
{
	class JadeExportedTypeMeta : public RootClass<>
	{
	public:
		static const JadeExportedTypeMeta& get(const Object& object);
		
		JadeExportedTypeMeta(RootSchema& parent, const JadeExportedEntityMeta& superclass);
	
		ExplicitInverseRef* const exportedConstants;
		ExplicitInverseRef* const exportedMethods;
		ExplicitInverseRef* const exportedProperties;
		ExplicitInverseRef* const package;
	};
};

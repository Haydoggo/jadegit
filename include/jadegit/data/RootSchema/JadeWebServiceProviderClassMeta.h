#pragma once
#include "JadeWebServicesClassMeta.h"

namespace JadeGit::Data
{
	class JadeWebServiceProviderClassMeta : public RootClass<>
	{
	public:
		static const JadeWebServiceProviderClassMeta& get(const Object& object);
		
		JadeWebServiceProviderClassMeta(RootSchema& parent, const JadeWebServicesClassMeta& superclass);
	
		PrimAttribute* const secureService;
	};
};

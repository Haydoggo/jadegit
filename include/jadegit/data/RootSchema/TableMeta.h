#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class TableMeta : public RootClass<GUIClass>
	{
	public:
		static const TableMeta& get(const Object& object);
		
		TableMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const allowDrag;
		PrimAttribute* const allowResize;
		PrimAttribute* const autoSize;
		PrimAttribute* const columns;
		PrimAttribute* const defaultRowHeight;
		PrimAttribute* const displayHotKey;
		PrimAttribute* const dropDown;
		PrimAttribute* const expandedHeight;
		PrimAttribute* const fixed3D;
		PrimAttribute* const fixedColumns;
		PrimAttribute* const fixedRows;
		PrimAttribute* const gridColor;
		PrimAttribute* const gridLines;
		PrimAttribute* const readOnly;
		PrimAttribute* const rows;
		PrimAttribute* const scrollBars;
		PrimAttribute* const selectMode;
		PrimAttribute* const sheets;
		PrimAttribute* const showFocus;
		PrimAttribute* const stretch;
		PrimAttribute* const tabActiveColor;
		PrimAttribute* const tabInactiveColor;
		PrimAttribute* const tabKey;
	};
};

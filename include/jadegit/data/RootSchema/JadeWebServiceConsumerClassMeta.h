#pragma once
#include "JadeWebServicesClassMeta.h"

namespace JadeGit::Data
{
	class JadeWebServiceConsumerClassMeta : public RootClass<>
	{
	public:
		static const JadeWebServiceConsumerClassMeta& get(const Object& object);
		
		JadeWebServiceConsumerClassMeta(RootSchema& parent, const JadeWebServicesClassMeta& superclass);
	
		PrimAttribute* const useSOAP12;
		PrimAttribute* const endPointURL;
		PrimAttribute* const useAsyncCalls;
		PrimAttribute* const useNewPrimTypes;
	};
};

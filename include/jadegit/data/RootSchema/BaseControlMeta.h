#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class BaseControlMeta : public RootClass<GUIClass>
	{
	public:
		static const BaseControlMeta& get(const Object& object);
		
		BaseControlMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const clipControls;
		PrimAttribute* const scrollBars;
		PrimAttribute* const transparent;
	};
};

#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class ExternalDbDriverInfoMeta : public RootClass<>
	{
	public:
		static const ExternalDbDriverInfoMeta& get(const Object& object);
		
		ExternalDbDriverInfoMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		PrimAttribute* const catalogLocation;
		PrimAttribute* const catalogName;
		PrimAttribute* const catalogNameSeparator;
		PrimAttribute* const catalogUsage;
		PrimAttribute* const columnAlias;
		PrimAttribute* const correlationName;
		ExplicitInverseRef* const database;
		PrimAttribute* const driverODBCVersion;
		PrimAttribute* const driverVersion;
		PrimAttribute* const identifierCase;
		PrimAttribute* const identifierQuoteChar;
		PrimAttribute* const maxCatalogNameLength;
		PrimAttribute* const maxColumnNameLength;
		PrimAttribute* const maxColumnsInOrderBy;
		PrimAttribute* const maxColumnsInSelect;
		PrimAttribute* const maxIdentifierLength;
		PrimAttribute* const maxSchemaNameLength;
		PrimAttribute* const maxStatementLength;
		PrimAttribute* const maxTableNameLength;
		PrimAttribute* const maxTablesInSelect;
		PrimAttribute* const orderByColumnsInSelect;
		PrimAttribute* const quotedIdentifierCase;
		PrimAttribute* const schemaUsage;
	};
};

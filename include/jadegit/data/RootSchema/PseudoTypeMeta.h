#pragma once
#include "TypeMeta.h"

namespace JadeGit::Data
{
	class PseudoTypeMeta : public RootClass<>
	{
	public:
		static const PseudoTypeMeta& get(const Object& object);
		
		PseudoTypeMeta(RootSchema& parent, const TypeMeta& superclass);
	};
};

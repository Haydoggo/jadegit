#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class ActiveXClassMeta : public RootClass<>
	{
	public:
		static const ActiveXClassMeta& get(const Object& object);
		
		ActiveXClassMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		ExplicitInverseRef* const activeXLibrary;
		PrimAttribute* const activeXName;
		ExplicitInverseRef* const baseClass;
		PrimAttribute* const defaultEventInterfaceGuid;
		PrimAttribute* const defaultInterfaceGuid;
		PrimAttribute* const dotNetName;
		PrimAttribute* const executeMode;
		PrimAttribute* const guid;
		PrimAttribute* const isEventInterface;
		PrimAttribute* const key;
	};
};

#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class ListBoxMeta : public RootClass<GUIClass>
	{
	public:
		static const ListBoxMeta& get(const Object& object);
		
		ListBoxMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const alternatingRowBackColor;
		PrimAttribute* const alternatingRowBackColorCount;
		PrimAttribute* const defaultLineHeight;
		PrimAttribute* const hasPictures;
		PrimAttribute* const hasPlusMinus;
		PrimAttribute* const hasTreeLines;
		PrimAttribute* const integralHeight;
		PrimAttribute* const multiSelect;
		PrimAttribute* const nameSeparator;
		PrimAttribute* const pictureClosed;
		PrimAttribute* const pictureLeaf;
		PrimAttribute* const pictureMinus;
		PrimAttribute* const pictureOpen;
		PrimAttribute* const picturePlus;
		PrimAttribute* const scrollBars;
		PrimAttribute* const scrollHorizontal;
		PrimAttribute* const sortAsc;
		PrimAttribute* const sortCased;
		PrimAttribute* const sorted;
	};
};

#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class JadeXamlControlMeta : public RootClass<GUIClass>
	{
	public:
		static const JadeXamlControlMeta& get(const Object& object);
		
		JadeXamlControlMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const xaml;
	};
};

#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class PictureMeta : public RootClass<GUIClass>
	{
	public:
		static const PictureMeta& get(const Object& object);
		
		PictureMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const clipControls;
		PrimAttribute* const hyperlink;
		PrimAttribute* const picture;
		PrimAttribute* const pictureDisabled;
		PrimAttribute* const pictureDown;
		PrimAttribute* const rotation;
		PrimAttribute* const scrollBars;
		PrimAttribute* const stretch;
		PrimAttribute* const transparent;
		PrimAttribute* const transparentColor;
		PrimAttribute* const upAndDownSwapped;
		PrimAttribute* const webFileName;
	};
};

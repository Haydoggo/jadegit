#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class ActiveXLibraryMeta : public RootClass<>
	{
	public:
		static const ActiveXLibraryMeta& get(const Object& object);
		
		ActiveXLibraryMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		ExplicitInverseRef* const _schema;
		ExplicitInverseRef* const coClasses;
		PrimAttribute* const componentType;
		PrimAttribute* const guid;
		PrimAttribute* const helpFileName;
		PrimAttribute* const status;
		PrimAttribute* const uuid;
	};
};

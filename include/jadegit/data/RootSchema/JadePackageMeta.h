#pragma once
#include "SchemaEntityMeta.h"

namespace JadeGit::Data
{
	class JadePackageMeta : public RootClass<>
	{
	public:
		static const JadePackageMeta& get(const Object& object);
		
		JadePackageMeta(RootSchema& parent, const SchemaEntityMeta& superclass);
	
		ExplicitInverseRef* const schema;
	};
};

#pragma once
#include "LabelMeta.h"

namespace JadeGit::Data
{
	class WebJavaAppletMeta : public RootClass<GUIClass>
	{
	public:
		static const WebJavaAppletMeta& get(const Object& object);
		
		WebJavaAppletMeta(RootSchema& parent, const LabelMeta& superclass);
	
		PrimAttribute* const appletName;
		PrimAttribute* const code;
		PrimAttribute* const codebase;
		PrimAttribute* const horizontalSpace;
		PrimAttribute* const parameters;
		PrimAttribute* const verticalSpace;
	};
};

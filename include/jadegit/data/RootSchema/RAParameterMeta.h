#pragma once
#include "RelationalAttributeMeta.h"

namespace JadeGit::Data
{
	class RAParameterMeta : public RootClass<>
	{
	public:
		static const RAParameterMeta& get(const Object& object);
		
		RAParameterMeta(RootSchema& parent, const RelationalAttributeMeta& superclass);
	};
};

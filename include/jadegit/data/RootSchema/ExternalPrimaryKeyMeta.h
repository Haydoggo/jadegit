#pragma once
#include "ExternalSchemaEntityMeta.h"

namespace JadeGit::Data
{
	class ExternalPrimaryKeyMeta : public RootClass<>
	{
	public:
		static const ExternalPrimaryKeyMeta& get(const Object& object);
		
		ExternalPrimaryKeyMeta(RootSchema& parent, const ExternalSchemaEntityMeta& superclass);
	
		ImplicitInverseRef* const columns;
	};
};

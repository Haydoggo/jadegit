#pragma once
#include "LocaleFormatMeta.h"

namespace JadeGit::Data
{
	class DateFormatMeta : public RootClass<>
	{
	public:
		static const DateFormatMeta& get(const Object& object);
		
		DateFormatMeta(RootSchema& parent, const LocaleFormatMeta& superclass);
	
		PrimAttribute* const dayHasLeadingZeros;
		PrimAttribute* const firstDayOfWeek;
		PrimAttribute* const firstWeekOfYear;
		PrimAttribute* const longFormatOrder;
		PrimAttribute* const monthHasLeadingZeros;
		PrimAttribute* const separator;
		CompAttribute* const shortDayNames;
		PrimAttribute* const shortFormatOrder;
		PrimAttribute* const showFullCentury;
	};
};

#pragma once
#include "ExternalSchemaEntityMeta.h"

namespace JadeGit::Data
{
	class ExternalStoredProcedureMeta : public RootClass<>
	{
	public:
		static const ExternalStoredProcedureMeta& get(const Object& object);
		
		ExternalStoredProcedureMeta(RootSchema& parent, const ExternalSchemaEntityMeta& superclass);
	
		PrimAttribute* const catalogName;
		ExplicitInverseRef* const database;
		PrimAttribute* const excluded;
		PrimAttribute* const schemaName;
		PrimAttribute* const type;
	};
};

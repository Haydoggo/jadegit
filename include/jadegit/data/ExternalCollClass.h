#pragma once
#include "CollClass.h"
#include "RootSchema/ExternalCollClassMeta.h"

namespace JadeGit::Data
{
	class ExternalCollClassMap;
	class ExternalDatabase;

	class ExternalCollClass : public CollClass
	{
	public:
		ExternalCollClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass = nullptr);

		ObjectValue<ExternalDatabase*, &ExternalCollClassMeta::externalDatabase> externalDatabase;
		ObjectValue<ExternalCollClassMap*, &ExternalCollClassMeta::externalSchemaMap> externalSchemaMap;
	};

	extern template ObjectValue<ExternalDatabase*, &ExternalCollClassMeta::externalDatabase>;
	extern template ObjectValue<ExternalCollClassMap*, &ExternalCollClassMeta::externalSchemaMap>;
}
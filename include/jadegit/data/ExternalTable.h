#pragma once
#include "ExternalColumn.h"
#include "ExternalForeignKey.h"
#include "ExternalIndex.h"
#include "RootSchema/ExternalTableMeta.h"

namespace JadeGit::Data
{
	class ExternalDatabase;

	class ExternalTable : public ExternalSchemaEntity
	{
	public:
		ExternalTable(ExternalDatabase& parent, const Class* dataClass, const char* name);

		EntityDict<ExternalColumn, &ExternalTableMeta::columns> columns;
		ObjectValue<ExternalDatabase* const, &ExternalTableMeta::database> database;
		EntityDict<ExternalForeignKey, &ExternalTableMeta::foreignKeys> foreignKeys;
		EntityDict<ExternalIndex, &ExternalTableMeta::indexes> indexes;
		ObjectValue<Array<ExternalColumn*>, &ExternalTableMeta::specialColumns> specialColumns;
	};
}
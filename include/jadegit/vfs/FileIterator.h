#pragma once
#include "File.h"

namespace JadeGit
{
	class AnyFileIterator;

	class FileIterator
	{
	public:
		FileIterator() noexcept;
		FileIterator(std::unique_ptr<AnyFileIterator>&& iter);
		FileIterator(const FileIterator& iter);
		FileIterator(FileIterator&& iter);
		virtual ~FileIterator();

		FileIterator& operator=(const FileIterator& iter);

		/* References current file handle */
		const File& operator*() const;
		const File* operator->() const;

		FileIterator& operator++();
		bool operator==(const FileIterator& iter) const;
		bool operator!=(const FileIterator& iter) const;

	protected:
		std::unique_ptr<AnyFileIterator> iter;
		File current;
	};

	// Range based support
	FileIterator begin(FileIterator iter) noexcept;
	FileIterator end(const FileIterator&) noexcept;
}
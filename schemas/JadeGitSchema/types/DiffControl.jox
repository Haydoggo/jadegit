<GUIClass name="DiffControl" id="ad0abe11-04e9-49aa-82c4-065f9acf8f28">
    <implementedInterfaces>
        <JadeInterface name="IDiffCallbacks"/>
    </implementedInterfaces>
    <superclass name="BaseControl"/>
    <transient>true</transient>
    <subclassSharedTransientAllowed>true</subclassSharedTransientAllowed>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <abstract>true</abstract>
    <text>Abstract control for displaying diffs.
Sublasses build dynamic controls for display.</text>
    <DbClassMap file="_usergui"/>
    <Constant name="ColourBackAdd" id="09182d89-c7e3-44fd-9fbc-ba5407641702">
        <source>13169355</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="ColourBackChanged" id="b51feb98-e4e9-4c67-acbf-8e7c87be0963">
        <source>11599871</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="ColourBackDefault" id="f1a55a6d-af26-4535-9df5-b55674852a91">
        <source>15269887</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="ColourBackPad" id="b932b9a5-526c-4bb5-b4b3-8cb7ea43d72d">
        <source>13750737</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="ColourBackRemove" id="0858894c-0b9c-4c5e-be4d-f80e1a1d327f">
        <source>15526141</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="ColourForeAdd" id="c2231b16-c487-4055-a9f7-3104792bd410">
        <source>29952</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="ColourForeDefault" id="7f4e0cfe-ecdc-4b59-8490-1135f999c3a9">
        <constantUsages>
            <GlobalConstant name="Black"/>
        </constantUsages>
        <source>Black</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="ColourForeRemove" id="e1435036-6b40-4a63-a650-193bd0cc782b">
        <constantUsages>
            <GlobalConstant name="Red"/>
        </constantUsages>
        <source>Red</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="NavigateBack" id="3153a2f9-7a20-4180-a734-984c47ff1575">
        <source>-1</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="NavigateNext" id="58232bbd-b0ed-40c3-87a4-a2d23a93d319">
        <source>1</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="NavigateToFirst" id="e7b87c78-a976-4ec2-a5a9-df5a973a7dae">
        <source>1</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="NavigateTotLast" id="59bcf988-5f95-460e-9f69-17f913536cd9">
        <constantUsages>
            <GlobalConstant name="Max_Integer"/>
        </constantUsages>
        <source>Max_Integer</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="OutlinePad" id="e00aa0f5-383c-4de5-8657-c9ce692a85d2">
        <source>8</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="OutlinePadScroll" id="2fa32398-8dd7-43bd-9ba1-03b16e9df9bf">
        <source>18</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="StyleHighlight" id="3cb0738f-33b4-49e0-95cf-7a7223b5c26c">
        <source>2</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="LineMarkLinesMaxSize" id="36aeb7a7-2622-4566-8242-d5b62b3541a0">
        <source>16300</source>
        <type name="Integer"/>
    </Constant>
    <ImplicitInverseRef name="formatNew" id="9a6cd46d-13b2-49f0-a784-6128461153db">
        <embedded>true</embedded>
        <type name="DiffFormat"/>
        <access>protected</access>
        <text>Stores the 'new' line info</text>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="formatOld" id="128755c2-bc94-4e2c-af84-67b613dbca48">
        <embedded>true</embedded>
        <type name="DiffFormat"/>
        <access>protected</access>
        <text>Stores the 'prior' line info</text>
    </ImplicitInverseRef>
    <CompAttribute name="linesNew" id="0a6236df-5d3f-4117-bca4-ccba515c1600">
        <type name="IntegerArray"/>
        <access>protected</access>
        <text>Captures the line info from the diff callback</text>
    </CompAttribute>
    <CompAttribute name="linesOld" id="8d7f5035-b877-48ff-9e9f-ea2c989d57bf">
        <type name="IntegerArray"/>
        <access>protected</access>
        <text>Captures the line info from the diff callback</text>
    </CompAttribute>
    <ImplicitInverseRef name="outline" id="d72cd07d-3756-465e-8716-7bd891e1fa2e">
        <embedded>true</embedded>
        <type name="Table"/>
        <access>protected</access>
        <text>Control which provides a birdseye view of the changes.</text>
    </ImplicitInverseRef>
    <PrimAttribute name="firstVisibleLine" id="49c977b7-c2f6-4be5-9ebc-05dc7d0799bc">
        <embedded>true</embedded>
        <type name="Integer"/>
    </PrimAttribute>
    <PrimAttribute name="rawView" id="acb54580-fad2-42a8-af20-5e371ec7c42d">
        <embedded>true</embedded>
        <type name="Boolean"/>
    </PrimAttribute>
    <JadeMethod name="delete" id="161c0f69-818d-4f4c-a93c-ff48a43aba03">
        <updating>true</updating>
        <executionLocation>client</executionLocation>
        <source>delete() updating, clientExecution;

vars

begin
	delete formatNew;
	delete formatOld;
end;
</source>
    </JadeMethod>
    <JadeMethod name="diff_Binary_cb" id="bf3e9d33-f36e-4e02-9672-f9d1364ea6f6">
        <interfaceImplements>
            <JadeInterfaceMethod name="IDiffCallbacks::binary_cb"/>
        </interfaceImplements>
        <source>diff_Binary_cb(delta : DiffDelta; binary : DiffBinary) protected;

vars

begin

end;
</source>
        <access>protected</access>
        <Parameter name="delta">
            <type name="DiffDelta"/>
        </Parameter>
        <Parameter name="binary">
            <type name="DiffBinary"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="diff_file_cb" id="e0de41f7-fa59-4925-a424-2a43066ff5ca">
        <interfaceImplements>
            <JadeInterfaceMethod name="IDiffCallbacks::file_cb"/>
        </interfaceImplements>
        <source>diff_file_cb(delta : DiffDelta; progress : Real) protected;

vars

begin

end;
</source>
        <access>protected</access>
        <Parameter name="delta">
            <type name="DiffDelta"/>
        </Parameter>
        <Parameter name="progress">
            <type name="Real"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="diff_hunk_cb" id="88e7d089-62cd-4e2e-80b3-674e32236a53">
        <interfaceImplements>
            <JadeInterfaceMethod name="IDiffCallbacks::hunk_cb"/>
        </interfaceImplements>
        <source>diff_hunk_cb(delta : DiffDelta; hunk : DiffHunk) protected;

vars

begin

end;
</source>
        <access>protected</access>
        <Parameter name="delta">
            <type name="DiffDelta"/>
        </Parameter>
        <Parameter name="hunk">
            <type name="DiffHunk"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="diff_line_cb" id="933b81ca-9dea-4332-9b4f-a6185076f508">
        <interfaceImplements>
            <JadeInterfaceMethod name="IDiffCallbacks::line_cb"/>
        </interfaceImplements>
        <source>diff_line_cb(delta : DiffDelta; hunk : DiffHunk; line : DiffLine) protected;

vars

begin
	linesNew.add( line.new_lineno );
	linesOld.add( line.old_lineno );
end;

</source>
        <access>protected</access>
        <Parameter name="delta">
            <type name="DiffDelta"/>
        </Parameter>
        <Parameter name="hunk">
            <type name="DiffHunk"/>
        </Parameter>
        <Parameter name="line">
            <type name="DiffLine"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="navigateDirection" id="ff964d69-f254-4dc1-989d-0b4f06095a72">
        <updating>true</updating>
        <source>navigateDirection(direction : Integer): Boolean updating;
// bounces up or down through the changes, which wlil reposition the textbox(es)

vars
	newLine		: Integer;
	startLine	: Integer;
	endLine		: Integer;
	linesVisible: Integer;
	
begin
	linesVisible:= (height / textMain.getLineHeight(1)).Integer - 1;
	startLine	:= textMain.firstVisibleLine;
	endLine		:= startLine + linesVisible - 2;
	newLine		:= formatNew.changeSection(startLine, endLine, direction);
	
	if newLine &gt; 0 then
		textLineIntoView(newLine );
		return true;
	else
		return false;
	endif;
end;
</source>
        <Parameter name="direction">
            <type name="Integer"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="navigateToChange" id="cba912da-0d06-4414-a915-e10488203aad">
        <updating>true</updating>
        <source>navigateToChange(changeNumber: Integer) updating;
// bounces to the param change number, which will reposition the textbox(es)
vars
	line	: Integer;
	
begin
	line	:= formatNew.changeLine(changeNumber);
	textLineIntoView(line);
end;


</source>
        <Parameter name="changeNumber">
            <type name="Integer"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="outlineBuild" id="46e8ad83-af6e-4522-b778-1bea1e99e274">
        <updating>true</updating>
        <source>outlineBuild() updating, protected;
// creates the control for showing the birdseye view

begin
	delete outline;
	
	create outline transient;

	outline.name			:= "outline";
	outline.width			:= 20;
	outline.rows			:= 0;
	outline.fixedRows		:= 0;	
	outline.columns			:= 3;
	outline.fixedColumns	:= 0;	
	outline.visible			:= true;
	outline.gridLines		:= false;
	outline.selectMode		:= Table.SelectMode_None;
	outline.parent			:= self;
	outline.scrollBars		:= Table.ScrollBars_None;
	outline.backColor		:= ColourBackDefault;
	form.addControl( outline );	
	outline.columnVisible[1]	:= false;
	outline.columnWidth[2]:= (outline.width/2).Integer;
	outline.columnWidth[3]:= outline.columnWidth[2];
	outline.setEventMapping( "click", "outlineClicked" );
end;
</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="outlineClicked" id="3146393b-84a7-4222-b3c9-ffe500d1c0ed">
        <updating>true</updating>
        <source>outlineClicked() updating;
// birdeye view has been clicked, we need to move the textbox(es) to this position

vars
	lineNumber	: Integer;
	
begin
	lineNumber	:= outline.getCellText( outline.row, 1 ).Integer;
	outline.column	:= 1;	// hides the row selected
	textLineIntoView( lineNumber );
	
	outline.refresh();
end;
</source>
    </JadeMethod>
    <JadeMethod name="outlinePadding" id="3ddfdff8-05a2-4555-a6b8-07f40d5cfba4">
        <source>outlinePadding(): Boolean protected;
// returns true if the outline view needs to pad (grey) lines.

begin
	return true;
end;
</source>
        <access>protected</access>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="outlinePopulate" id="7e05b247-24f0-4541-b90a-6a40aa73205d">
        <updating>true</updating>
        <source>outlinePopulate() updating, protected;
// populates the outline view
// The table is reduced down to very small rows and we set these to similar colours to the textbox(es)

constants
	RowHeight = 3;

vars
	line		: Integer;
	row			: Integer;
	rowStart	: Integer;
	rowStop		: Integer;
	numLines 	: Integer;
	fmtOld		: Character;
	fmtNew		: Character;
	
begin
	// hide the outline if we can see everything anyway
	numLines	:= formatNew.lineFormat.size;
	if textMain.getLineHeight(1) * numLines &lt; outline.height then
		outline.visible	:= false;
		return;
	endif;
	outline.visible			:= true;
	
	// default all the row heights	
	outline.rows				:= (outline.height / RowHeight).Integer;
	foreach row in 1 to outline.rows do
		outline.rowHeight[row]	:= RowHeight; 
	endforeach;
	
	// colour all the rows
	foreach line in 1 to numLines do
		fmtOld	:= formatOld.lineFormat[line];
		fmtNew	:= formatNew.lineFormat[line];
		
		rowStart:= ((line / numLines) * outline.rows).Integer.max(1);
		rowStop	:= (((line+1) / numLines) * outline.rows).Integer.max(1).min(outline.rows);
		
		foreach row in rowStart to rowStop - 1 do
			outline.setCellText( row, 1, line.String );
		endforeach;
		
		if fmtOld &lt;&gt; DiffFormat.LineNormal 
		or fmtNew &lt;&gt; DiffFormat.LineNormal then
		
			// force a row to be shown
			row	:= rowStart;
			if row = rowStop then
				if rowStop = 1 then
					rowStop	:= 2;
				else
					row		:= row - 1;
				endif;
			endif;
			while row &lt; rowStop do
				if fmtOld = DiffFormat.LinePad then
					if outlinePadding then
						outline.accessCell( row, 2 ).backColor	:= ColourBackPad;
					else
						outline.accessCell( row, 2 ).backColor	:= ColourBackAdd;
					endif;
				elseif fmtOld = DiffFormat.LineChanged then
					outline.accessCell( row, 2 ).backColor	:= ColourBackRemove;
				endif;
				if fmtNew = DiffFormat.LinePad then
					if outlinePadding() then
						outline.accessCell( row, 3 ).backColor	:= ColourBackPad;
					else
						outline.accessCell( row, 3 ).backColor	:= ColourBackRemove;
					endif;
				elseif fmtNew = DiffFormat.LineChanged then
					outline.accessCell( row, 3 ).backColor	:= ColourBackAdd;
				endif;
				row  += 1;
			endwhile;
		endif;
	endforeach;
end;</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="populate" id="ebf42f50-3081-4ba8-8853-4806234f8611">
        <controlMethod name="Control::populate"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>populate(cntrl: Control input; delta: DiffDelta io): Boolean updating, protected;
// builds dynamic controls to display the diff content

vars
	oldText		: String;
	newText		: String;

begin
	linesNew.clear();
	linesOld.clear();
	
	delete formatOld;
	delete formatNew;
	formatOld	:= create DiffFormat( not rawView) transient;
	formatNew	:= create DiffFormat( not rawView) transient;
	
	if delta.old_file &lt;&gt; null then
		oldText	:= delta.old_file.content();
	endif;
	if delta.new_file &lt;&gt; null then
		newText	:= delta.new_file.content();
	endif;
	
	formatOld.load(oldText);
	formatNew.load(newText);
	
	if oldText &lt;&gt; null 
	or newText &lt;&gt; null then
		delta.foreach_(self);
	endif;
	
	formatLines();
	
	outlineBuild();
		
	textBuild();
	
	textPopulate();
	
	outlinePopulate();
		
	return true;
end;
</source>
        <access>protected</access>
        <Parameter name="cntrl">
            <usage>input</usage>
            <type name="Control"/>
        </Parameter>
        <Parameter name="delta">
            <usage>io</usage>
            <type name="DiffDelta"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="resized" id="bacc336c-4133-467c-9b4e-35a0ac14f5f8">
        <updating>true</updating>
        <source>resized() updating;
// event called to reposition the controls

begin

end;
</source>
    </JadeMethod>
    <JadeMethod name="textCreate" id="5a0f7ce8-4402-4177-a8b9-aaa793965628">
        <updating>true</updating>
        <source>textCreate(): JadeTextEdit protected, updating;
// create a textbox control

vars
	tbx	: JadeTextEdit;
	
begin
	create tbx transient;
	tbx.readOnly		:= true;
	tbx.fontName		:= "Courier New";
	tbx.fontSize		:= 9;
	tbx.borderStyle		:= BorderStyle_None;
	tbx.parentAspect	:= ParentAspect_AnchorBottom;
	tbx.backColor		:= ColourBackDefault;
	tbx.viewLinenumbers	:= true;
	tbx.markerMargin	:= true;
	tbx.parent		    := self;
	
	form.addControl( tbx );
	return tbx;
end;
</source>
        <access>protected</access>
        <ReturnType>
            <type name="JadeTextEdit"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="textLineIntoView" id="23046ac7-a679-4151-87a2-4eb9ca86950b">
        <updating>true</updating>
        <source>textLineIntoView( line : Integer ) updating, protected;
// repositions the text view to the param linenumber

constants	
	LinesBeforeActual = 8;
				  
vars
	topLine	: Integer;
	
begin
	topLine	:= (line - LinesBeforeActual).max(0);

	firstVisibleLine	:= topLine;
end;
</source>
        <access>protected</access>
        <Parameter name="line">
            <type name="Integer"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="textMain" id="11fe30f9-6314-4a91-9ec9-6ca63dbdbaf8">
        <source>textMain(): JadeTextEdit protected, abstract;
// the default textbox used for display
</source>
        <abstract>true</abstract>
        <access>protected</access>
        <ReturnType>
            <type name="JadeTextEdit"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="windowCreated" id="185c5a12-b0c6-45ce-aac1-9b0b5f4b84a0">
        <controlMethod name="Control::windowCreated"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <executionLocation>client</executionLocation>
        <source>windowCreated(cntrl: Control input; persistCntrl: Control) updating, clientExecution;

begin
	inheritMethod(cntrl, persistCntrl);
	
	name			:= "diffControl";
	borderStyle		:= BorderStyle_None;
	transparent		:= true;
	parentAspect	:= ParentAspect_StretchBoth;
end;
</source>
        <Parameter name="cntrl">
            <usage>input</usage>
            <type name="Control"/>
        </Parameter>
        <Parameter name="persistCntrl">
            <type name="Control"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="changeCounts" id="1069e6aa-9ba0-4b82-99c1-04c20dbf2e14">
        <source>changeCounts( changes	   : Integer output;
              linesAdded   : Integer output;
			  linesRemoved : Integer output);

// counts of the adds/removes for the text comparison
			  
vars
	ignoreResult	: Integer;
	
begin
	formatNew.changeCounts( changes, linesAdded, ignoreResult );
	formatOld.changeCounts( changes, linesRemoved, ignoreResult );
end;
</source>
        <Parameter name="changes">
            <usage>output</usage>
            <type name="Integer"/>
        </Parameter>
        <Parameter name="linesAdded">
            <usage>output</usage>
            <type name="Integer"/>
        </Parameter>
        <Parameter name="linesRemoved">
            <usage>output</usage>
            <type name="Integer"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="textBuild" id="c29ab1d2-e745-4b22-89b3-857682cc871f">
        <updating>true</updating>
        <source>textBuild() protected, updating, abstract;
// build all textbox controls
</source>
        <abstract>true</abstract>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="formatLines" id="f12c1f0a-bee9-41fc-bb53-893a9e4e7f6a">
        <updating>true</updating>
        <source>formatLines() updating, protected, abstract;

</source>
        <abstract>true</abstract>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="textPopulate" id="34b807b2-ce14-40d2-8f1c-0ed4fe45760a">
        <updating>true</updating>
        <source>textPopulate() updating, protected;
// builds the text and formatting into the textbox(es)

begin

end;
</source>
        <access>protected</access>
    </JadeMethod>
    <JadeMethod name="textHighlightKeys" id="2a1e6e26-4627-44b5-b2b6-2c68ff2dfd1d">
        <source>textHighlightKeys( text : JadeTextEdit input) protected;

constants
	StyleMask = 2;

vars
	posStart  : Integer;
	posEnd	  : Integer;
	source	  : String;
	
begin
	// set the style for highlighting
	text.setStyleAttributes( StyleHighlight, "Tahoma", text.fontSize, -1, -1, 1, 0, 0, 0 );

	source	:= text.text;

	// name="HIGHLIGHT" id=
	posStart		:= 1;
	while true do
		posStart	 := source.pos( ' name="', posStart );
		if posStart = 0 then
			break;
		endif;
		posStart += 7;
		posEnd := source.pos( '"', posStart+1 );
		if posEnd &gt; 0 then
			if source[posEnd:5] = '" id=' then			// id signifies important info
				if source.reversePosIndex( "&lt;JadeMethod ", posStart ) &lt;= source.reversePosIndex( Cr, posStart ) then
					text.setTextRangeToStyle( posStart-1, posEnd-posStart, StyleHighlight, StyleMask );
				endif;
			endif;
		endif;
	endwhile;
	
	//         &lt;source&gt;myHIGHLIGHT()
	posStart		:= 1;
	while true do
		
		// highlight name
		posStart	 := source.pos( CrLf &amp; '        &lt;source&gt;', posStart );
		if posStart = 0 then
			break;
		endif;
		posStart += 18;
		posEnd := source.pos( '(', posStart );
		if posEnd &gt; 0 and posEnd &lt; source.pos( Cr, posStart ) then
			if posStart &lt; posEnd then
				text.setTextRangeToStyle( posStart-1, posEnd-posStart, 2, 2 );
			endif;
		endif;
	endwhile;
end;
</source>
        <access>protected</access>
        <Parameter name="text">
            <usage>input</usage>
            <type name="JadeTextEdit"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="firstVisibleLine" id="9ad99bdb-0348-41fe-8cd4-88baaabc54a5">
        <methodInvocation>mapping</methodInvocation>
        <source>firstVisibleLine(set: Boolean; _value: Integer io) mapping;

vars

begin
	if textMain &lt;&gt; null then
		if set then
			textMain.firstVisibleLine	:= _value;
		else
			_value	:= textMain.firstVisibleLine;
		endif;
	endif;
end;</source>
        <Parameter name="set">
            <type name="Boolean"/>
        </Parameter>
        <Parameter name="_value">
            <usage>io</usage>
            <type name="Integer"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="trimArray" id="59debf24-267e-45e8-8cee-d9f1d0877b1f">
        <source>trimArray( array   : Array input;
		   maxSize : Integer ) protected;
		   
// JadeTextEdit can crash if certain limits are exceeded

begin
	while array.size &gt; maxSize do
		array.removeAt( maxSize + 1 );
	endwhile;
end;</source>
        <access>protected</access>
        <Parameter name="array">
            <usage>input</usage>
            <type name="Array"/>
        </Parameter>
        <Parameter name="maxSize">
            <type name="Integer"/>
        </Parameter>
    </JadeMethod>
    <DevControlTypes name="DiffControl">
        <windowClass>AlBaseControl</windowClass>
        <DevControlProperties name="backBrush">
            <cntrlType>picture</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="backBrushStyle">
            <cntrlType>list</cntrlType>
            <optionsList>
                <value>0 - tile</value>
                <value>1 - stretch</value>
                <value>2 - center</value>
                <value>3 - stretch proportional</value>
            </optionsList>
        </DevControlProperties>
        <DevControlProperties name="backColor">
            <cntrlType>color</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="borderColorSingle">
            <cntrlType>color</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="borderStyle">
            <cntrlType>list</cntrlType>
            <optionsList>
                <value>0 - None</value>
                <value>1 - Single</value>
                <value>2 - 3D sunken</value>
                <value>3 - 3D raised</value>
            </optionsList>
        </DevControlProperties>
        <DevControlProperties name="bubbleHelp">
            <cntrlType>multilined-string</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="clipControls">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="description">
            <cntrlType>multilined-string</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="dragCursor">
            <cntrlType>picture</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="enabled">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="focusBackColor">
            <cntrlType>color</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="focusForeColor">
            <cntrlType>color</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="fontBold">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="fontItalic">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="fontName">
            <cntrlType>font</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="fontSize">
            <cntrlType>font-size</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="fontStrikethru">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="fontUnderline">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="foreColor">
            <cntrlType>color</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="height">
            <cntrlType>signed-integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="helpContextId">
            <cntrlType>integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="helpKeyword">
            <cntrlType>string</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="ignoreSkin">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="left">
            <cntrlType>signed-integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="mouseCursor">
            <cntrlType>picture</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="mousePointer">
            <cntrlType>list</cntrlType>
            <optionsList>
                <value> 0 - Default</value>
                <value> 1 - Arrow</value>
                <value> 2 - Cross</value>
                <value> 3 - I-Beam</value>
                <value> 4 - Cursor</value>
                <value> 5 - Size</value>
                <value> 6 - Size NE SW</value>
                <value> 7 - Size N S</value>
                <value> 8 - Size NW SE</value>
                <value> 9 - Size W E</value>
                <value>10 - Up Arrow</value>
                <value>11 - HourGlass</value>
                <value>12 - No Drop</value>
                <value>13 - Drag</value>
                <value>14 - Horizontal line move</value>
                <value>15 - Vertical Line move</value>
                <value>16 - Hand Pointing</value>
            </optionsList>
        </DevControlProperties>
        <DevControlProperties name="name">
            <cntrlType>name</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="parent">
            <cntrlType>parent</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="parentAspect">
            <cntrlType>list</cntrlType>
            <optionsList>
                <value> 0 - None</value>
                <value> 1 - Stretch Right</value>
                <value> 2 - Stretch Bottom</value>
                <value> 3 - Stretch Both</value>
                <value> 4 - Anchor Right</value>
                <value> 6 - Anchor Right &amp; Stretch Bottom</value>
                <value> 8 - Anchor Bottom</value>
                <value> 9 - Anchor Bottom &amp; Stretch Right</value>
                <value>12 - Anchor Both</value>
                <value>16 - Center Horz</value>
                <value>18 - Center Horz &amp; Stretch Bottom</value>
                <value>24 - Center Horz &amp; Anchor bottom</value>
                <value>32 - Center Vert</value>
                <value>33 - Center Vert &amp; Stretch Right</value>
                <value>36 - Center Vert &amp; Anchor Right</value>
                <value>48 - Center Both</value>
            </optionsList>
        </DevControlProperties>
        <DevControlProperties name="parentBottomOffset">
            <cntrlType>signed-integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="parentRightOffset">
            <cntrlType>signed-integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="relativeHeight">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="relativeLeft">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="relativeTop">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="relativeWidth">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="scrollBars">
            <cntrlType>list</cntrlType>
            <optionsList>
                <value>0 - None</value>
                <value>1 - Horizontal</value>
                <value>2 - Vertical</value>
                <value>3 - Both</value>
            </optionsList>
        </DevControlProperties>
        <DevControlProperties name="securityLevelEnabled">
            <cntrlType>integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="securityLevelVisible">
            <cntrlType>integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="show3D">
            <cntrlType>list</cntrlType>
            <optionsList>
                <value>0 - use application default</value>
                <value>1 - Not 3D</value>
                <value>2 - Use 3D</value>
                <value>3 - Use borderStyle</value>
            </optionsList>
        </DevControlProperties>
        <DevControlProperties name="skinCategoryName">
            <cntrlType>string</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="tabIndex">
            <cntrlType>integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="tabStop">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="tag">
            <cntrlType>string</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="top">
            <cntrlType>signed-integer</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="transparent">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="visible">
            <cntrlType>boolean</cntrlType>
        </DevControlProperties>
        <DevControlProperties name="width">
            <cntrlType>signed-integer</cntrlType>
        </DevControlProperties>
    </DevControlTypes>
</GUIClass>

## 0.12.0 (2024-01-01)

### Added (12 changes)

- [feat(scm): new branch from commit](jadelab/jadegit@649c33d9ec689d6cf631ef7dbd0b9627fbce50ea) ([merge request](jadelab/jadegit!200))
- [feat(scm): checkout commit](jadelab/jadegit@000abd9cdbf5b7c3fd0fbb58682ea963f8673b87) ([merge request](jadelab/jadegit!200))
- [feat(scm): commit graph](jadelab/jadegit@c8696a85f2b8a03464acb02d2e556a296e7a0a92) by @JohnBeaufoy ([merge request](jadelab/jadegit!186))
- [feat(scm): compare display formatting](jadelab/jadegit@b2280e86990e0dd839bb5db2441762c5c83945a9) by @JohnBeaufoy ([merge request](jadelab/jadegit!192))
- [feat: add JADE 2022 SP2](jadelab/jadegit@98e53c4e2d341dfdab54e34294737c5466a0a185) ([merge request](jadelab/jadegit!190))
- [feat: support for external databases](jadelab/jadegit@e972dca386c50c51555d1b47fafa4f01465f64a6) ([merge request](jadelab/jadegit!173))
- [feat(scm): add patch builder](jadelab/jadegit@f2883c71af078b562f002265e71ec3bfa5b42ddb) ([merge request](jadelab/jadegit!183))
- [feat(scm): compare commits](jadelab/jadegit@6938400da6dd4c2c2579c6d517be7da02b3d7cae) ([merge request](jadelab/jadegit!184))
- [feat(scm): view commit details](jadelab/jadegit@66a5e1d89977582d110c64d30503624b4a07c4e2) by @JohnBeaufoy ([merge request](jadelab/jadegit!181))
- [feat(deploy): add jari deployment format](jadelab/jadegit@e287f69a5867ed5d9565e83a4b1f1ab68831cea7) ([merge request](jadelab/jadegit!176))
- [feat(console): add registry commands](jadelab/jadegit@8f60cd3b5313a72ea55fe597b58483bdd696ddad) ([merge request](jadelab/jadegit!175))
- [feat: add JADE 2022 SP1](jadelab/jadegit@f044d4ffff32f022c351338f4af79bb74c298e7a) ([merge request](jadelab/jadegit!174))

### Fixed (24 changes)

- [fix(scm): hard reset to commit](jadelab/jadegit@94e784e45e583c4813eb612dd6a15be668bde73a) ([merge request](jadelab/jadegit!200))
- [fix(extract): remove missing entities during deep extract](jadelab/jadegit@0e0db7c8aed5b7636872bce0e85f6777c00c32b9) ([merge request](jadelab/jadegit!199))
- [fix(scm): ui compare crashes jade if size exceeded](jadelab/jadegit@1d47d90b1c11c0f769670e9d0cb5a4ecc857186c) by @JohnBeaufoy ([merge request](jadelab/jadegit!198))
- [fix(registry): fetch missing commits during build](jadelab/jadegit@78edd4e91ed50a21aaf35e917a14f8469c308a35) ([merge request](jadelab/jadegit!197))
- [fix(build): handle removing control property](jadelab/jadegit@904d6ce9ae699150dbca5ba62c0ee9f279324c55) ([merge request](jadelab/jadegit!196))
- [fix(extract): resolve existing control properties](jadelab/jadegit@d4a7d044b4092db76c8d2c71f3425edc2a2d0a6d) ([merge request](jadelab/jadegit!195))
- [fix(build): imported key type](jadelab/jadegit@233d743e62cb4b2b4663494fe5d606725cbe8f30) ([merge request](jadelab/jadegit!187))
- [fix(scm): view commit details fixes](jadelab/jadegit@269f0da2499908bba9cd04d97de18365629fdc96) by @JohnBeaufoy ([merge request](jadelab/jadegit!191))
- [fix(scm): handle initial commit](jadelab/jadegit@68539cd77f740d756b6a048f0b398ff3e006849d) ([merge request](jadelab/jadegit!185))
- [refactor(data): external component libraries](jadelab/jadegit@9c752b6588ceda3554ca9c8579ec58d3dc999a11) ([merge request](jadelab/jadegit!179))
- [fix(scm): invalid object exception on commit](jadelab/jadegit@5bad58a2ba48dadde3e3fe90e013dc21155b5c80) by @BDeath ([merge request](jadelab/jadegit!178))
- [fix(dev): handle removing applications](jadelab/jadegit@5713f9f0affb37c4b83d5b248b7579052c7326ad) ([merge request](jadelab/jadegit!174))
- [fix(data): preserve whitespace](jadelab/jadegit@a9849b0c9821bccec5e9623c29f8b2ec46a11ab2) ([merge request](jadelab/jadegit!172))
- [fix(extract): web event classes](jadelab/jadegit@3aaffb1309125b1b9817ba6ee6a7e685a1f13c4e) ([merge request](jadelab/jadegit!171))
- [fix(extract): suppress mapping methods](jadelab/jadegit@7f39e35a1649019e82bd336c7297255d737c279d) ([merge request](jadelab/jadegit!171))
- [fix(build): handle cyclic event method dependency](jadelab/jadegit@ff1b02b5390fb56344d0a16147ec18ca0abd3c8f) ([merge request](jadelab/jadegit!169))
- [fix(extract): web service method parameter wsdl name](jadelab/jadegit@9639a93c7a3a4425be1e2db1c662c9665d75c7c7)
- [fix(extract): add property xml attribute flag](jadelab/jadegit@2ae57cf3849545fdd0e9337da90a83b3a23709ba)
- [fix(data): handle invalid date & time values](jadelab/jadegit@62e946eb3065a3c81d746566a722151fe74aa61d) ([merge request](jadelab/jadegit!167))
- [fix(data): add ProgressDialog class to RootSchema](jadelab/jadegit@e332db1dbed0fd37ccd5e7b786a4d8012316b276) ([merge request](jadelab/jadegit!167))
- [fix(data): handle parsing negative date](jadelab/jadegit@b66032107c2be0ec89b56ce480a1d6589f4e1478) ([merge request](jadelab/jadegit!167))
- [fix(extract): default web service header direction](jadelab/jadegit@250ca5393117e75c9191faf0398aa3bf1ef78e32) ([merge request](jadelab/jadegit!167))
- [fix(build): ignore superclass subschema copies during class move analysis](jadelab/jadegit@6ff9b1fcd3e6372547ec1abd0769f9bdf1c181b7) ([merge request](jadelab/jadegit!165))
- [fix(extract): ignore invalid menu item shortcut key](jadelab/jadegit@57c34f84ec5e853e4a9554025726ab502266548f) by @mason.drew ([merge request](jadelab/jadegit!164))

### Changed (12 changes)

- [chore(upgrade): libgit2 v1.7.1](jadelab/jadegit@8a72df317ca48d656bb057eaf5e46185f94226c2) ([merge request](jadelab/jadegit!197))
- [feat(console): add original commit option to preserve entity ids during extract](jadelab/jadegit@7d28d0d44ba2d7632cf1474f194b7179d4abe2cd) ([merge request](jadelab/jadegit!195))
- [refactor(build): reduce unnecessary updates](jadelab/jadegit@9d28dfc432ae07bac5be92c053bbe7afaf621d98) ([merge request](jadelab/jadegit!193))
- [refactor(build): target platform version](jadelab/jadegit@c03506ef7c5e33c7b999f577aec9caa3c6f0eb15) ([merge request](jadelab/jadegit!190))
- [feat(scm): add option to force push](jadelab/jadegit@a7282fb7412af42608e5ab86ff70b169dc3f8d40) by @BDeath ([merge request](jadelab/jadegit!188))
- [feat(scm): update progress dialog](jadelab/jadegit@65588c01cb609b3afa9b35d07ee048bdfcc1c625) ([merge request](jadelab/jadegit!183))
- [refactor(deploy): remove manifest commands from pwsh build](jadelab/jadegit@c72228ff89622438bd1ea01766dc6fc77b5128ea) ([merge request](jadelab/jadegit!177))
- [feat(scm): restore repositories from registry](jadelab/jadegit@953e761793247c7f62b06e7f9ec4668822cfce52) ([merge request](jadelab/jadegit!177))
- [feat(scm): use registry during clone to restore repository](jadelab/jadegit@6512d38eed368146fc999d257fd16b4e8f145f70) ([merge request](jadelab/jadegit!175))
- [feat(deploy): update registry during pwsh deployment](jadelab/jadegit@056026e1783edd1acd47a7eb874b542b8e4d4aa4) ([merge request](jadelab/jadegit!175))
- [feat(dev): prevent unsupported rename/delete operations subject to platform version](jadelab/jadegit@795779aa3309d893019baeab32e4967194cbdb69) ([merge request](jadelab/jadegit!174))
- [refactor(scm): merge schemas submodule](jadelab/jadegit@f8bcfd0016b133ac5f7b754de8ee486fed17e9e9) ([merge request](jadelab/jadegit!166))

## 0.11.1 (2023-05-24)

### Fixed (1 change)

- [fix(scm): update schemas submodule location](jadelab/jadegit@0d800812f38fdfdb5bc900571f7c458206b2e077)

## 0.11.0 (2023-03-18)

### Added (9 changes)

- [feat(deploy): add pwsh deployment format](jadelab/jadegit@98045866faee80b4cbb3f7582a3b882ad763a020) ([merge request](jadelab/jadegit!158))
- [feat(console): add option to log progress using format specified](jadelab/jadegit@e22e35d0dd28d1803a407c9da319f62aa06e6069) ([merge request](jadelab/jadegit!157))
- [feat: support for deferred collection maintenance](jadelab/jadegit@a3aabea633a397e7192774ae95e8900cff223e15) ([merge request](jadelab/jadegit!155))
- [feat: add JADE 2022](jadelab/jadegit@8ce999a0f9146609fb9169a8d893f7da638a570d) ([merge request](jadelab/jadegit!150))
- [feat(data): support for timestamp attributes](jadelab/jadegit@dff53e250c598e2c38c712538ce1ab09bc7cf541) ([merge request](jadelab/jadegit!145))
- [feat(scm): add diff proxy classes](jadelab/jadegit@51cc2009e583aeb44af35238d9fc57927e041266) ([merge request](jadelab/jadegit!142))
- [feat(scm): add history form](jadelab/jadegit@48ad051240d504106fc75c8d1c8766dbe3e18971) ([merge request](jadelab/jadegit!138))
- [feat(config): deployment scripts](jadelab/jadegit@9fb3c460e78af7c5e6b5e8d599678c7b85f458f7) ([merge request](jadelab/jadegit!133))
- [Add generic xml deployment format](jadelab/jadegit@a80bbdbb7f789adbd24aca5fc9a7d3dedbe1c78d) ([merge request](jadelab/jadegit!132))

### Fixed (21 changes)

- [fix(scm): handle entity removal while parent still being added](jadelab/jadegit@3251160e7b23e73bc4e3541c5f23f68e003d2f6b)
- [fix(build): unit test method options](jadelab/jadegit@08f87e152f57a96f709b7a9e95bd07cee49eb334) ([merge request](jadelab/jadegit!163))
- [fix(scm): commit timestamp conversion](jadelab/jadegit@7f620b7a97e3a8832450fd48465f23cc5cf61101) ([merge request](jadelab/jadegit!162))
- [fix(build): web application properties](jadelab/jadegit@26e7c14d3bb00513c57b7341fbe35098dc5d7262) ([merge request](jadelab/jadegit!161))
- [fix(data): menu item shortcut keys](jadelab/jadegit@e1d08343f320a19d509d7c5270787479fbcc4410) ([merge request](jadelab/jadegit!161))
- [fix(build): web service header wsdl name](jadelab/jadegit@612de700515e4b1f8f87a381b5f615773e63f040) ([merge request](jadelab/jadegit!161))
- [fix(extract): add collection member type wsdl name](jadelab/jadegit@4da47e8e4cc95a847da78758e527878c2f3733fc) ([merge request](jadelab/jadegit!161))
- [fix(extract): add property xml nillable flag](jadelab/jadegit@fd1e9d6dc12dabb12b134a9f7251e820024cb577) ([merge request](jadelab/jadegit!161))
- [fix(build): exposed class auto added flag](jadelab/jadegit@4c7e7a3591d6d8c01a2e6b6c3b4ca4a99a00a3d4) ([merge request](jadelab/jadegit!161))
- [fix(extract): add menu item command id](jadelab/jadegit@e0a6e5989b3ca0358d40f2395b2c024f577e5ac9) ([merge request](jadelab/jadegit!161))
- [fix(extract): add printer/web form flags](jadelab/jadegit@041069ae5e9798ffb4f5f5a6a309667a3f95f780) ([merge request](jadelab/jadegit!161))
- [fix(dev): handle debugging scripts](jadelab/jadegit@f99225403d489aeda5e592bafaf386e7f2dda463) ([merge request](jadelab/jadegit!160))
- [fix(console): suppress progress bar during pipeline](jadelab/jadegit@e26e97eca9f9f89cdc89742bb9591fc6d5f6bbbf) ([merge request](jadelab/jadegit!157))
- [fix(data): add property name to runtime error during write](jadelab/jadegit@a48e519da1a921f85a6a6753801182b40e194d40)
- [fix(build): package documentation text](jadelab/jadegit@67939eb374d1d8e6f58a829d9880eecd5a0f4bc3) by @TangataRereke ([merge request](jadelab/jadegit!156))
- [fix(build): use control specific ddx format for primitive arrays](jadelab/jadegit@a8afd0098f0f0d74dece25f85dbba2787e143244) ([merge request](jadelab/jadegit!151))
- [fix(scm): identify indirect child dependencies during rename](jadelab/jadegit@58abbb93679aa6022c00f81140eda1eebb9562b0) ([merge request](jadelab/jadegit!149))
- [fix(build): infer update mode for implied references from inverse](jadelab/jadegit@be598dc13e2110258f64e9490de1d0fa7e08660c)
- [fix(dev): handle interface renames](jadelab/jadegit@8c9b9a7e5a4ff30afca0fe1100cfd422d0e3f40b) ([merge request](jadelab/jadegit!142))
- [fix(devenv): add auth library to thin client files](jadelab/jadegit@7434a04276c374870382223d64c9164996ed7550) ([merge request](jadelab/jadegit!140))
- [fix(data): load menu items](jadelab/jadegit@f66e6c66419fd9bb042b8809c006eafa24d437e4) ([merge request](jadelab/jadegit!135))

### Changed (10 changes)

- [refactor(scm): config backend](jadelab/jadegit@d6b6dcb23826a1bd30bfa5bba2277b77a546d0f9) ([merge request](jadelab/jadegit!153))
- [fix(auth): approve/reject credentials](jadelab/jadegit@4e90a4a1b6e1ae8e1db13c9b1b6bdb90bd128dc7) ([merge request](jadelab/jadegit!147))
- [fix: disable strict http parser mode](jadelab/jadegit@75323dda4b72946313278168e6888c1e24f2b0e8) ([merge request](jadelab/jadegit!147))
- [refactor(scm): remote operations](jadelab/jadegit@d4d81fdb2337b7e0381bbb4332ebcef05db35259) ([merge request](jadelab/jadegit!146))
- [Merge branch 'ksaul/101-references' into 'main'](jadelab/jadegit@872322a6539310edbae18e69818c4deebaf7764a) ([merge request](jadelab/jadegit!144))
- [refactor(scm): store index data in common map file](jadelab/jadegit@b16a5a8a3ee1ba03d273a7cb244adb1fc214ad05) ([merge request](jadelab/jadegit!141))
- [refactor(scm): use worktree for change tracking](jadelab/jadegit@8e7d4e0b9d8f32a7f17b16da4191f85802fc29ea) ([merge request](jadelab/jadegit!141))
- [feat(dev): allow changes to JadeGitSchema when repo has been cloned](jadelab/jadegit@919945879ef64b54f8b987eab7ee82cafe884f3f) ([merge request](jadelab/jadegit!137))
- [refactor(scm): install schema using submodule to build deployment](jadelab/jadegit@6c63bd57f3d50629f2369ebf882e42daa1f9368b) ([merge request](jadelab/jadegit!134))
- [feat(schema): run scripts during internal deployment](jadelab/jadegit@cde492b86397b7abe8bbf703163286e00b12d425) ([merge request](jadelab/jadegit!133))

### Removed (1 change)

- [refactor(console): remove option to force install](jadelab/jadegit@953a75296f3fe1b9c8e615bdf06d892357244d96) ([merge request](jadelab/jadegit!134))

## 0.10.0 (2022-08-23)
### Added
- Support for imported classes with local features.
- Console command for cloning repository.
- Support for .jadegit configuration file using toml++.
- Console deploy manifest commands to store/retrieve origin/commit during deployment.

### Changed
- Prevent redundant subschema class copy extracts.
- Use 'jox' file extension for xml format.
- Use subfolders for schemas & forms, and lowercase all entity subfolder names.
- Use system configuration file.
- Removed option to set target JADE version during build (now defined by config).

### Fixed
- Renamed/split library into jadegitdev/jadegitscm so entry points are found by JADE when commands invoked via console.
- Derive logs directory from executable path (rather than relative to current working directory).

## 0.9.0 (2022-04-09)
### Added
- Support for HTML documents.
- Support for dynamic properties.
- Support for JADE 2020 SP1.

### Changed
- Refactored inverse maintenance within proxy data model using metadata.
- Use 'main' as default branch name.
- Resolve current session using process instead of username.
- Refactored id allocation & storage using stduuid.

### Fixed
- Use integer input type for helpContextId properties during extract.
- Ignore interface references during extract.
- Handle resolving inherited/cloned locale entities.
- Handle deleting subschema subclasses before base class.
- Define static schema properties for subschema member key dictionary usage.
- Ignore duplicated system files during extract.
- Confirm order during initial extract when objects may be created out of order.
- Mark entities as inferred when they may not be extracted.
- Reset data items when ignored during extract.
- Ignore attribute precision unless decimal type during extract.
- Clean up non-static schemas when unloading during save.
- Handle encoding accent characters within documentation text.
- Handle defining custom event methods before any element of dependent classes.
- Handle resolving inherited applications.
- Handle loading circular package references.

### Removed
- Support for JADE 2018.
- Support for JADE 2020.

## 0.8.0 (2021-04-26)
### Added
- Support for JADE 2020.
- Support for unicode environments.
- Ability for console to open JADE database connection.
- Console commands to open bare repository, switch branch, extract schemas & commit.
- CardSchema definition (now excluded from source control by default).

### Changed
- Refactored console to support shell mode for series of commands while connected to JADE database.
- Pushing new branches will now default to only or oldest remote when 'origin' doesn't exist.
- Embedded database definition within schema file to support names which don't match default.

### Fixed
- ActiveXControl properties which cannot be accessed without creating ActiveX object will now be skipped during extract.
- Allow text boxes on remote dialog to scroll horizontally.
- Support for adding/deleting remotes.
- Support for deleting translatable strings.

# 0.7.0 (2020-11-05)
### Added
- Support for showing progress.
- Support for ActiveX libraries.
- Support for .NET libraries.
- Support for type methods.

### Changed
- Evaluate prior/next version when schema selected.
- Install process to allow re-orgs when required due to control changes.

### Fixed
- Handling of schema deregistration and resetting branch state when unloading repositories.
- Handling of form & control renames.
- Handling of form deletions.
- Handling of copying forms.
- Support for moving classes (better handling for new classes being inserted above existing).
- Staging renames with updates to related entities.
- Handling of super-interface mapping dependencies.

## 0.6.0 (2020-06-07)
### Added
- Support for relational views.
- Support for tracking changes to latest schema version.

### Changed
- Updated internal deployments to use JadeSchemaLoader & JadeReorgApp.
- Refactored build process to generate DDX data files.
- Improved support for locales (changing schema default & converting base locale to/from clone).
- Simplified DbClassMap format (now written as single element within class file).
- Refactored authentication using presentation client library to retrieve git credentials (jadegitauth).
- Refactored RootSchema metadata generation to simplify future updates.

### Fixed
- Handling of duplicates allowed option for collection classes.
- Handling of collection class option to map all instances to selected map file.
- Handling of dependency between form control methods and custom control event methods.
- Handling of cross-dependencies between global constants spanning multiple categories.

### Removed
- Support for JADE 2016.

## 0.5.0 (2020-02-05)
### Added
- Support for C# exposures.
- Support for web services.
- Support for schema views (local use only).

### Changed
- Refactored how references are stored to support branch folders.
- Refactored schema file generation to include modified timestamps.
- Refactored console build to load data from git repository.
- Removed use of default 'JADE' root folder.

### Fixed
- Support for cloning repositories from local filesystem.
- Support for cloned locale definitions.
- Support for references to imported types & imported interface mappings.
- Removal of local branches (config backend refactored to support removals).
- Schema deletion now handled via separate command files (avoids problems when internal deployment is being restarted).

## 0.4.0 (2019-11-02)
### Added
- Ability to restart & abort internal deployments (used to load/unload/reset branches & repositories).
- Ability to stage & commit changes selectively.
- Ability to reset index with changes being kept (unstage all).

### Changed
- Refactored internal deployment build process to store files persistently.
- Schema versioning & re-org functions are now allowed, provided there is no internal deployment outstanding.
- Building internal deployments are now prevented when there's an outstanding re-org.
- Major entity file path now used to resolve parent during load, removing need to store qualified name within file.

## 0.3.0 (2019-07-04)
### Added
- Support for adding existing schemas to source control.
- Support for custom control subclasses.
- Support for building schema updates using command line utility with previous/latest source folders.
- Support for imported packages.
- Support for locale formats.
- Implemented branch reset functions to discard changes.
- Implemented repository reset function to unload all branches with complete schema reload.

### Changed
- Deep extract now performed when new entity is added, removing the need to track child changes.
- Consolidated into one shared library and/or command line executable (jadegit.dll & jadegit.exe).
- Suppress extract of shallow/proxy entities which cannot be loaded (i.e. subschema copies of an interface).

### Fixed
- Support for external methods.
- Support for removing class interface mappings.
- Support for libraries & external functions.

## 0.2.0 (2019-04-16)
### Added
- Support for JADE 2018.
- Support for translatable strings.
- Support for schema/repository associations.
- Support for unloading repositories.
- Support for removing schemas.
- Support for renaming schema entities.
- Support for conditional collections.

### Changed
- Improved extract process using data mappings which can be explicitly defined or automatically generated as required.
- Improved support for subschemas.
- Improved support for forms (renames, event methods, menu items).
- Improved support for adding/removing inverses between references.
- Jade-Git Explorer now uses notifications to refresh/show changes (possibly caused by another user).

### Fixed
- Replaced use of paramSetString with paramSetCString to explicitly initialize DskParam format.
- Suppress 'unknown property' errors with warnings being logged instead (identified during automatic data mapping).
- Suppress Jade-Git Explorer when starting IDE in administration mode.

## 0.1.0 (2018-12-15)
### Added
- This CHANGELOG file to document notable changes going forward.
- jadegit UI explorer, which runs alongside the JADE IDE (refer to README for configuration steps), provides:
  - Ability to create/clone repositories.
  - Ability to manage branches, with push & pull support for remote repositories.
  - Ability to track changes to basic schema entities (supporting add/update/delete operations).
  - Ability to commit changes, during which affected entities are re-extracted from JADE and converted to source control friendly format.
- Ability to build schemas from jadegit repository using command line utility (independently of JADE IDE).

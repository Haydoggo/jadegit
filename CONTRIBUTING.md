# Welcome to jade-git!

We're aiming to do great things for JADE developers using libgit2, and would welcome any help in doing so!

## Licensing

By contributing to jade-git, you agree to release your contribution under
the terms of the license.  All code is released under the [LGPL v3.0+](COPYING.LESSER).

Refer to the [README](README) for further explanation of the licensing strategy.

## How To

This project doesn't have a specific style guide as such, suffice to say, it'd be best match the common theme/style that's emerging.

Documentation is seriously lacking to define the design/direction, so the most useful help at this stage might be to raise issues, whether they be potential bugs where it looks like things should already be working, or simply specific topics for discussion where it'd be useful to know how we plan on implementing support for a specific scenario/feature.
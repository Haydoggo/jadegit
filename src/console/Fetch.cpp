#include "Command.h"
#include "CommandRegistration.h"
#include "Progress.h"
#include "Session.h"
#include <jade/Iterator.h>
#include <jade/Transaction.h>
#include <schema/Remote.h>
#include <schema/data/Repository.h>
#include <schema/data/Root.h>
#include <schema/RemoteCallbacks.h>
#include <schema/RemoteCallbacksPayload.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Console
{
	class Fetch : public Command
	{
	public:
		static constexpr const char* description = "Download objects and refs from remote repositories";

		Fetch(CLI::App& cmd, Session& session) : Command(cmd, session)
		{
			auto group = cmd.add_option_group("remotes");
			group->add_flag("--all", "Fetch from all remotes");
			group->add_option("remote", this->remote, "Name of remote to fetch from");
			group->require_option(1);

			cmd.add_option("--access-token", this->access_token, "Access token to be used for authorization");
		}

		void execute() final
		{
			Progress progress(*this, session);

			if (!execute(progress))
				throw jadegit_operation_aborted();
		}

	protected:
		bool execute(IProgress& progress) const
		{
			// Fetch for repository already open
			if (session.repo)
				return execute(session.repo, progress);

			// Fetch for all repositories
			Collection<> repositories(Schema::Root::get(), TEXT("repositories"));

			if (!progress.start(repositories.size()))
				return false;

			Schema::RepositoryData repo;
			Iterator<Schema::RepositoryData> iter(repositories);
			while (iter.next(repo))
			{
				progress.message("Updating " + repo.GetName());
				if (!execute(repo.open(), progress))
					return false;
			}

			return progress.finish();
		}

		bool execute(const unique_ptr<git_repository>& repo, IProgress& progress) const
		{
			// Fetch from specified remote
			if (!this->remote.empty())
			{
				unique_ptr<git_remote> ptr;
				git_throw(git_remote_lookup(git_ptr(ptr), repo.get(), this->remote.c_str()));
				return execute(ptr, progress);
			}
			
			// Fetch from all remotes
			unique_ptr<git_strarray> remotes = make_unique<git_strarray>();
			git_throw(git_remote_list(remotes.get(), repo.get()));

			if (!progress.start(remotes->count))
				return false;

			for (int i = 0; i < remotes->count; i++)
			{
				unique_ptr<git_remote> ptr;
				git_throw(git_remote_lookup(git_ptr(ptr), repo.get(), remotes->strings[i]));
				if (!execute(ptr, progress))
					return false;
			}

			return progress.finish();
		}

		bool execute(const unique_ptr<git_remote>& remote, IProgress& progress) const
		{
			if (!progress.start(1, "Fetching from " + string(git_remote_name(remote.get()))))
				return false;
			
			// Configure fetch options
			git_fetch_options options;
			git_throw(git_fetch_options_init(&options, GIT_FETCH_OPTIONS_VERSION));

			// Setup remote callbacks
			Schema::RemoteCallbacks callbacks(&progress);
			Schema::RemoteCallbacksPayload payload(options.callbacks, callbacks);

			// Suppress updating fetch head
			options.update_fetchhead = 0;

			// Auto detect proxy
			options.proxy_opts.type = git_proxy_t::GIT_PROXY_AUTO;

			// Use vector to collect custom headers
			vector<char*> custom_headers;

			// Use access token if supplied
			string authorization_header;
			if (!access_token.empty())
			{
				// Disable credentials callback
				options.callbacks.credentials = nullptr;

				// Define authorization header with base64 encoded <user>:<token>
				string usertoken = ":" + access_token;
				authorization_header = "Authorization: Basic " + static_cast<string>(Data::Binary(reinterpret_cast<const Byte*>(usertoken.c_str()), usertoken.size()));;
				custom_headers.push_back(const_cast<char*>(authorization_header.c_str()));
			}

			// Populate custom headers
			options.custom_headers = { custom_headers.data(), custom_headers.size() };

			// Fetch
			Transaction transaction;
			git_strarray refspecs = { 0 };
			if (git_throw(git_remote_fetch(remote.get(), &refspecs, &options, NULL)) == GIT_EUSER)
				return false;	// Operation cancelled via callback

			transaction.commit();
			return progress.finish();
		}

	private:
		string remote;
		string access_token;
	};
	static CommandRegistration<Fetch> registration("fetch");
}
#include "Application.h"
#include "CommandRegistry.h"
#include "Session.h"
#include <jadegit/Exception.h>
#include <Platform.h>

using namespace std;

namespace JadeGit::Console
{
	string jadegit_version()
	{
		ostringstream v;
		v << JadeGit::build_version << " (" << JadeGit::build_timestamp << ")";
		return v.str();
	}

	class Application : public CLI::App
	{
	public:
		Application() : CLI::App("", "jadegit")
		{
#if USE_JADE
			// setup options for opening database
			add_option("-p, --path", this->dbPath, "JADE database path")->required();
			add_option("-i, --ini", this->iniPath, "JADE initialization file")->required();
			add_option("-s, --server", this->serverType, "JADE server mode (singleUser|multiUser)")->capture_default_str()->excludes(
				add_flag("-m, --multiUser", this->multiUser, "JADE multi-user mode")
			);
#endif

			// setup non-interactive options
			if (!session.interactive)
			{
				add_option("--log-progress-format", session.log_progress_format, "Log progress percent using std::format specified");
			}
		}

		int run(int argc, char* argv[])
		{
			bool shell = false;
			int result = 0;
			do
			{
				result = 0;

				try
				{
					// Cleanup prior set of subcommands 
					for (auto& cmd : get_subcommands(nullptr))
						remove_subcommand(cmd);

					// Setup help flags
					set_help_flag("-h,--help", "Display help");
					set_help_all_flag("--help-all", "Display all help");

					// Setup subcommands
					CommandRegistry::get().setup(*this, session, shell);

					if (shell)
					{
						// Setup command to exit shell
						auto exit = add_subcommand("exit", "Exit shell");
						exit->callback([&]() { shell = false; });

						// Setup help subcommand
						bool help_all = false;
						add_subcommand("help", "Display help")->callback([&]()
						{
							clear();
							cout << help("", help_all ? CLI::AppFormatMode::All : CLI::AppFormatMode::Normal);
						})->add_flag("-a,--all", help_all);

						// Replace version option with subcommand
						add_subcommand("version", "Display version")->callback([&]()
						{
							throw(CLI::CallForVersion(jadegit_version(), 0));
						});

						// Prompt for next command
						cout << "jadegit> ";
						string commandline;
						getline(cin, commandline);
						session.started = chrono::high_resolution_clock::now();
						clear();
						parse(commandline);
					}
					else
					{
						// add version flag
						set_version_flag("-v, --version", &jadegit_version)->description("Display version");

						session.started = chrono::high_resolution_clock::now();
						parse(argc, argv);

						// Switch into shell mode for repeated commands when no subcommands provided upfront
						if (get_subcommands().empty())
						{
							shell = true;

							// Reset name to omit from help produced within shell
							name();

							// Remove all base command options (help, version & connection options)
							for (auto& opt : get_options())
								remove_option(opt);
						}
					}
				}
				catch (const CLI::Error& e)
				{
					result = exit(e);
				}
				catch (const JadeGit::jadegit_exception& e)
				{
					cout << endl;
					cout << "ERROR: " << e << endl;
					result = e.code ? e.code : 1;
				}
				catch (const std::runtime_error& e)
				{
					cout << endl;
					cout << e.what() << endl;
					result = 1;
				}
			} while (shell);

			return result;
		}

	private:
		Session session;

#if USE_JADE
		void pre_callback() final
		{
			using namespace Jade;

			// open database
			if (!session.db)
			{
				if (multiUser)
					serverType = "multiUser";

				session.db = make_unique<DbContext>(dbPath.c_str(), iniPath.c_str(), serverType);
			}

			// initialize application
			if (!session.app)
			{
				// always initialize RootSchema app initially to determine whether JadeGitSchema installed rather than assuming/exceptioning when it's not
				session.app = make_unique<AppContext>(&session.db->node, TEXT("RootSchema"), TEXT("RootSchemaApp"));

				// use console app if installed
				if constexpr (use_schema)
				{
					// lookup schema
					DskSchema rootSchema(&RootSchemaOid);
					DskSchema schema;
					rootSchema.getSubschema(TEXT("JadeGitSchema"), schema);

					// start console app if installed
					if (!schema.isNull())
					{
						session.app.reset();
						session.app = make_unique<AppContext>(&session.db->node, TEXT("JadeGitSchema"), TEXT("JadeGitConsole"));
					}
				}
			}
		}

		string dbPath;
		string iniPath;
		string serverType = "singleUser";
		bool multiUser = false;
#endif
	};

	int run(int argc, char* argv[])
	{
		return Application().run(argc, argv);
	}
}
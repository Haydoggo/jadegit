#include <jadegit/data/Development.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/RootSchema.h>
#include "ObjectRegistration.h"

namespace JadeGit::Data
{
	DEFINE_OBJECT_CAST(DevControlClass);

	static ObjectRegistration<DevControlTypes, GUIClass> devControlTypes("DevControlTypes");
	static ObjectRegistration<DevControlProperties, DevControlTypes> devControlProperties("DevControlProperties");

	std::map<DevControlProperties::Type, const char*> EnumStrings<DevControlProperties::Type>::data =
	{
		{ DevControlProperties::Parent, "parent" },
		{ DevControlProperties::Name, "name" },
		{ DevControlProperties::String, "string" },
		{ DevControlProperties::Boolean, "boolean" },
		{ DevControlProperties::Integer, "integer" },
		{ DevControlProperties::Character, "character" },
		{ DevControlProperties::Color, "color" },
		{ DevControlProperties::Font, "font" },
		{ DevControlProperties::FontSize, "font-size" },
		{ DevControlProperties::Real, "real" },
		{ DevControlProperties::Picture, "picture" },
		{ DevControlProperties::IntegerSigned, "signed-integer" },
		{ DevControlProperties::RealSigned, "signed-real" },
		{ DevControlProperties::StringMultiLined, "multilined-string" },
		{ DevControlProperties::Xaml, "xaml" },
		{ DevControlProperties::ViaPropertyPage, "property-page" },
		{ DevControlProperties::List, "list" }
	};

	DevControlClass::DevControlClass(GUIClass& parent, const Class* dataClass) : Development(parent, dataClass),
		guiClass(&parent)
	{
	}

	DevControlTypes::DevControlTypes(GUIClass& parent, const Class* dataClass, const char* name) : NamedObject(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::devControlTypes), name) {}

	DevControlProperties::DevControlProperties(DevControlTypes& parent, const Class* dataClass, const char* name) : NamedObject(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::devControlProperties), name), 
		parent(&parent)
	{
	}

	DevelopmentMeta::DevelopmentMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "Development", superclass) {}

	DevControlClassMeta::DevControlClassMeta(RootSchema& parent, const DevelopmentMeta& superclass) : RootClass(parent, "DevControlClass", superclass),
		guiClass(NewReference<ExplicitInverseRef>("guiClass", NewType<Class>("GUIClass")))
	{
		guiClass->manual().child().bind(&DevControlClass::guiClass);
	}

	DevControlTypesMeta::DevControlTypesMeta(RootSchema& parent, const DevControlClassMeta& superclass) : RootClass(parent, "DevControlTypes", superclass),
		bitmap(NewBinary("bitmap")),
		bitmapDisabled(NewBinary("bitmapDisabled")),
		bitmapDown(NewBinary("bitmapDown")),
		bitmapID(NewInteger("bitmapID")),
		bitmapOver(NewBinary("bitmapOver")),
		controlPropsNameDict(NewReference<ExplicitInverseRef>("controlPropsNameDict", NewType<CollClass>("ControlPropNameDict"))),
		darkBitmap(NewBinary(Version(20, 0, 1), "darkBitmap")),
		darkBitmapDisabled(NewBinary(Version(20, 0, 1), "darkBitmapDisabled")),
		darkBitmapDown(NewBinary(Version(20, 0, 1), "darkBitmapDown")),
		darkBitmapOver(NewBinary(Version(20, 0, 1), "darkBitmapOver")),
		hideFromPainterControlPalette(NewBoolean("hideFromPainterControlPalette")),
		name(NewString("name", 101)),
		windowClass(NewString("windowClass"))
	{
		// Group development metadata after all others
		subject->bracket(999);

		controlPropsNameDict->automatic().parent().bind(&DevControlTypes::controlProps);
		name->unwritten().bind(&DevControlTypes::name);
	}

	DevControlPropertiesMeta::DevControlPropertiesMeta(RootSchema& parent, const DevelopmentMeta& superclass) : RootClass(parent, "DevControlProperties", superclass),
		cntrlType(NewInteger("cntrlType")),
		name(NewString("name", 101)),
		optionsList(NewProperty<CompAttribute>("optionsList", NewType<CollClass>("JadeLongStringArray"))),
		parent(NewReference<ExplicitInverseRef>("parent", NewType<Class>("DevControlTypes")))
	{
		cntrlType->bind(&DevControlProperties::cntrlType);
		name->unwritten().bind(&DevControlProperties::name);
		optionsList->SetAlias("PainterControlPropertyOptions")->bind(&DevControlProperties::optionsList);
		this->parent->manual().child().bind(&DevControlProperties::parent);
	}
}
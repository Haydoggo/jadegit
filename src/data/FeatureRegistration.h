#include "EntityRegistration.h"

namespace JadeGit::Data
{
	template<class TDerived, class TParent>
	class FeatureRegistration : protected EntityRegistration<TDerived, TParent>
	{
	public:
		using EntityRegistration<TDerived, TParent>::EntityRegistration;
		
		using EntityRegistration<TDerived, TParent>::Resolve;

		TDerived* Resolve(TParent* parent, const std::string& name, bool shallow, bool inherit) const override
		{
			// Iterate through superschema type hierarchy
			TParent* type = parent;
			while (type)
			{
				if (TDerived* feature = EntityRegistration<TDerived, TParent>::Resolve(type, name, shallow, inherit))
					return feature;

				if (!inherit)
					break;

				// Infer static class features (possibly invalid, but cheaper than having to explicitly instantiate all features upfront)
				if constexpr (std::is_constructible_v<TDerived, TParent*, const Class*, const char*>)
				{
					if (!type->superschemaType && type->isStatic())
						return new TDerived(type, nullptr, name.c_str());
				}

				// Get next superschema type to check
				type = static_cast<TParent*>(static_cast<Type*>(type->superschemaType));
			}

			// Feature doesn't exist in superschema type hierarchy (may exist in superclass hierarchy, which we don't currently check)
			return nullptr;
		}
	};

	template<class TDerived, class TParent = Class>
	class PropertyRegistration : protected FeatureRegistration<TDerived, TParent>
	{
		static_assert(std::is_base_of<Class, TParent>(), "Parent is not a class");

	public:
		PropertyRegistration(const char* key) : FeatureRegistration<TDerived, TParent>(key, "property", &Class::properties) {}
	};

	template<class TDerived>
	class MethodRegistration : protected FeatureRegistration<TDerived, Type>
	{
	public:
		MethodRegistration(const char* key) : FeatureRegistration<TDerived, Type>(key, "method", &Type::methods) {}
	};
}
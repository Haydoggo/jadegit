#include <jadegit/data/PseudoType.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/PseudoTypeMeta.h>
#include "TypeRegistration.h"

namespace JadeGit::Data
{
	static TypeRegistration<PseudoType, Schema> registrar("PseudoType", &Schema::pseudoTypes);

	PseudoTypeMeta::PseudoTypeMeta(RootSchema& parent, const TypeMeta& superclass) : RootClass(parent, "PseudoType", superclass) {}

	PseudoType::PseudoType(Schema* parent, const Class* dataClass, const char* name) : Type(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::pseudoType), name)
	{
		schema = parent;
	}

	void PseudoType::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	AnyValue* PseudoType::CreateValue() const
	{
		throw jadegit_exception("Storing pseudo type values is not supported");
	}

	AnyValue* PseudoType::CreateValue(Object& object, const Property& property, bool exclusive) const
	{
		throw jadegit_exception("Storing pseudo type values is not supported");
	}

	void PseudoType::LoadFor(Object &object, const Property& property, const tinyxml2::XMLElement* source, bool strict, std::queue<std::future<void>>& tasks) const
	{
		throw jadegit_exception("Loading pseudo type values is not supported");
	}

	void PseudoType::WriteFor(const Object &object, const Property& property, tinyxml2::XMLNode& parent) const
	{
		throw jadegit_exception("Writing pseudo type values is not supported");
	}
}
#include "Printer.h"
#include "Commit.h"

using namespace std;

namespace JadeGit::Registry
{
	Printer::Printer(std::ostream& os) : os(os)
	{
	}

	bool Printer::visitEnter(const DataT& registry)
	{
		os << endl;

		// Show message when experimental mode has been enabled
		if (registry.experimental)
		{
			os << "  ** experimental mode enabled **" << endl;
			os << endl;
		}

		// Show platform info
		if (registry.platform)
		{
			auto platform = *registry.platform;
			os << "platform: " << platform.version << endl;
			os << " charset: " << EnumNamesCharset()[platform.charset] << endl;
			os << endl;
		}

		return true;
	}

	bool Printer::visitEnter(const RepositoryT& repo)
	{
		if (repo.name.empty() && repo.origin.empty())
		{
			os << "    repo: <unknown>" << endl;
		}
		else
		{
			if (!repo.name.empty())
				os << "    repo: " << repo.name << endl;

			if (!repo.origin.empty())
				os << "  origin: " << repo.origin << endl;
		}

		return true;
	}

	bool Printer::visitLeave(const RepositoryT& repo)
	{
		os << endl;
		return true;
	}

	bool Printer::visit(const CommitT& commit, bool first, bool latest)
	{
		os << (first ? (latest ? "  latest: " : "previous: ") : "          ") << commit << endl;
		return true;
	}

	bool Printer::visit(const SchemaT& schema, bool first)
	{
		os << (first ? " schemas: " : "          ") << schema.name << endl;
		return true;
	}
}
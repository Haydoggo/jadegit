#include "Storage.h"
#include <registry/Root.h>
#include <registry/Validator.h>

using namespace std;
using namespace flatbuffers;

namespace JadeGit::Registry
{
	void Storage::load(Root& registry, const uint8_t* buffer, size_t length) const
	{
		// Verify buffer
		Verifier verifier(buffer, length);
		if (!VerifyDataBuffer(verifier))
			throw jadegit_exception("Cannot load corrupted registry");

		// Unpack
		GetData(buffer)->UnPackTo(&registry);
	}

	void Storage::save(const Root& registry) const
	{
		// Check data valid before saving
		Validator validator;
		registry.accept(validator);

		// Pack data
		FlatBufferBuilder fbb;
		FinishDataBuffer(fbb, Data::Pack(fbb, &registry));

		// Write to storage
		save(fbb.GetBufferPointer(), fbb.GetSize());
	}
}
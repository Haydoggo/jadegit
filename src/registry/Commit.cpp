#include "Commit.h"
#include <jadegit/Exception.h>

using namespace std;

namespace JadeGit::Registry
{
	uint8_t from_hex(char input)
	{
		if (input >= '0' && input <= '9')
			return input - '0';
		if (input >= 'A' && input <= 'F')
			return input - 'A' + 10;
		if (input >= 'a' && input <= 'f')
			return input - 'a' + 10;

		throw invalid_argument("Invalid hex string");
	}

	CommitT make_commit(const std::string& sha)
	{
		CommitT commit;

		for (int i = 0; i < sha.length(); i += 2)
			commit.id.push_back(from_hex(sha[i]) * 16 + from_hex(sha[i + 1]));

		return commit;
	}

	template <typename T>
	string to_hex(const T& input)
	{
		ostringstream output;
		for (const auto& elem : input)
			output << hex << setw(2) << setfill('0') << static_cast<unsigned>(elem);
		return output.str();
	}

	ostream& operator<<(ostream& os, const CommitT& commit)
	{
		os << to_hex(commit.id);
		return os;
	}

	bool operator==(const CommitT& lhs, const CommitT& rhs)
	{
		return lhs.id == rhs.id;
	}

	bool operator==(const CommitT& lhs, const std::string& sha)
	{
		return lhs == make_commit(sha);
	}
}

#if USE_GIT2
#include "Fetch.h"
#include <jadegit/git2.h>

namespace JadeGit::Registry
{
	CommitT make_commit(const git_oid& oid)
	{
		CommitT commit;
		commit.id.assign(oid.id, oid.id + GIT_OID_MAX_SIZE);
		return commit;
	}

	CommitT make_commit(const git_commit& src)
	{
		return make_commit(*git_commit_id(&src));
	}

	bool commit_exists(const git_repository& repo, const CommitT& commit)
	{
		try
		{
			return !!commit_lookup(repo, commit);
		}
		catch (...)
		{
			// Assume commit doesn't exist when attempt to fetch fails
			return false;
		}
	}

	unique_ptr<git_commit> commit_lookup(const git_repository& repo, const CommitT& src)
	{
		git_oid oid = { 0 };
		git_throw(git_oid_fromraw(&oid, src.id.data()));

		unique_ptr<git_commit> commit;
		auto result = git_commit_lookup(git_ptr(commit), const_cast<git_repository*>(&repo), &oid);

		// Fetch the commit if missing
		if (result != GIT_ENOTFOUND)
		{
			git_throw(result);
		}
		else
		{
			try
			{
				fetch(repo, oid);
			}
			catch (runtime_error& e)
			{
				throw jadegit_exception(format("{}, while fetching missing commit ({})", e.what(), git_oid_tostr_s(&oid)));
			}

			// Lookup commit
			git_throw(git_commit_lookup(git_ptr(commit), const_cast<git_repository*>(&repo), &oid));
		}

		return commit;
	}
}
#endif
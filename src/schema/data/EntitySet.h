#pragma once
#include "Entity.h"
#include <jade/ObjectSet.h>

namespace JadeGit::Schema
{
	class GitEntitySet : public Jade::ObjectSet<GitEntity>
	{
	public:
		using ObjectSet::ObjectSet;
		GitEntitySet();
		GitEntitySet(const GitEntity& owner);

	};
}
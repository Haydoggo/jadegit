#include "NodeDict.h"
#include "Directory.h"
#include <jade/Iterator.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	void ReferenceNodeDict::cleanup() const
	{
		Iterator<ReferenceNode> iter(*this);
		ReferenceNode node;
		while (iter.next(node))
		{
			if (node.isDirectory())
			{
				// Cleanup children recursively
				ReferenceNodeDict children = ReferenceDirectory(node.oid).children();
				children.cleanup();

				// Delete empty directories
				if (children.isEmpty())
					node.deleteObject();
			}
		}
	}

	ReferenceData ReferenceNodeDict::init(const string& path) const
	{
		auto pos = path.find("/");
		if (pos == path.length())
			throw jadegit_exception("Invalid path");

		auto name = path.substr(0, pos);
		if (pos == path.npos)
		{
			auto existing = getAtKey<ReferenceData>(name);
			if (!existing.isNull())
				return existing;

			ReferenceData ref;
			DskObjectId parent;
			oid.getParent(parent);
			jade_throw(ref.createObject());
			jade_throw(ref.setProperty(TEXT("parent"), parent));
			ref.setProperty(TEXT("name"), name);
			return ref;
		}

		auto existing = getAtKey<ReferenceDirectory>(name);
		if (!existing.isNull())
			return existing.children().init(path.substr(pos + 1));

		ReferenceDirectory node;
		DskObjectId parent;
		oid.getParent(parent);
		jade_throw(node.createObject());
		jade_throw(node.setProperty(TEXT("parent"), parent));
		node.setProperty(TEXT("name"), name);
		return node.children().init(path.substr(pos + 1));
	}

	ReferenceData ReferenceNodeDict::lookup(const std::string& path) const
	{
		auto pos = path.find("/");
		if (pos == path.length())
			throw jadegit_exception("Invalid path");

		auto name = path.substr(0, pos);
		if (pos == path.npos)
			return getAtKey<ReferenceData>(name);

		auto node = getAtKey<ReferenceDirectory>(name);
		if (node.isNull())
			return ReferenceData();

		return node.children().lookup(path.substr(pos + 1));
	}

	void ReferenceNodeDict::remove(const std::string& path) const
	{
		// Lookup reference to be removed
		auto reference = lookup(path);
		if (reference.isNull())
			return;

		// Delete reference
		reference.deleteObject();

		// Cleanup empty directories
		cleanup();
	}

	void ReferenceNodeDict::swap(std::string a, std::string b) const
	{
		auto left = lookup(a);
		auto right = lookup(b);

		if (!left.isNull())
		{
			if (!right.isNull())
				left.swap(right);
			else
				//	left.rename(b, true);					// TODO: Refactor to remove assumption about root directory
				left.setProperty(TEXT("name"), b);
		}
		else if (!right.isNull())
			//	right.rename(a, true);
			right.setProperty(TEXT("name"), a);
	}

	bool ReferenceNodeDict::write(const git_reference* reference, bool force) const
	{
		if (!force)
		{
			auto existing = lookup(git_reference_name(reference));
			if (!existing.isNull())
				return false;
		}

		init(git_reference_name(reference)).write(reference);
		return true;
	}
}
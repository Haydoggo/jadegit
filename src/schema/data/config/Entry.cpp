#include "Entry.h"
#include "Backend.h"
#include <jade/Iterator.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema::Backend
{
	void jadegit_config_entry__free(git_config_entry* entry)
	{
		// Ignore calls to free directly, entries are freed when shared jadegit_config_entries is freed
		// This assumes iterators & backends are freed after entries are no longer needed
	}

	jadegit_config_entry::jadegit_config_entry(string name, string value) : name(move(name)), value(move(value))
	{
		git_config_entry::name = this->name.c_str();
		git_config_entry::value = this->value.c_str();
		git_config_entry::free = &jadegit_config_entry__free;
	}

	jadegit_config_entries::jadegit_config_entries(const jadegit_config_backend& backend)
	{
		// Populate entries
		copy(backend.data, string());
	}

	jadegit_config_entries::~jadegit_config_entries()
	{
		jadegit_config_entry* current;
		current = first;
		while (current)
		{
			jadegit_config_entry* next = current->next;
			delete current;
			current = next;
		}
	}

	void jadegit_config_entries::add(const std::string& name, const std::string& value)
	{
		jadegit_config_entry* entry = new jadegit_config_entry(name, value);

		if (last)
			last->next = entry;
		else
			first = entry;

		last = entry;

		// TODO: What if there's multivars with same name?
		index[entry->name] = entry;
	}

	void jadegit_config_entries::copy(const ConfigData& data, std::string path)
	{
		// Append name to path
		path += data.name();

		// Copy values
		String value;
		Iterator<String> iter(data.values());
		while (iter.next(value))
			add(path, narrow(value));

		// Copy children recursively
		path += ".";
		copy(data.children(), path);
	}

	void jadegit_config_entries::copy(const ConfigDataDict& data, const std::string& path)
	{
		ConfigData member;
		Iterator<ConfigData> iter(data);
		while (iter.next(member))
			copy(member, path);
	}

	jadegit_config_entry* jadegit_config_entries::get(const char* key) const
	{
		auto iter = index.find(key);
		if (iter != index.end())
			return iter->second;

		return nullptr;
	}
}
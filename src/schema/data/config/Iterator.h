#pragma once
#include "Backend.h"

namespace JadeGit::Schema::Backend
{
	class jadegit_config_entry;
	class jadegit_config_entries;

	class jadegit_config_iterator : public git_config_iterator
	{
	public:
		jadegit_config_iterator(jadegit_config_backend& backend);

		jadegit_config_entry* next = nullptr;

	private:
		std::shared_ptr<jadegit_config_entries> entries;
	};
}
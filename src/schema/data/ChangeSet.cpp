#include "ChangeSet.h"
#include "Entity.h"
#include "Worktree.h"
#include <extract/Assembly.h>
#include <jade/Iterator.h>
#include <schema/ObjectRegistration.h>

using namespace Jade;
using namespace JadeGit::Schema;

namespace Jade
{
	template<>
	Transient<GitChangeSet>::~Transient()
	{
		// Purge transient changes
		GitChange change;
		Iterator<GitChange> changes(*this);
		while (changes.next(change))
		{
			if (change.isTransient() && change.isValid())
				change.deleteObject();
		}

		// Delete transient collection
		deleteObject();
	}
}

namespace JadeGit::Schema
{
	static GitObjectRegistration<GitChangeSet> registration(TEXT("GitChangeSet"));

	GitChangeSet::GitChangeSet() : ObjectSet(registration)
	{
	}

	GitChangeSet::GitChangeSet(const GitEntity& entity)
	{
		entity.getProperty(TEXT("changes"), *this);
	}

	GitChangeSet::GitChangeSet(const WorktreeData& worktree)
	{
		worktree.getProperty(TEXT("changes"), *this);
	}

	GitChangeSet::GitChangeSet(const GitChange& change, const Character* collection)
	{
		change.getProperty(collection, *this);
	}

	std::unique_ptr<Transient<GitChangeSet>> GitChangeSet::Clone() const
	{
		auto clone = std::make_unique<Transient<GitChangeSet>>();

		std::map<DskObjectId, DskObjectId> directory;

		GitChange src;
		Iterator<GitChange> source(*this);
		while (source.next(src))
		{
			GitChange change = Clone(src, directory);
			if (!change.isNull())
				clone->add(&change);
		}

		return clone;
	}

	GitChange GitChangeSet::Clone(const GitChange& src, std::map<DskObjectId, DskObjectId>& directory) const
	{
		// Ignore changes which have already been staged 
		// TODO: Add option to clone/reset changes transiently?
		if (src.isStaged())
			return GitChange();

		// Check for existing clone
		auto iter = directory.find(src.oid);
		if (iter != directory.end())
			return GitChange(iter->second);

		// Clone change, populating references except branch which should not be affected by transient staging
		GitChange clone;
		jade_throw(src.cloneObject(clone, src.oid.getClassNo(), Lifetime::LIFETIME_TRANSIENT, false, __LINE__));
		clone.setLatest(src.getLatest());
		clone.setPrevious(src.getPrevious());

		// Update directory before cloning predecessors
		directory[src.oid] = clone.oid;

		// Clone predecessors
		GitChange predecessor;
		Iterator<GitChange> source(src, TEXT("predecessors"));
		while (source.next(predecessor))
			clone.addPredecessor(Clone(predecessor, directory));

		return clone;
	}

	bool GitChangeSet::stage(Extract::Assembly& assembly) const
	{
		// Preload entities affected by changes
		// This is needed to prevent issues coming about as a result of trying to load
		// entities with references to other entities that are being deleted or renamed
		std::set<DskObjectId> loaded;
		GitChange change;
		Iterator<GitChange> iter(*this);
		while (iter.next(change))
			change.stagePreload(assembly, loaded);

		// No need to continue if there's no changes to stage
		if (loaded.empty())
			return false;

		// Stage changes, deleting & renaming entities in dependency order
		Transient<GitChangeSet> extract;
		jade_throw(iter.reset());
		while (iter.next(change))
			change.stage(assembly, extract);

		// Extract changes, this is handled after renames to prevent inferring/creating duplicates
		if (!extract.isEmpty())
		{
			Iterator<GitChange> iter(extract);
			while (iter.next(change))
				change.stageExtract(assembly);
		}

		// Compact staged changes
		jade_throw(iter.reset());
		while (iter.next(change))
			change.compact();

		return true;
	}

	bool GitChangeSet::stage(const FileSystem& fs, bool transient) const
	{
		// Setup assembly to stage changes
		Extract::Assembly assembly(fs);

		// Stage changes
		if (!(transient ? Clone()->stage(assembly) : stage(assembly)))
			return false;

		// Save changes to file repository
		assembly.save();
		return true;
	}
}
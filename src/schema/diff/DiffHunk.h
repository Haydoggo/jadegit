#pragma once
#include <schema/Proxy.h>

namespace JadeGit::Schema
{
	class DiffHunk : public GitObject<const git_diff_hunk*>
	{
	public:
		using GitObject::GitObject;
		DiffHunk(const git_diff_hunk* ptr);

	};
}
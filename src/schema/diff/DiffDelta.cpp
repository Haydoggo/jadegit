#include "Diff.h"
#include "DiffCallbacks.h"
#include "DiffDelta.h"
#include "DiffFile.h"
#include <schema/ObjectRegistration.h>
#include <git2/patch.h>

namespace JadeGit::Schema
{
	static GitObjectRegistration<DiffDelta> registration(TEXT("DiffDelta"));

	DiffDelta::DiffDelta(const Diff& diff, const git_diff_delta* delta) : GitObject(registration, delta, diff)
	{
		jade_throw(setProperty(TEXT("status"), delta->status));
		jade_throw(setProperty(TEXT("flags"), delta->flags));
		jade_throw(setProperty(TEXT("similarity"), delta->similarity));
		jade_throw(setProperty(TEXT("nfiles"), delta->nfiles));

		DiffFile old_file(*this, TEXT("old_file"), git_oid_is_zero(&delta->old_file.id) ? nullptr : &delta->old_file);
		DiffFile new_file(*this, TEXT("new_file"), git_oid_is_zero(&delta->new_file.id) ? nullptr : &delta->new_file);
	}

	DiffDelta::DiffDelta(const git_diff_delta* delta) : DiffDelta(Diff(), delta)
	{
	}
}
#include "DiffBinary.h"
#include <schema/ObjectRegistration.h>

namespace JadeGit::Schema
{
	static GitObjectRegistration<DiffBinary> registration(TEXT("DiffBinary"));

	DiffBinary::DiffBinary(const git_diff_binary* ptr) : GitObject(registration, ptr)
	{
	}
}
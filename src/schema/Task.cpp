#include "Task.h"
#include "Exception.h"
#include "ObjectRegistration.h"
#include "Progress.h"

using namespace std;

namespace JadeGit::Schema
{
	int Task::make(DskParam* pReturn, Function function)
	{
		return paramSetOid(pReturn, (new Task(function))->oid);
	}

	static GitObjectRegistration<Task> registration(TEXT("Task"));

	Task::Task(const DskObjectId& pOid) : Object(pOid) {}

	Task::Task(Function function) : Object(registration), task(function)
	{
		jade_throw(createTransientObject());
		setThis();
	}

	Task::~Task()
	{
		jade_throw(deleteObject());
	}

	bool Task::execute(IProgress* progress)
	{
		future = task.get_future();
		task(progress);					// TODO: Execute via another thread
		return future.get();
	}

	int JOMAPI jadegit_task_delete(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		// TODO: Notifications?

		return 0;
	}

	int JOMAPI jadegit_task_execute(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				Progress progress;
				JADE_RETURN(paramGetOid(*pParams, progress.oid));

				unique_ptr<Task> task(Task::getThis(pBuffer));	// Whatever the result, we want to clean-up
				return paramSetBoolean(*pReturn, task->execute(progress.isNull() ? nullptr : &progress));
			});
	}
}
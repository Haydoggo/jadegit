#include "ObjectFactory.h"
#include <jadegit/Exception.h>

namespace JadeGit::Schema
{
	template <class T>
	concept disposable = requires(T & t) {
		t.dispose();
	};

	template<typename TDerived>
	class GitObjectRegistration : public GitObjectFactory::Registration
	{
	public:
		GitObjectRegistration(const Character* name)
		{
			GitObjectFactory::get().registration(name, this);
		}

		operator ClassNumber() const
		{
			if (!number)
			{
				GitObjectFactory::get().initialize();
				assert(number);
			}
			return number;
		}

	protected:
		TDerived* create(const DskObjectId& oid) const final
		{
			if constexpr (std::is_constructible_v<TDerived, const DskObjectId&>)
				return new TDerived(oid);
			else
				throw jadegit_exception("Cannot create schema object");
		}

		void dispose(const DskObjectId& oid) const final
		{
			if constexpr (disposable<TDerived>)
			{
				static_assert(std::is_constructible_v<TDerived, const DskObjectId&>);
				TDerived(oid).dispose();
			}
		}

		void initialize(ClassNumber number) const final
		{
			assert(!this->number);
			this->number = number;
		}

	private:
		mutable ClassNumber number = 0;
	};
}
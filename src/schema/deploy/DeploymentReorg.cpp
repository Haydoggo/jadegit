#include "DeploymentReorg.h"
#include <jade/Loader.h>
#include <schema/ObjectRegistration.h>

using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<GitDeploymentReorg> registration(TEXT("GitDeploymentReorg"));

	GitDeploymentReorg::GitDeploymentReorg(const GitDeploymentCommand& parent) : GitDeploymentCommand(parent, registration) {}

	void GitDeploymentReorg::abort(Transaction& transaction, Loader& loader, bool& done) const
	{
		// Re-orgs cannot be aborted once completed
		if (isCompleted())
		{
			done = true;
			return;
		}

		// TODO: Remember/verify schemas for which re-org is being aborted?

		// Commit current progress
		transaction.commit();

		// Abort re-org
		loader.reorgAbort();

		// Restart transaction
		transaction.begin();
	}

	void GitDeploymentReorg::execute(Transaction& transaction, Loader& loader) const
	{
		// TODO: Remember/verify/specify schemas to re-org

		// Commit current progress
		transaction.commit();

		// Re-org
		loader.reorg();

		// Mark as completed immediately, and put repository into unstable state (reset when deployment has completed successfully)
		transaction.begin();
		markAsCompleted();
		markRepositoryAsUnstable();
		transaction.commit();

		// Restart transaction
		transaction.begin();
	}
}
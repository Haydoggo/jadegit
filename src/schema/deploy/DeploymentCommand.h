#pragma once
#include <schema/Object.h>
#include <jade/Transaction.h>

namespace Jade
{
	class Loader;
}

namespace JadeGit
{
	class IProgress;
}

namespace JadeGit::Schema
{
	class GitDeployment;
	class RepositoryData;
	class User;

	class GitDeploymentCommand : public Object
	{
	public:
		enum State
		{
			Executing = (1 << 0),
			Completed = (1 << 1),
			Aborted = (1 << 2)
		};

		using Object::Object;
		GitDeploymentCommand(const GitDeploymentCommand& parent, ClassNumber classNo);

		inline bool isExecuting() const { return (getState() & State::Executing) > 0; }
		inline bool isCompleted() const { return (getState() & State::Completed) > 0; }
		inline bool isAborted() const { return (getState() & State::Aborted) > 0; }
		inline bool isAborting() const { return isAborted() && isExecuting(); }

	protected:
		friend GitDeployment;

		virtual bool abort(Jade::Transaction& transaction, Jade::Loader& loader, bool& done, IProgress* progress) const;
		virtual void abort(Jade::Transaction& transaction, Jade::Loader& loader, bool& done) const = 0;
		virtual void abortEnter() const;
		virtual void abortExit() const;

		virtual bool execute(Jade::Transaction& transaction, Jade::Loader& loader, IProgress* progress) const;
		virtual void execute(Jade::Transaction& transaction, Jade::Loader& loader) const = 0;
		virtual void executeEnter() const;
		virtual void executeExit() const;

		virtual void GetRepository(RepositoryData& repo) const;
		virtual void GetUser(User& user) const;

		void markAsCompleted() const;
		void markRepositoryAsUnstable() const;

	private:
		void setState(State value) const;
		State getState() const;
	};
	DEFINE_ENUM_FLAG_OPERATORS(GitDeploymentCommand::State);
}
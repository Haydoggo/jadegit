#pragma once
#include "Deployment.h"

namespace JadeGit::Schema
{
	class GitRepositoryDeployment : public GitDeployment
	{
	public:
		enum Action
		{
			Load = 1,
			Reset,
			Unload
		};

		using GitDeployment::GitDeployment;
		GitRepositoryDeployment();

		void SetAction(Action action);
		void SetRepository(const RepositoryData& repo);

	protected:
		void executeExit() const final;
		void executeLoad(RepositoryData& repo) const;
		void executeReset(RepositoryData& repo) const;
		void executeUnload(RepositoryData& repo) const;
	};
}
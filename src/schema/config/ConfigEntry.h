#pragma once
#include <schema/Proxy.h>

namespace JadeGit::Schema
{
	class ConfigEntry : public GitObject<git_config_entry>
	{
	public:
		using GitObject::GitObject;
		ConfigEntry(const Object& parent, git_config_entry* ptr, bool owner = true);
	};
}
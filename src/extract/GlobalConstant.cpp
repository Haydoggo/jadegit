#include "GlobalConstant.h"
#include "Schema.h"
#include "DataMapper.h"
#include "EntityRegistration.h"
#include <jadegit/data/RootSchema/GlobalConstantMeta.h>

using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<GlobalConstantMeta> mapper(DSKGLOBALCONSTANT, &RootSchema::globalConstant, {
		{PRP_GlobalConstant_category, new DataProperty(&GlobalConstantMeta::category)}		/* Global constants are extracted as children of schema, so need to extract reference to category (its other parent) */
		});

	static EntityRegistration<GlobalConstant> registrar(DSKGLOBALCONSTANT);

	bool GlobalConstant::lookup(const Entity* ancestor, const QualifiedName& path)
	{
		return Entity::lookup<Schema>(ancestor, path, PRP_Schema_consts);
	}
}
#pragma once
#include "SchemaEntity.h"

namespace JadeGit::Extract
{
	class Feature : public SchemaEntity
	{
	public:
		using SchemaEntity::SchemaEntity;

		void dependents(std::set<DskObjectId>& dependents) const override;
	};
}
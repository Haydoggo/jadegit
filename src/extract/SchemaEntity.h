#pragma once
#include "Entity.h"

namespace JadeGit::Extract
{	
	class SchemaEntity : public Entity
	{
	public:
		using Entity::Entity;

		std::string getName() const override;

		using Entity::GetParentId;
		DskObjectId GetParentId() const override;
	};
}
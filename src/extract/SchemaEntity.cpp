#include "SchemaEntity.h"
#include "DataMapper.h"
#include <jadegit/data/RootSchema/FeatureMeta.h>

using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<SchemaEntityMeta> schemaEntityMapper(DSKSCHEMAENTITY, &RootSchema::schemaEntity, {
		{PRP_SchemaEntity__deprecated, nullptr},
		{PRP_SchemaEntity__flags, nullptr},
		{PRP_SchemaEntity__systemBasic, nullptr},
		{PRP_SchemaEntity__wsdlName, new DataProperty(&SchemaEntityMeta::wsdlName)},
		{PRP_SchemaEntity_modifiedTimeStamp, nullptr},
		{PRP_SchemaEntity_name, nullptr},		// Named on creation
		{PRP_SchemaEntity_number, nullptr},
		{PRP_SchemaEntity_patchVersion, nullptr},
		{PRP_SchemaEntity_systemVersion, nullptr},
		{PRP_SchemaEntity_userProfile, nullptr},
		{PRP_SchemaEntity_uuid, nullptr}
		});

	static DataMapper<FeatureMeta> featureMapper(DSKFEATURE, &RootSchema::feature, {
		{PRP_Feature__sysExposedClassRefs, nullptr},
		{PRP_Feature__sysExposedFeatureRefs, nullptr},
		{PRP_Feature__sysScriptRefs, nullptr},
		{PRP_Feature_activeXFeature, nullptr},
		{PRP_Feature_exposedClassRefs, nullptr},
		{PRP_Feature_exposedFeatureRefs, nullptr},
		{PRP_Feature_scriptRefs, nullptr}
		});

	std::string SchemaEntity::getName() const
	{
		return getProperty<std::string>(PRP_SchemaEntity_name);
	}

	DskObjectId SchemaEntity::GetParentId() const
	{
		ShortDskParam out;
		jade_throw(sendMsg(TEXT("_getMetaParent"), NULL, (DskParam*)& out, __LINE__));

		DskObjectId oid;
		jade_throw(paramGetOid(out, oid));
		return oid;
	}
}
#pragma once
#include <jadegit/data/Assembly.h>
#include <jomtypes.h>
#include <map>

namespace JadeGit::Extract
{
	class DataMap;
	class DataMapFactory;

	class Assembly : public Data::Assembly
	{
	public:
		Assembly(const FileSystem& source);
		~Assembly();

		void extract(const std::vector<std::string>& schemas = std::vector<std::string>(), std::function<void(const std::string& message)> print = nullptr);

	protected:
		void unload() final;

	private:
		friend DataMapFactory;
		std::map<ClassNumber, DataMap*> dataMaps;
	};
}
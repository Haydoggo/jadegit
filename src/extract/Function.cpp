#include "Function.h"
#include "Schema.h"
#include "EntityRegistration.h"

namespace JadeGit::Extract
{
	static EntityRegistration<Function> registrar(DSKFUNCTION);

	bool Function::lookup(const Entity* ancestor, const QualifiedName& path)
	{
		return Entity::lookup<Schema>(ancestor, path, PRP_Schema_functions);
	}
}
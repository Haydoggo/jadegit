#pragma once
#include "Schema.h"
#include <jade/Iterator.h>
#include <jade/Transient.h>
#include <set>
#include <stack>

namespace JadeGit::Extract
{
	class SchemaColl : public Jade::Transient<Jade::Collection<DskArray>>
	{
	public:
		SchemaColl() : Transient<Collection<DskArray>>(DSKSCHEMACOLL) {}

		bool isLast(const Schema& schema) const
		{
			DskObject member;
			jade_throw(last(member));
			return schema.oid == member.oid;
		}
	};

	class SchemaIterator
	{
	public:
		SchemaIterator() noexcept;

		bool next(Schema& schema);
		bool next(Schema& schema, bool& acyclic);

	private:
		bool acyclic = false;
		SchemaColl imported;
		std::stack<Jade::Iterator<Schema>> stack;
		std::set<DskObjectId> visited;

		bool import(const Schema& schema, std::set<DskObjectId>& trail);
	};
}
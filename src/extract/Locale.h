#pragma once
#include "SchemaEntity.h"
#include "Schema.h"

namespace JadeGit::Extract
{
	class Locale : public SchemaEntity
	{
	public:
		using SchemaEntity::SchemaEntity;

	protected:
		bool lookup(const Entity* ancestor, const QualifiedName& path) override;
	};
}
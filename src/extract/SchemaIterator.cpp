#include "SchemaIterator.h"
#include <jomoid.hpp>

namespace JadeGit::Extract
{
	SchemaIterator::SchemaIterator() noexcept
	{
		visited.insert(RootSchemaOid);
		stack.emplace(DskObject(RootSchemaOid), PRP_Schema_subschemas);
	}

	bool SchemaIterator::import(const Schema& schema, std::set<DskObjectId>& trail)
	{	
		// Check for cyclic dependency
		if (trail.contains(schema.oid))
		{
			if (!imported.includes(schema))
			{
				this->import(schema.getProperty<Schema>(PRP_Schema_superschema), trail);
				imported.add(schema);
			}
			return false;
		}

		// Ignore schemas already visited
		if (visited.contains(schema.oid) || imported.includes(schema))
			return true;

		// Add to trail for cyclic check above
		trail.insert(schema.oid);

		// Handle imported superschema
		auto acyclic = import(schema.getProperty<Schema>(PRP_Schema_superschema), trail);

		// Handle imported package dependencies
		Object package;
		Jade::Iterator<Object> iter(schema, PRP_Schema_importedPackages);
		while (iter.next(package))
		{
			acyclic = import(package.getProperty<Object>(PRP_JadeImportedPackage_exportedPackage).getProperty<Schema>(PRP_JadePackage_schema), trail) && acyclic;
		}

		// Add imported schema to group (if not added due to cyclic dependency above)
		if (!imported.includes(schema))
			imported.add(schema);

		trail.erase(schema.oid);
		return acyclic;
	}

	bool SchemaIterator::next(Schema& schema)
	{
		bool acyclic = false;
		return next(schema, acyclic);
	}

	bool SchemaIterator::next(Schema& schema, bool& acyclic)
	{
		while (!stack.empty())
		{
			auto& iter = stack.top();

			// Move to next schema
			if (iter.next(schema))
			{
				// Return if iterating imported collection
				if (iter.isCollection(imported))
				{
					visited.insert(schema.oid);
					acyclic = this->acyclic || imported.isLast(schema);
					return true;
				}
				else
				{
					// Push subschema iterator onto stack
					stack.emplace(schema, PRP_Schema_subschemas);

					// Traverse imported dependencies if schema hasn't been visited (may have been imported)
					if (!visited.contains(schema.oid))
					{
						imported.clear();
						std::set<DskObjectId> trail;
						this->acyclic = import(schema, trail);

						// Push imported iterator onto stack (which includes schema just identified)
						stack.emplace(imported);
					}
				}
			}
			else
			{
				stack.pop();
			}
		}
		
		schema.oid = NullDskObjectId;
		return false;
	}
}
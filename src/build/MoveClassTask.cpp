#include "MoveClassTask.h"
#include "CommandStrategy.h"

namespace JadeGit::Build
{
	MoveClassTask::MoveClassTask(Graph& graph, std::string qualifiedName, std::string newSuperClass) : CommandTask(graph),
		qualifiedName(std::move(qualifiedName)),
		newSuperClass(std::move(newSuperClass))
	{
	}

	void MoveClassTask::execute(CommandStrategy& strategy) const
	{
		strategy.MoveClass(qualifiedName, newSuperClass);
	}

	void MoveClassTask::Print(std::ostream& output) const
	{
		output << "Move Class " << qualifiedName << " " << newSuperClass;
	}
}
#pragma once
#include "ConstantDelta.h"
#include <jadegit/data/TranslatableString.h>

namespace JadeGit::Build
{
	class TranslatableStringDelta : public ConstantDelta<TranslatableString>
	{
	public:
		TranslatableStringDelta(Graph& graph) : ConstantDelta(graph, "TranslatableString") {}

		// TODO: Locale dependencies?  Subschema removals?
	};
}
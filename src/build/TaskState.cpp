#include "TaskState.h"
#include "Task.h"
#include <Singleton.h>
#include <Log.h>

using namespace std;

namespace JadeGit::Build
{
	bool TaskState::executeEnter(const Task& task, const Task* cascade) const
	{
		for (auto& child : task.children)
			if (!child->state->executeEnter(*child, cascade))
				return false;

		return true;
	}

	bool TaskState::executeExit(const Task& task, const Task* cascade, bool result, size_t& visited, bool& aborted, IProgress* progress) const
	{
		for (auto& child : task.children)
			if (!child->state->executeExit(*child, cascade, result, visited, aborted, progress))
				return false;

		return true;
	}
	
	void TaskState::reset(const Task& task, int depth) const
	{
		for (auto& child : task.children)
			child->state->reset(*child, depth + 1);
	}

	const TaskState* TaskState::get(const Task& task) const
	{
		return task.state;
	}

	void TaskState::set(const Task& task, const TaskState* state) const
	{
		assert(state);
		task.state = state;
	}

	bool TaskState::traversable(const Task& task) const
	{
		return !task.children_processing && !task.predecessors_processing;
	}

	bool TaskState::traverse(const Task& task, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade) const
	{
		assert(cascade);

		// Try visit all children
		if (!task.children.empty())
		{
			LOG_DEBUG("build: " << string(depth, '\t') << "visit children");

			for (auto& child : task.children)
			{
				if (!child->state->traverse(*child, visited, visitor, aborted, progress, depth + 1, cascade) || aborted)
					return false;
			}
		}

		return true;
	}

	class ProcessingState : public TaskState, public Singleton<ProcessingState>
	{
	public:
		bool execute(const Task& task, bool repeat, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade) const;

	protected:
		bool executeEnter(const Task& task, const Task* cascade) const final
		{
			assert(cascade == &task);

			return TaskState::executeEnter(task, cascade);
		}

		bool executeExit(const Task& task, const Task* cascade, bool result, size_t& visited, bool& aborted, IProgress* progress) const final
		{
			assert(cascade == &task);

			return TaskState::executeExit(task, cascade, result, visited, aborted, progress);
		}

		bool traversable(const Task& task) const final
		{
			return false;
		}

		bool traverse(const Task& task, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade) const final
		{
			LOG_DEBUG("build: " << string(depth, '\t') << task << " (cyclic!)");
			throw logic_error("Cyclic build task");
		}

		void updateCounts(const Task& task, short increment) const
		{
			// Update children processing count for parents
			const Task* parent = task.parent;
			while (parent && parent->state != this)
			{
				parent->children_processing += increment;
				parent = parent->parent;
			}

			// Update predecessors processing count for successors
			if (!task.successors.empty())
			{
				queue<const Task*> q;
				q.push(&task);

				while (!q.empty())
				{
					auto& task = *q.front();
					q.pop();

					for (auto& successor : task.successors)
					{
						if (successor->state == this)
							continue;

						// Update count on successor
						successor->predecessors_processing += increment;

						// Update successor parents, provided they're not a common ancestor
						const Task* parent = successor->parent;
						while (parent && parent->state != this && !task.isChildOf(parent))
						{
							parent->predecessors_processing += increment;
							parent = parent->parent;
						}

						// Queue successor to update successors of that
						q.push(successor);
					}
				}
			}
		}
	};

	template <bool Repeat>
	class IncompleteState : public TaskState, public Singleton<IncompleteState<Repeat>>
	{
	protected:
		bool traverse(const Task& task, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade) const final
		{
			// Repeat during cascade only, return true immediately otherwise for task already executed previously
			if constexpr (Repeat)
			{
				if (!cascade)
					return true;
			}

			// Determine if parent task can be executed instead of child (if not already cascading through children)
			const Task* parent = cascade ? nullptr : task.parent;
			const Task* substitute = nullptr;
			while (parent)
			{
				// Check task can be executed without creating a cycle (because it's already being partially processed somehow)
				if (!get(*parent)->traversable(*parent))
					break;

				// Substitute top-level tasks, incomplete tasks, or complete tasks where parent is incomplete
				if (!parent->parent || !parent->complete || !parent->parent->complete)
					substitute = parent;

				parent = parent->parent;
			}

			if (substitute)
			{
				LOG_DEBUG("build: " << string(depth, '\t') << "substituting: " << *substitute << ", for: " << task);
			}

			// Execute task
			return ProcessingState::Instance()->execute((substitute ? *substitute : task), Repeat, visited, visitor, aborted, progress, depth, cascade);
		}

		bool executeEnter(const Task& task, const Task* cascade) const final;
	};

	const TaskState* TaskState::initialState()
	{
		return IncompleteState<false>::Instance();
	}

	class CompletedState : public TaskState, public Singleton<CompletedState>
	{
	protected:
		void reset(const Task& task, int depth) const final
		{
			// Suppress reset for top-level tasks, as these have been fully completed without any need to repeat again
			if (!depth)
				return;

			// Reset state to incomplete state, ready for repeating if required
			set(task, IncompleteState<true>::Instance());

			// Reset child tasks
			TaskState::reset(task, depth);
		}

		bool traverse(const Task& task, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade) const final
		{
			// Return true immediately for tasks already completed
			return true;
		}
	};

	template <bool Repeat>
	class SuppressedState : public TaskState, public Singleton<SuppressedState<Repeat>>
	{
	protected:
		void reset(const Task& task, int depth) const final
		{
			// Revert to incomplete state
			set(task, IncompleteState<Repeat>::Instance());

			// Reset child tasks
			TaskState::reset(task, depth);
		}
		
		bool traverse(const Task& task, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade) const final
		{
			// Process task when required by successor task (suppression applies during cascade execution only)
			if (!cascade)
			{
				// Return true immediately when task has already been completed previously
				if (Repeat)
					return true;
				
				// Execute required task
				LOG_DEBUG("build: " << string(depth, '\t') << task << " (required)");
				return ProcessingState::Instance()->execute(task, false, visited, visitor, aborted, progress, depth, nullptr);
			}

			LOG_DEBUG("build: " << string(depth, '\t') << task << (Repeat ? " (not required again)" : " (not required)"));
			return TaskState::traverse(task, visited, visitor, aborted, progress, depth, cascade);
		}

		bool executeEnter(const Task& task, const Task* cascade) const final
		{
			// Verify task should still be suppressed
			if (task.required(cascade, Repeat))
				throw logic_error("Task already suppressed");

			return TaskState::executeEnter(task, cascade);
		}

		bool executeExit(const Task& task, const Task* cascade, bool result, size_t& visited, bool& aborted, IProgress* progress) const final
		{
			if (result)
			{
				// Update visited & progress if task wasn't being repeated
				if constexpr (!Repeat)
				{
					visited++;

					// Increment progress, aborting traversal if requested
					if (aborted = (progress && !progress->step()))
						return false;
				}

				// Move to completed state
				set(task, CompletedState::Instance());
			}
			else
			{
				// Revert to incomplete state
				set(task, IncompleteState<Repeat>::Instance());
			}

			return TaskState::executeExit(task, cascade, result, visited, aborted, progress);
		}
	};

	template <bool Repeat>
	bool IncompleteState<Repeat>::executeEnter(const Task& task, const Task* cascade) const
	{
		// Suppress child tasks when they're not required
		if (!task.required(cascade, Repeat))
			set(task, SuppressedState<Repeat>::Instance());

		return TaskState::executeEnter(task, cascade);
	}

	template <bool Repeat>
	class DeferredState : public TaskState, public Singleton<DeferredState<Repeat>>
	{
	protected:
		bool executeEnter(const Task& task, const Task* cascade) const final
		{
			// Return false immediately for tasks that've been deferred until next iteration
			assert(cascade && cascade != &task);
			return false;
		}
		
		void reset(const Task& task, int depth) const final
		{
			// Reset state to indicate task can be reassessed/tried again during next iteration
			set(task, IncompleteState<Repeat>::Instance());

			// Reset child tasks
			TaskState::reset(task, depth);
		}

		bool traversable(const Task& task) const final
		{
			// Cannot traverse tasks that've already been deferred until next iteration
			return false;
		}

		bool traverse(const Task& task, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade) const final
		{
			// Return true immediately for repeats (as it was done previously), false otherwise for tasks that've been deferred until next iteration
			assert(!cascade || !Repeat);
			return Repeat;
		}
	};

	bool ProcessingState::execute(const Task& task, bool repeat, size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade) const
	{		
		assert(cascade || !repeat);

		LOG_DEBUG("build: " << string(depth, '\t') << task << (repeat ? " (enter again)" : " (enter)"));
		
		// Update task state to reflect it's being processed
		set(task, this);

		// Update processing counts on related tasks
		if (!repeat)
			updateCounts(task, 1);

		// Determine whether children tasks are still required
		bool result = cascade || executeEnter(task, &task);

		// Try visit all prerequisites for top-level task, or direct predecessors otherwise
		if (result && !repeat)
		{
			auto tasks = cascade ? task.predecessors : task.prerequisites(cascade);
			if (!tasks.empty())
			{
				LOG_DEBUG("build: " << string(depth, '\t') << "visit " << (cascade ? "predecessors" : "prerequisites"));

				// Sort tasks
				stable_sort(tasks.begin(), tasks.end(), [](const Task* a, const Task* b) { return *a < *b; });

				for (auto& task : tasks)
				{
					result = task->traverse(visited, visitor, aborted, progress, depth + 1) && result;
					if (aborted)
						return false;
				}
			}
		}

		// Try visit all children
		result = result && TaskState::traverse(task, visited, visitor, aborted, progress, depth, cascade ? cascade : &task);
		if (aborted)
			return false;

		// Try visit self, provided all prerequisites & children have been completed		
		if (result)
		{
			LOG_DEBUG("build: " << string(depth, '\t') << "visit self");

			if ((result = task.accept(visitor)) && !repeat)
			{
				visited++;

				// Increment progress, aborting traversal if requested
				if (aborted = (progress && !progress->step()))
					return false;
			}
		}

		// Update processing counts on related tasks
		if (!repeat)
			updateCounts(task, -1);

		// Update state to reflect outcome
		if (result)
			set(task, CompletedState::Instance());
		else
		{
			if (repeat)
				set(task, DeferredState<true>::Instance());
			else
				set(task, DeferredState<false>::Instance());
		}

		LOG_DEBUG("build: " << string(depth, '\t') << task << (result ? (repeat ? " (completed again)" : " (completed)") : " (deferred)"));
		return (cascade || executeExit(task, &task, result, visited, aborted, progress)) && result;
	}
}
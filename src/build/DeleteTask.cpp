#include "DeleteTask.h"
#include "CommandStrategy.h"

namespace JadeGit::Build
{
	DeleteTask::DeleteTask(Graph& graph, Task* parent, const char* entityType, std::string qualifiedName) : CommandTask(graph, parent, LoadStyle::Latest, !qualifiedName.empty()),
		entityType(entityType),
		qualifiedName(std::move(qualifiedName))
	{
	}

	bool DeleteTask::accept(TaskVisitor& v) const
	{
		// Suppress visiting/execution when explicit deletion isn't required
		return qualifiedName.empty() || CommandTask::accept(v);
	}

	void DeleteTask::execute(CommandStrategy& strategy) const
	{
		strategy.Delete(entityType, qualifiedName);
	}

	void DeleteTask::Print(std::ostream& output) const
	{
		if (!qualifiedName.empty())
			output << "Delete " << entityType << " " << qualifiedName;
		else
			output << "<delete " << entityType << ">";
	}

	bool DeleteTask::required(const Task* cascade, bool repeat) const
	{
		// Suppress repeat deletions
		if (repeat)
			return false;

		// Suppress deletions when they're implied by a complete parent task
		// NOTE: This needs to iterate parents to check as there may be an intermediary incomplete/empty placeholder task
		if (cascade)
		{
			assert(isChildOf(cascade));

			const Task* parent = this->parent;
			while (parent)
			{
				if (parent->complete)
					return false;

				if (parent == cascade)
					break;

				parent = parent->parent;
			}
		}

		// Explicit deletion still required
		return true;
	}
}
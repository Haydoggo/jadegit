#pragma once
#include "SchemaComponentDelta.h"
#include "BuildTask.h"
#include "CommandStrategy.h"
#include <jadegit/data/Schema.h>

namespace JadeGit::Build
{
	class SchemaBuildTask : public BuildTask
	{
	public:
		SchemaBuildTask(Graph& graph, Schema& schema) : BuildTask(graph), schema(schema) {}

	protected:
		Schema& schema;
	};

	class RegisterSchemaTask : public SchemaBuildTask
	{
	public:
		using SchemaBuildTask::SchemaBuildTask;

		bool execute(Builder& builder) const final
		{
			builder.RegisterSchema(schema.name.c_str());
			return true;
		}

		void Print(std::ostream& output) const
		{
			output << "Register Schema " << schema.name;
		}
	};

	class DeregisterSchemaTask : public SchemaBuildTask
	{
	public:
		using SchemaBuildTask::SchemaBuildTask;

		bool execute(Builder& builder) const final
		{
			builder.DeregisterSchema(schema.name.c_str());
			return true;
		}

		void Print(std::ostream& output) const
		{
			output << "Deregister Schema " << schema.name;
		}
	};

	class SchemaDeletionTask : public DeleteTask
	{
	public:
		using DeleteTask::DeleteTask;

		void execute(CommandStrategy& strategy) const final
		{
			DeleteTask::execute(strategy);

			// Delete schemas via separate command files as JADE commits their deletion immediately so can't be repeated during a retry
			strategy.Flush();
		}
	};

	class ModifySchemaTask : public CommandTask
	{
	public:
		std::string schemaName;
		std::string key;
		std::string value;

		ModifySchemaTask(Graph& graph, std::string schemaName, std::string key, std::string value) : CommandTask(graph, nullptr, LoadStyle::Current), 
			schemaName(std::move(schemaName)), 
			key(std::move(key)),
			value(std::move(value))
		{}

		void execute(CommandStrategy& strategy) const final
		{
			strategy.ModifySchema(schemaName, key, value);
		}

		void Print(std::ostream& output) const final
		{
			output << "Modify Schema " << schemaName << " (" << key << " = " << value << ")";
		}
	};

	class SchemaDelta : public SchemaComponentDelta<Schema>
	{
	public:
		SchemaDelta(Graph& graph) : SchemaComponentDelta(graph, "Schema") {}
		
		using SchemaComponentDelta<Schema>::previous;

		Task* GetCreation() const final
		{
			// Declaration task establishes new schema
			if (declaration)
				return declaration;

			return SchemaComponentDelta<Schema>::GetCreation();
		}

	protected:
		bool AnalyzeEnter() final
		{
			// Basic analysis
			if (!SchemaComponentDelta::AnalyzeEnter())
			{
				// Deregister schema
				new DeregisterSchemaTask(graph, *previous);

				// Handle deletion before deleting previous superschema
				this->addDeletionDependency(previous->superschema, GetDeletion());

				return false;
			}

			// Register schema on creation
			if (!previous)
				new RegisterSchemaTask(graph, *latest);

			// Definition dependent on superschema being created
			this->addCreationDependency(latest->superschema, GetDefinition());

			// TODO: Handle schema moves

			return true;
		}

		void AnalyzeExit() final
		{
			SchemaComponentDelta::AnalyzeExit();

			// Handle schema modifications
			if (previous && latest)
			{
				// Handle switching default locale
				if (previous->primaryLocale && latest->primaryLocale && previous->primaryLocale->name != latest->primaryLocale->name)
				{
					auto modification = new ModifySchemaTask(graph, QualifiedName(), "DefaultLocale", latest->primaryLocale->name);
					modification->addPredecessor(GetRename());

					// Create new primary locale before switch
					this->addCreationDependency(latest->primaryLocale, modification);

					// Delete previous primary locale after switch
					this->addDeletionDependency(previous->primaryLocale, modification);

					// Define new primary locale after switch
					this->GetDefinition()->addPredecessor(modification, true);
				}
			}
		}

		Task* handleDefinition(Task* parent) final
		{
			auto definition = SchemaComponentDelta<Schema>::handleDefinition(parent);

			// Use partial definition as schema declaration ahead of complete definition
			if (!previous)
				declaration = new ExtractDefinitionTask<Schema>(graph, definition, *latest, false, true);

			return definition;
		}
		
		Task* HandleDeletion(Task* parent) final
		{
			return new SchemaDeletionTask(graph, parent, entityType, QualifiedName());
		}

	private:
		Task* declaration = nullptr;
	};
}
#include "RenameTask.h"
#include "CommandStrategy.h"

namespace JadeGit::Build
{
	RenameTask::RenameTask(Graph& graph, const char* entityType, std::string qualifiedName, std::string newName) : CommandTask(graph),
		entityType(entityType),
		qualifiedName(std::move(qualifiedName)),
		newName(std::move(newName))
	{
	}

	void RenameTask::execute(CommandStrategy& strategy) const
	{
		strategy.Rename(entityType, qualifiedName, newName);
	}

	void RenameTask::Print(std::ostream& output) const
	{
		output << "Rename " << entityType << " " << qualifiedName << " " << newName;
	}
}
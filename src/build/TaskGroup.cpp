#include "TaskGroup.h"
#include "Graph.h"
#include <jadegit/data/Entity.h>

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Build
{
	TaskGroup* TaskGroup::make(Graph& graph, const Data::Entity* entity)
	{
		if (!entity)
			return nullptr;

		if (TaskGroup* task = graph.groups[entity])
			return task;

		return graph.groups[entity] = new TaskGroup(graph, *entity);
	}

	TaskGroup::TaskGroup(Graph& graph, const Data::Entity& entity) : Task(graph), entity(entity)
	{
	}

	void TaskGroup::Print(std::ostream& output) const
	{
		output << "[" << entity.GetQualifiedName() << "]";
	}
}
#pragma once
#include "CommandTask.h"

namespace JadeGit::Build
{
	class MoveClassTask : public CommandTask
	{
	public:
		std::string qualifiedName;
		std::string newSuperClass;

		MoveClassTask(Graph& graph, std::string qualifiedName, std::string newSuperClass);

	protected:
		void execute(CommandStrategy& strategy) const final;
		void Print(std::ostream& output) const final;
	};
}
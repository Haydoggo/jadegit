#pragma once
#include <string>

namespace JadeGit::Build
{
	class CommandStrategy
	{
	public:
		virtual void CreateLocale(const std::string& qualifiedName, const char* baseLocale) = 0;
		virtual void Delete(const char* entityType, const std::string& qualifiedName) = 0;
		virtual void Rename(const char* entityType, const std::string& qualifiedName, const std::string& newName) = 0;
		virtual void ModifySchema(const std::string& schemaName, const std::string& key, const std::string& value) = 0;
		virtual void MoveClass(const std::string& qualifiedName, const std::string& newSuperclass) = 0;
		virtual void Flush() = 0;
	};
}
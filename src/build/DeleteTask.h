#pragma once
#include "CommandTask.h"

namespace JadeGit::Build
{
	class DeleteTask : public CommandTask
	{
	public:
		DeleteTask(Graph& graph, Task* parent, const char* entityType, std::string qualifiedName = std::string());

	protected:
		const char* entityType;
		std::string qualifiedName;

		bool accept(TaskVisitor& v) const final;
		void execute(CommandStrategy &strategy) const override;
		void Print(std::ostream& output) const final;
		bool required(const Task* cascade, bool repeat) const final;
	};
}
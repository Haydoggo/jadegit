#include "DefinitionNode.h"

namespace JadeGit::Build::Classic
{
	void DefinitionNode::Write(std::ostream& output, const std::string& indent)
	{
		WriteEnter(output, indent);
		WriteBody(output, indent);
		WriteExit(output, indent);
	}

	void DefinitionNode::WriteAttribute(std::ostream& output, const char* attribute, bool condition)
	{
		if (!condition)
			return;

		if (delim)
			output << ",";
		else
			delim = true;

		output << " " << attribute;
	}

	void DefinitionNode::WriteAttribute(std::ostream& output, const char* attribute, int value)
	{
		WriteAttribute(output, attribute, value, !!value);
	}

	void DefinitionNode::WriteAttribute(std::ostream& output, const char* attribute, int value, bool condition)
	{
		if (!condition)
			return;

		WriteAttribute(output, attribute, true);
		output << " = " << value;
	}

	void DefinitionNode::WriteAttribute(std::ostream& output, const char* attribute, const char* value)
	{
		if (!value)
			return;

		WriteAttribute(output, attribute, true);
		output << " = \"" << value << "\"";
	}

	void DefinitionNode::WriteAttribute(std::ostream& output, const char* attribute, const std::string& value)
	{
		if (value.empty())
			return;

		WriteAttribute(output, attribute, value.c_str());
	}

	void DefinitionNodes::Write(std::ostream& output, const std::string& indent) const
	{
		if (this->empty())
			return;

		if(heading)
			output << indent << heading << "\n";

		if (bracketed)
			output << indent << "\t(\n";

		for (DefinitionNode* node : *this)
			node->Write(output, indent + "\t");

		if (bracketed)
			output << indent << "\t)\n";
	}
}
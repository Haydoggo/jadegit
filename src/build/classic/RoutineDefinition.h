#pragma once
#include "FeatureDefinition.h"
#include "RoutineSource.h"

namespace JadeGit::Build::Classic
{
	class RoutineDefinition : public FeatureDefinition<Data::Routine>
	{
	public:
		using FeatureDefinition<Data::Routine>::FeatureDefinition;

	protected:
		void WriteEnter(std::ostream& output, const std::string& indent) override;
		void WriteAttributes(std::ostream& output) override;
	};

	template <class TDerived, class TRoutine>
	class RoutineDefinitionRegistration : public NodeRegistration<TDerived, TRoutine>
	{
	public:
		using NodeRegistration<TDerived, TRoutine>::NodeRegistration;

	protected:
		TDerived* create(SchemaDefinition& schema, TDerived::parent_node& parent, const TRoutine& routine, bool complete, bool modified) const final
		{
			auto result = NodeRegistration<TDerived, TRoutine>::create(schema, parent, routine, complete, modified);

			// Define routine source when it's modified
			if (result && modified)
				NodeFactory<RoutineSource>::get().create(std::type_index(typeid(routine)), schema, routine);

			return result;
		}
	};
}
#include "CollClassDefinition.h"
#include "SchemaDefinition.h"
#include "TypeDeclaration.h"
#include <jadegit/data/ExternalCollClass.h>

namespace JadeGit::Build::Classic
{
	template <class TClass>
	class CollClassNodeRegistration : public NodeRegistration<CollClassDefinition, TClass>
	{
	public:
		using NodeRegistration<CollClassDefinition, TClass>::NodeRegistration;

	protected:
		CollClassDefinition* create(SchemaDefinition& schema, SchemaDefinition& parent, const TClass& klass, bool complete, bool modified) const final
		{
			// Resolve definition (creating if required)
			auto result = NodeRegistration<CollClassDefinition, TClass>::create(schema, parent, klass, complete, modified);

			// Need to include key definitions for complete definition
			if (complete && !klass.keys.empty())
			{
				// Need to include header which schema loader depends on for loading key definitions
				NodeFactory<TypeDeclaration>::get().create(std::type_index(typeid(klass)), schema, klass);
				
				for (auto& key : klass.keys)
					NodeFactory<DefinitionNode>::get().create(std::type_index(typeid(*key)), schema, *key);
			}

			return result;
		}
	};

	static CollClassNodeRegistration<Data::CollClass> collClass;
	static CollClassNodeRegistration<Data::ExternalCollClass> externalCollClass(true);

	CollClassDefinition::CollClassDefinition(SchemaDefinition& schema, const Data::CollClass& klass) : ClassDefinition(schema, klass)
	{
	}
}
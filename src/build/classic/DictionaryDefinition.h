#pragma once
#include "SchemaDefinitionNode.h"
#include <jadegit/data/CollClass.h>
#include <jadegit/data/Key.h>

namespace JadeGit::Build::Classic
{
	class DictionaryDefinition : public SchemaDefinitionNode<Data::CollClass>
	{
	public:
		DictionaryDefinition(SchemaDefinition& schema, const Data::CollClass& klass);

		DefinitionNodes keys;

	protected:
		void WriteAttributes(std::ostream& output) override;
		void WriteBody(std::ostream& output, const std::string& indent) override;
	};

	class KeyDefinition : public DefinitionNode
	{
	public:
		KeyDefinition(SchemaDefinition& schema, DictionaryDefinition& dictionary, const Data::Key& key);

	protected:
		const Data::Key& source;

		void WriteBody(std::ostream& output, const std::string& indent) override;
	};
}
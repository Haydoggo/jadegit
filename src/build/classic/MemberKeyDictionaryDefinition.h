#pragma once
#include "DictionaryDefinition.h"
#include <jadegit/data/Key.h>

namespace JadeGit::Build::Classic
{
	class MemberKeyDictionaryDefinition : public DictionaryDefinition
	{
	public:
		using base_node = MemberKeyDictionaryDefinition;
		using parent_node = SchemaDefinition;

		MemberKeyDictionaryDefinition(SchemaDefinition& schema, const Data::CollClass& klass);
	};

	class MemberKeyDefinition : public KeyDefinition
	{
	public:
		using parent_node = MemberKeyDictionaryDefinition;

		MemberKeyDefinition(SchemaDefinition& schema, MemberKeyDictionaryDefinition& dictionary, const Data::MemberKey& key) : KeyDefinition(schema, dictionary, key) {}

		void WriteBody(std::ostream& output, const std::string& indent) override;
	};
}
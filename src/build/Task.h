#pragma once
#include <jadegit/MemoryAllocated.h>
#include <jadegit/Progress.h>
#include <ostream>

namespace JadeGit::Build
{
	class Graph;
	class TaskVisitor;

	class Task : public MemoryAllocated
	{
	public:
		Task(Graph& graph, bool complete = false, short priority = 0);
		Task(Graph& graph, Task& parent, bool complete = false, short priority = 0);
		Task(Graph& graph, Task* parent, bool complete = false, short priority = 0);

		bool operator<(const Task& rhs) const;

		const bool complete;
		Task* const parent = nullptr;

		virtual bool accept(TaskVisitor& v) const = 0;

		// Add predecessor
		void addPredecessor(Task& predecessor, bool common = false, bool needsReorg = false);
		inline void addPredecessor(Task* predecessor, bool common = false, bool needsReorg = false) { if (predecessor) addPredecessor(*predecessor, common, needsReorg); }

		// Determine if task is a child of possible parent supplied
		bool isChildOf(const Task* parent) const;

		// Determine if task is a successor of possible predecessor supplied, excluding child dependencies
		bool isSuccessorOf(const Task& predecessor) const;
		inline bool isSuccessorOf(const Task* predecessor) const { return predecessor && isSuccessorOf(*predecessor); }

		virtual bool required(const Task* cascade, bool repeat) const { return true; }

		// Try adding predecessor
		bool tryAddPredecessor(Task& predecessor);
		inline bool tryAddPredecessor(Task* predecessor) { return predecessor && tryAddPredecessor(*predecessor); }

		virtual void Print(std::ostream& output) const = 0;

	protected:
		friend Graph;
		Graph& graph;
		virtual ~Task();

	private:
		Task(const Task&) = delete;

		friend class TaskState;
		friend class ProcessingState;
		mutable const TaskState* state;

		const short priority = 0;

		std::vector<const Task*> children;
		mutable size_t children_processing = 0;

		std::vector<const Task*> predecessors;
		std::vector<const Task*> predecessors_common;
		mutable size_t predecessors_processing = 0;
		std::vector<const Task*> successors;

		void reset() const;

		// Build list of prerequisites for task
		std::vector<const Task*> prerequisites(bool cascading) const;

		bool traverse(size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth = 0, const Task* cascade = nullptr) const;
	};

	std::ostream& operator<< (std::ostream& stream, const Task& task);
}

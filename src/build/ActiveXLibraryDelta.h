#pragma once
#include "ClassDelta.h"
#include <jadegit/data/ActiveXLibrary.h>

namespace JadeGit::Build
{
	class ActiveXLibraryDelta : public SchemaComponentDelta<ActiveXLibrary>
	{
	public:
		ActiveXLibraryDelta(Graph& graph) : SchemaComponentDelta(graph, "ActiveXLibrary") {}

	protected:
		bool AnalyzeEnter() final
		{
			// Analyze base class
			baseClass = graph.Analyze<IClassDelta>(latest ? latest->getBaseClass() : previous->getBaseClass());

			// Basic analysis
			if (!SchemaComponentDelta::AnalyzeEnter())
				return false;

			return true;
		}

		Task* HandleDeletion(Task* parent) final
		{
			// Deletion implied by base class deletion
			if (baseClass)
				return baseClass->GetDeletion();

			return SchemaComponentDelta::HandleDeletion(parent);
		}

		Task* handleRename() final
		{
			// Rename implied by base class rename
			if (baseClass)
				return baseClass->GetRename();

			return SchemaComponentDelta::handleRename();
		}

	private:
		IClassDelta* baseClass = nullptr;
	};

	template<class TComponent>
	class ActiveXComponentDelta : public Delta<TComponent>
	{
	public:
		ActiveXComponentDelta(Graph& graph) : Delta<TComponent>(graph) {}

		using Delta<TComponent>::previous;
		using Delta<TComponent>::latest;

	protected:
		using Delta<TComponent>::graph;
		using Delta<TComponent>::GetDefinition;
		using Delta<TComponent>::GetDeletion;

		Task* handleDefinition(Task* parent) final
		{
			// Defined by parent
			return parent;
		}

		Task* HandleDeletion(Task* parent) final
		{
			// Deleted implicitly by related/original entity
			if (const IDelta* original = graph.Analyze<IDelta>(&previous->getOriginal()))
			{
				if (auto deletion = original->GetDeletion())
					return deletion;
			}

			// Deleted implicitly by parent otherwise
			return parent;
		}

		bool AnalyzeEnter() final
		{
			// Returns false when deleted
			if (!Delta<TComponent>::AnalyzeEnter())
				return false;

			// Definition depends on related/original entities
			this->addDefinitionDependency(latest->getOriginal(), GetDefinition());

			return true;
		}
	};
}
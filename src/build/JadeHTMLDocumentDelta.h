#pragma once
#include "ClassDelta.h"
#include "ExtractDefinitionRenameTask.h"
#include <jadegit/data/JadeHTMLDocument.h>

namespace JadeGit::Build
{
	class JadeHTMLDocumentDelta : public SchemaComponentDelta<JadeHTMLDocument>
	{
	public:
		JadeHTMLDocumentDelta(Graph& graph) : SchemaComponentDelta(graph, "JadeHTMLDocument") {}

	protected:
		bool AnalyzeEnter() override
		{
			// Analyze HTML class
			htmlClass = graph.Analyze<IClassDelta>(latest ? latest->htmlClass : previous->htmlClass);

			// Basic analysis
			if (!SchemaComponentDelta::AnalyzeEnter())
				return false;

			return true;
		}

		Task* handleDefinition(Task* parent) final
		{
			if (previous && previous->name != latest->name)
				return new ExtractDefinitionRenameTask<JadeHTMLDocument>(graph, parent, *latest, previous->name.c_str());

			return SchemaComponentDelta::handleDefinition(parent);
		}

		Task* HandleDeletion(Task* parent) override
		{
			// Deletion implied by HTML class deletion
			if (htmlClass)
				return htmlClass->GetDeletion();

			return SchemaComponentDelta::HandleDeletion(parent);
		}

		Task* handleRename() override
		{
			// Rename handled implicitly by definition
			return nullptr;
		}

	private:
		IClassDelta* htmlClass = nullptr;
	};
}
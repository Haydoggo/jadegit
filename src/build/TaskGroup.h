#pragma once
#include "Task.h"

namespace JadeGit::Data
{
	class Entity;
}

namespace JadeGit::Build
{
	class TaskGroup : public Task
	{
	public:
		static TaskGroup* make(Graph& graph, const Data::Entity* entity);
		
	private:
		TaskGroup(Graph& graph, const Data::Entity& entity);

		const Data::Entity& entity;

		bool accept(TaskVisitor& v) const final { return true; };
		void Print(std::ostream& output) const final;
	};
}
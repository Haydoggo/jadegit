#include "ExtractTask.h"
#include "TaskVisitor.h"

namespace JadeGit::Build
{
	bool ExtractTask::accept(TaskVisitor& v) const
	{
		return v.visit(*this);
	}
	
	bool ExtractTask::required(const Task* cascade, bool repeat) const
	{
		// Always required first time
		if (!repeat)
			return true;

		// Repeat required for complete parent task
		// NOTE: This needs to iterate parents to check as there may be an intermediary incomplete/empty placeholder task
		if (cascade)
		{
			assert(isChildOf(cascade));

			const Task* parent = this->parent;
			while (parent)
			{
				if (parent->complete)
					return true;

				if (parent == cascade)
					break;

				parent = parent->parent;
			}
		}

		return false;
	}
}
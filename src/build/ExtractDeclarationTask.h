#pragma once
#include "ExtractTask.h"

namespace JadeGit::Build
{
	template<Declarable TEntity>
	class ExtractDeclarationTask : public ExtractTask
	{
	public:
		ExtractDeclarationTask(Graph& graph, Task* parent, const TEntity& entity) : ExtractTask(graph, parent), entity(entity) {};

		bool execute(ExtractStrategy& strategy) const final
		{
			return strategy.Declare(entity);
		}

		void Print(std::ostream& output) const final
		{
			output << "Declare " << entity;
		}

	private:
		const TEntity& entity;
	};
}
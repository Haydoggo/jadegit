#pragma once
#include "ExtractDefinitionTask.h"

namespace JadeGit::Build
{
	template<class TEntity>
	class ExtractDefinitionRenameTask : public ExtractDefinitionTask<TEntity>
	{
	public:
		ExtractDefinitionRenameTask(Graph& graph, Task* parent, const TEntity& entity, const char* oldName) : ExtractDefinitionTask<TEntity>(graph, parent, entity), oldName(oldName)
		{
		};

		bool execute(ExtractStrategy& strategy) const final
		{
			return strategy.Define(this->entity, oldName);
		}

		void Print(std::ostream& output) const final
		{
			ExtractDefinitionTask<TEntity>::Print(output);
			if (oldName)
				output << ", previously " << oldName;
		}

	protected:
		const char* oldName = nullptr;
	};
}
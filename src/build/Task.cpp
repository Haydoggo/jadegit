#include "Task.h"
#include "BuildTask.h"
#include "CommandTask.h"
#include "Graph.h"
#include "ReorgTask.h"
#include "TaskState.h"
#include "TaskVisitor.h"
#include <stack>
#include <queue>

using namespace std;

namespace JadeGit::Build
{
	Task::Task(Graph& graph, bool complete, short priority) : Task(graph, nullptr, complete, priority)
	{
	}

	Task::Task(Graph& graph, Task& parent, bool complete, short priority) : Task(graph, &parent, complete, priority)
	{
	}

	Task::Task(Graph& graph, Task* parent, bool complete, short priority) : graph(graph), parent(parent), complete(complete), priority(priority)
	{
		// Set initial state
		state = TaskState::initialState();
		
		// Add to parent
		if (parent)
			parent->children.push_back(this);
		else
			graph.children.push_back(this);

		// Increment graph size
		graph.size++;
	}

	Task::~Task()
	{
		for (const Task* task : children)
			delete task;
	}

	bool Task::operator<(const Task& rhs) const
	{
		return priority < rhs.priority;		
	}

	void Task::addPredecessor(Task& predecessor, bool common, bool needsReorg)
	{
		if (!this || &predecessor == this)
			return;

		// Add predecessor with reorg task if needed
		if (needsReorg)
		{
			auto reorg = new ReorgTask(graph);
			reorg->addPredecessor(predecessor);
			return addPredecessor(*reorg, common);
		}

		assert(!predecessor.isSuccessorOf(*this));

		predecessors.push_back(&predecessor);
		predecessor.successors.push_back(this);

		// Add common predecessor (which are inherited by children)
		if (common)
			predecessors_common.push_back(&predecessor);
	}

	bool Task::isChildOf(const Task* parent) const
	{
		const Task* child = this;
		while (child)
		{
			if (child->parent == parent)
				return true;

			child = child->parent;
		}

		return false;
	}
	
	bool Task::isSuccessorOf(const Task& predecessor) const
	{
		if (&predecessor == this)
			return false;

		if (std::find(predecessors.begin(), predecessors.end(), &predecessor) != predecessors.end())
			return true;

		for (const Task* task : predecessors)
		{
			if (task->isSuccessorOf(predecessor))
				return true;
		}

		return false;
	}

	vector<const Task*> Task::prerequisites(bool cascading) const
	{
		vector<const Task*> prerequisites;

		// Add common predecessors inherited from parent tasks
		if (!cascading)
		{
			stack<const Task*> stack;
			const Task* parent = this->parent;
			while (parent)
			{
				stack.push(parent);
				parent = parent->parent;
			}

			while (!stack.empty())
			{
				for (auto& predecessor : stack.top()->predecessors_common)
					prerequisites.push_back(predecessor);

				stack.pop();
			}
		}

		// Add direct predecessors
		for (auto& predecessor : predecessors)
			prerequisites.push_back(predecessor);

		// Add child predecessors, which are not children
		queue<const Task*> queue;
		for (auto& child : children)
			queue.push(child);

		while (!queue.empty())
		{
			auto& next = *queue.front();
			queue.pop();

			for (auto& predecessor : next.predecessors)
			{
				if (!predecessor->isChildOf(this))
					prerequisites.push_back(predecessor);
			}

			for (auto& child : next.children)
				queue.push(child);
		}

		return prerequisites;
	}

	void Task::reset() const
	{
		state->reset(*this);
	}

	bool Task::traverse(size_t& visited, TaskVisitor& visitor, bool& aborted, IProgress* progress, int depth, const Task* cascade) const
	{
		assert(!aborted);
		assert(!children_processing && !predecessors_processing);

		return state->traverse(*this, visited, visitor, aborted, progress, depth, cascade);
	}

	bool Task::tryAddPredecessor(Task& predecessor)
	{
		if (predecessor.isSuccessorOf(*this))
			return false;

		addPredecessor(predecessor);
		return true;
	}

	ostream& operator<< (ostream& stream, const Task& task)
	{
		task.Print(stream);
		return stream;
	}

	bool BuildTask::accept(TaskVisitor& v) const
	{
		return v.visit(*this);
	}

	bool CommandTask::accept(TaskVisitor& v) const
	{
		return v.visit(*this);
	}
}
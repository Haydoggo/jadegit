#pragma once
#include "ClassDelta.h"
#include "JadeInterfaceDelta.h"
#include "EmptyTask.h"
#include <jadegit/data/JadeImportedPackage.h>

using namespace JadeGit::Data;

namespace JadeGit::Build
{
	class JadeImportedPackageDelta : public SchemaComponentDelta<JadeImportedPackage>
	{
	public:
		JadeImportedPackageDelta(Graph& graph) : SchemaComponentDelta(graph, "Package") {}

	protected:
		bool AnalyzeEnter() override
		{
			if (!SchemaComponentDelta::AnalyzeEnter())
			{
				// Exported package cannot be deleted until imported package is
				this->addDeletionDependency(previous->exportedPackage, GetDeletion(), previous->GetRootSchema().version < par66557);

				return false;
			}

			// Defining imported package depends on exported package
			this->addCreationDependency(latest->exportedPackage, GetDefinition());
			 
			return true;
		}
	};

	template<class TEntity, template<class> class TBase> 
	class JadeImportedEntityDelta : public TBase<TEntity>
	{
	public:
		using TBase<TEntity>::TBase;

		using TBase<TEntity>::previous;
		using TBase<TEntity>::latest;

	protected:
		using TBase<TEntity>::graph;
		using TBase<TEntity>::GetCreation;
		using TBase<TEntity>::GetDeletion;

		bool AnalyzeEnter() override
		{
			if (!TBase<TEntity>::AnalyzeEnter())
			{
				// Exported entity cannot be deleted until imported entity is
				this->addDeletionDependency(previous->GetExportedEntity(), GetDeletion(), previous->GetRootSchema().version < par66557);

				return false;
			}

			// Dependent on exported entity
			this->addCreationDependency(latest->GetExportedEntity(), GetCreation());

			return true;
		}

		Task* HandleDeletion(Task* parent) override
		{
			// Use stub deletion task for imported entities as they're not explicitly deleted
			return new DeleteTask(graph, parent, "imported entity");
		}
	};

	class JadeImportedClassDelta : public JadeImportedEntityDelta<JadeImportedClass, ClassDelta>
	{
	public:
		using JadeImportedEntityDelta::JadeImportedEntityDelta;

	protected:
		bool requiresDeclaration() const final
		{
			return true;
		}
	};

	using JadeImportedInterfaceDelta = JadeImportedEntityDelta<JadeImportedInterface, JadeInterfaceDelta>;

	template<class TFeature>
	class JadeImportedFeatureDelta : public JadeImportedEntityDelta<TFeature, Delta>
	{
	public:
		using JadeImportedEntityDelta<TFeature, Delta>::JadeImportedEntityDelta;

	protected:
		Task* handleDefinition(Task* parent) override
		{
			// Use empty task for imported features as they're not explicitly defined
			return new EmptyTask(this->graph, parent);
		}
	};
}
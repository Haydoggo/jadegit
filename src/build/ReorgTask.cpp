#include "ReorgTask.h"
#include "TaskVisitor.h"
#include <build/Builder.h>

namespace JadeGit::Build
{
	bool ReorgTask::accept(TaskVisitor& v) const
	{
		return v.visit(*this);
	}

	void ReorgTask::Print(std::ostream& output) const 
	{
		output << "<reorg>"; 
	}

	bool ReorgTask::execute(Builder& builder) const
	{
		builder.Reorg();
		return true;
	}
}
#pragma once
#include "Task.h"

namespace JadeGit::Build
{
	class EmptyTask : public Task
	{
	public:
		EmptyTask(Graph& graph, Task* parent, const char* description = "empty") : Task(graph, parent), 
			description(description)
		{
		}

	private:
		const char* description;

		bool accept(TaskVisitor& v) const final { return true; };
		void Print(std::ostream& output) const final { output << "<" << description << ">"; };
	};
}
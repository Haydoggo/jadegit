#pragma once
#include "TypeDelta.h"
#include <jadegit/data/JadeInterface.h>

namespace JadeGit::Build
{
	class IJadeInterfaceDelta : public ITypeDelta
	{
	public:
		using ITypeDelta::ITypeDelta;

		// Nothing specific as yet
	};

	template<class TComponent, class TInterface = IJadeInterfaceDelta>
	class JadeInterfaceDelta : public TypeDelta<TComponent, TInterface>
	{
	public:
		JadeInterfaceDelta(Graph& graph) : TypeDelta<TComponent, TInterface>(graph, "Interface") {}

		using TypeDelta<TComponent, TInterface>::previous;
		using TypeDelta<TComponent, TInterface>::latest;

	protected:
		using TypeDelta<TComponent, TInterface>::graph;
		using TypeDelta<TComponent, TInterface>::GetDefinition;
		using TypeDelta<TComponent, TInterface>::GetDeletion;

		bool AnalyzeEnter() override
		{
			// Basic analysis
			if (!TypeDelta<TComponent, TInterface>::AnalyzeEnter())
			{
				// Handle deletion before deleting previous super-interfaces
				for (JadeInterface* interface : previous->superinterfaces)
					this->addDeletionDependency(interface, GetDeletion());

				return false;
			}

			// Latest definition is dependent on latest super-interfaces being created
			for (JadeInterface* interface : latest->superinterfaces)
				this->addCreationDependency(interface, GetDefinition());

			// Deleting previous super-interfaces depends on interface being updated
			if (previous)
				for (JadeInterface* interface : previous->superinterfaces)
					this->addDeletionDependency(interface, GetDefinition());

			return true;
		}

		bool requiresCompleteDefinition() const final
		{
			// Always create a 'complete' definition for interfaces (otherwise, omitted features are deleted during load)
			return true;
		}
	};
}
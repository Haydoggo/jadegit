#pragma once
#include "Delta.h"
#include "DeleteTask.h"
#include "RenameTask.h"
#include <jadegit/data/SchemaEntity.h>

namespace JadeGit::Build
{
	class ISchemaComponentDelta : public IDelta
	{
	public:
		using IDelta::IDelta;

		virtual std::string QualifiedName() const = 0;
		virtual Task* GetRename() const = 0;
	};

	template<class TComponent, class TInterface = ISchemaComponentDelta>
	class SchemaComponentDelta : public Delta<TComponent, TInterface>
	{
	public:
		SchemaComponentDelta(Graph& graph, const char* entityType) : Delta<TComponent, TInterface>(graph), entityType(entityType) {}

		using Delta<TComponent, TInterface>::previous;
		using Delta<TComponent, TInterface>::latest;

	protected:
		using Delta<TComponent, TInterface>::analyzed;
		using Delta<TComponent, TInterface>::graph;

		const char* const entityType;

		Task* GetCreation() const override
		{
			/* Rename is like creation (new entity name needs to be established before use) */
			return rename ? rename : Delta<TComponent, TInterface>::GetCreation();
		}

		Task* GetRename() const final
		{
			return rename;
		}

		std::string QualifiedName(const Data::Entity* entity) const
		{
			const Data::Entity* parent = entity->GetQualifiedParent();
			if (!parent)
				return entity->GetName();
			
			const ISchemaComponentDelta* delta = graph.Find<ISchemaComponentDelta>(parent);
			return (delta ? delta->QualifiedName() : QualifiedName(parent)) + "::" + entity->GetName();
		}

		std::string QualifiedName() const final
		{
			return QualifiedName((previous && !rename) ? previous : latest);
		}

		Task* handleDefinition(Task* parent)
		{
			// Setup rename task
			if (previous && previous->GetName() != latest->GetName())
				rename = handleRename();

			// Setup definition task
			auto definition = Delta<TComponent, TInterface>::handleDefinition(parent);

			// Handle renaming before definition (common predecessor inherited by child definition tasks)
			definition->addPredecessor(rename, true);

			return definition;
		}

		Task* HandleDeletion(Task* parent) override
		{
			if (Task* task = Delta<TComponent, TInterface>::HandleDeletion(parent))
				return task;

			// Setup deletion command, with explicit deletion suppressed for copies
			Task* deletion = previous->isCopy() ? new DeleteTask(graph, parent, "copy") : new DeleteTask(graph, parent, entityType, QualifiedName());

			// Parent rename before deletion
			deletion->addPredecessor(GetRename());

			return deletion;
		}

		virtual Task* handleRename()
		{
			return new RenameTask(graph, entityType, QualifiedName(), latest->GetName());
		}
	private:
		Task* rename = nullptr;
	};
}
#pragma once
#include "ExtractTask.h"

namespace JadeGit::Build
{
	template <Definable TEntity>
	class ExtractDefinitionTask : public ExtractTask
	{
	public:
		ExtractDefinitionTask(Graph& graph, Task* parent, const TEntity& entity) : ExtractTask(graph, parent, true, 1), entity(entity)
		{
		};

		bool execute(ExtractStrategy& strategy) const override
		{
			return strategy.Define(entity);
		}

		void Print(std::ostream& output) const override
		{
			output << "Define " << entity;
		}

	protected:
		const TEntity& entity;
	};

	template <PartiallyDefinable TEntity>
	class ExtractDefinitionTask<TEntity> : public ExtractTask
	{
	public:
		ExtractDefinitionTask(Graph& graph, Task* parent, const TEntity& entity, bool complete, bool modified) : ExtractTask(graph, parent, complete, 1), entity(entity), modified(modified)
		{
		};

		bool execute(ExtractStrategy& strategy) const final
		{
			return strategy.Define(entity, complete, modified);
		}

		void Print(std::ostream& output) const final
		{
			output << "Define " << entity << (complete ? "" : " <partial>");
		}

	protected:
		const TEntity& entity;
		const bool modified;
	};
}
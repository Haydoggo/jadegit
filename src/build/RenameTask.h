#pragma once
#include "CommandTask.h"

namespace JadeGit::Build
{
	class RenameTask : public CommandTask
	{
	public:
		const char* entityType;
		std::string qualifiedName;
		std::string newName;

		RenameTask(Graph& graph, const char* entityType, std::string qualifiedName, std::string newName);

	protected:
		void execute(CommandStrategy &strategy) const final;
		void Print(std::ostream& output) const final;
	};
}
#pragma once
#include <jadegit/vfs/NativeFileSystem.h>

namespace JadeGit
{
	class TempFileSystem : public NativeFileSystem
	{
	public:
		TempFileSystem(std::filesystem::path subfolder);
		~TempFileSystem();

		std::filesystem::path path() const;
		void purge();
	};
}
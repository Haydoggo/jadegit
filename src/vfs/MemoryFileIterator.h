#pragma once
#include <jadegit/vfs/AnyFileIterator.h>
#include <map>

namespace JadeGit
{
	class MemoryFileNode;
	class MemoryFileDirectory;

	class MemoryFileIterator : public AnyFileIterator
	{
	public:
		MemoryFileIterator(std::unique_ptr<AnyFile>&& parent, const MemoryFileDirectory& directory);
		virtual ~MemoryFileIterator();

		std::unique_ptr<AnyFileIterator> clone() const override;
		bool valid() const override;
		std::filesystem::path filename() const override;
		void next() override;

	protected:
		const MemoryFileDirectory& directory;
		std::map<std::string, MemoryFileNode*>::const_iterator iter;
	};
}
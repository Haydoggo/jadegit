#pragma once
#include <string>

namespace JadeGit::Deploy
{
	class Manifest
	{
	public:
		static Manifest get();
		static void set(const Manifest& manifest);
		static void set(std::string origin, std::string commit);

		std::string origin;
		std::string commit;
	};
}
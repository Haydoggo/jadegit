#include "Commands.h"
#include <jadegit/Exception.h>
#include <registry/Root.h>

using namespace std;
using namespace std::filesystem;

namespace JadeGit::Deploy
{
	void start(const path& registry)
	{
		// Validate registry changes
		Registry::Root::validate(registry);
	}

	void finish(const path& registry)
	{
		// Load registry changes
		Registry::Root::load(registry);
	}
}
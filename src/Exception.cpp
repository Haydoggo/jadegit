#include <jadegit/Exception.h>
#include <jadegit/Version.h>
#include <Platform.h>

using namespace std;

namespace JadeGit
{
	jadegit_operation_aborted::jadegit_operation_aborted() : jadegit_exception("Operation aborted by user") {}

	jadegit_unimplemented_feature::jadegit_unimplemented_feature() : jadegit_unimplemented_feature("Feature") {}
	jadegit_unimplemented_feature::jadegit_unimplemented_feature(const string& feature) : jadegit_exception(feature + " is not available as support for this has not been implemented in this release") {}
	
	jadegit_unsupported_feature::jadegit_unsupported_feature() : jadegit_unsupported_feature("Feature") {}
	jadegit_unsupported_feature::jadegit_unsupported_feature(const string& feature) : jadegit_exception(feature + " is not supported") {}
	jadegit_unsupported_feature::jadegit_unsupported_feature(const string& feature, const AnnotatedVersion& requiredVersion) : jadegit_exception(format("{} is not supported (requires {})", feature, static_cast<string>(requiredVersion))) {}
}

#if USE_GIT2
#include <jadegit/git2.h>
#include <git2/sys/errors.h>

namespace JadeGit
{
	git_exception::git_exception(int code, const git_error* error) : jadegit_exception(code, error ? error->message : "Missing error message") { git_error_clear(); }
}

#endif
#include "Function.h"
#include <dev/Session.h>

namespace JadeGit::Development
{
	// Sign-on functions (those that don't require specific handling, except to complete sign-on process and/or restart explorer)
	class SignOnFunction : public SessionFunction
	{
	public:
		SignOnFunction(std::initializer_list<const char*> taskNames, bool administration) : SessionFunction(taskNames), administration(administration) {}

	protected:
		bool administration = false;

		bool execute(Session& session, const std::string& entityName) const final
		{
			// Sign-on completed, start explorer if not in admin mode
			return administration || session.Start();
		}
	};

	static SignOnFunction signon({ Function::AddSchema,			// Need to check explorer is running beforehand as it cannot start while schema is being added
								   Function::BrowseClasses, 
								   Function::ShowPainter 
								 }, false);
	static SignOnFunction signonAdmin({ Function::Administration }, true);
}
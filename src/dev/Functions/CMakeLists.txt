target_sources(jadegit PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/Function.cpp
	${CMAKE_CURRENT_LIST_DIR}/Blacklist.cpp
	${CMAKE_CURRENT_LIST_DIR}/Deletes.cpp
	${CMAKE_CURRENT_LIST_DIR}/Reorg.cpp
	${CMAKE_CURRENT_LIST_DIR}/SignOn.cpp
	${CMAKE_CURRENT_LIST_DIR}/Updates.cpp
	${CMAKE_CURRENT_LIST_DIR}/Whitelist.cpp
)
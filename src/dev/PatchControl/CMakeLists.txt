target_sources(jadegit PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/ActiveXControl.cpp
	${CMAKE_CURRENT_LIST_DIR}/DeploymentControl.cpp
	${CMAKE_CURRENT_LIST_DIR}/EntityControl.cpp
	${CMAKE_CURRENT_LIST_DIR}/EntityRenameStrategy.cpp
	${CMAKE_CURRENT_LIST_DIR}/FormControl.cpp
	${CMAKE_CURRENT_LIST_DIR}/HTMLDocumentControl.cpp
	${CMAKE_CURRENT_LIST_DIR}/LocaleControl.cpp
	${CMAKE_CURRENT_LIST_DIR}/PatchControl.cpp
	${CMAKE_CURRENT_LIST_DIR}/SchemaControl.cpp
	${CMAKE_CURRENT_LIST_DIR}/TranslatableStringControl.cpp
)